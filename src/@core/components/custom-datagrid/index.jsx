import React, { useState, useEffect } from 'react';
import { DataGrid } from '@mui/x-data-grid';
import { Box } from '@mui/system';



const CustomDataGrid = ({ rows, columns, show, rowHeight }) => {

  const [paginationModel, setPaginationModel] = useState({ page: 0, pageSize: show });





  return (
    <>
      <Box sx={{  width: '100%',height:'500px'}}>
        <DataGrid

          columns={columns}
          rows={rows || []}
          pageSizeOptions={[7, 10, 25, 50]}
          paginationModel={paginationModel}
          onPaginationModelChange={setPaginationModel}
          rowHeight={rowHeight}


        />
      </Box>
    </>
  );
};

export default CustomDataGrid;
