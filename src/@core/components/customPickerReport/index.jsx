import { Button, Card, CardContent, Typography } from '@mui/material'
import { Box } from '@mui/system'
import React, { useState } from 'react'
import DatePicker from 'react-datepicker'
import { useTranslation } from 'react-i18next'

import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'
import { FormateDate } from 'src/utiltis/DateFormate'
import { DateFormateOfMonth } from 'src/utiltis/DateFormateOfMonth'
import { DateFormateOfYear } from 'src/utiltis/DateFormateOfYear'
import { setDateSelected } from '../../../store/apps/user'
import { useDispatch } from 'react-redux'

export const ReportDaily = ({ handleClose }) => {
  const [startDate, setStartDate] = useState(new Date())
  const [showMonthPicker, setShowMonthPicker] = useState('month')

  const dispatch = useDispatch()

  const toggleDatePickerMonth = () => {
    setStartDate(new Date())
    setShowMonthPicker('month')
  }

  function Close() {
    handleClose()
  }

  const { t } = useTranslation()

  const toggleDatePickerYear = () => {
    setStartDate(new Date())
    setShowMonthPicker('year')
  }

  const toggleDatePickerDay = () => {
    setStartDate(new Date())
    setShowMonthPicker('day')
  }

  const handleDateSend = date => {
    if (showMonthPicker === 'year') {
      const formattedDate = DateFormateOfYear(date)
      setStartDate(date)

      dispatch(setDateSelected(formattedDate))
    }
    if (showMonthPicker === 'month') {
      const formattedDate = DateFormateOfMonth(date)
      setStartDate(date)
      dispatch(setDateSelected(formattedDate))
    }
    if (showMonthPicker === 'day') {
      const formattedDate = FormateDate(date)
      setStartDate(date)
      dispatch(setDateSelected(formattedDate))
    }
    Close()
  }

  return (
    <Card sx={{ width: '426px', height: '350px' }}>
      <CardContent>
        <Typography sx={{ fontSize: '20px', fontWeight: '600', color: '#8090A7' }}>{t('Filter')}</Typography>
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
          <Button
            variant='contained'
            color='secondary'
            sx={{ mr: 2, backgroundColor: showMonthPicker === 'day' ? '#6AB2DF' : 'secondary' }}
            onClick={toggleDatePickerDay}
          >
            {t('Day')}
          </Button>
          <Button
            variant='contained'
            color='secondary'
            onClick={toggleDatePickerMonth}
            sx={{ mr: 2, backgroundColor: showMonthPicker === 'month' ? '#6AB2DF' : 'secondary' }}
          >
            {t('Month')}{' '}
          </Button>
          <Button
            variant='contained'
            color='secondary'
            onClick={toggleDatePickerYear}
            sx={{ mr: 2, backgroundColor: showMonthPicker === 'year' ? '#6AB2DF' : 'secondary' }}
          >
            {t('Year')}
          </Button>
        </Box>
      </CardContent>

      <DatePickerWrapper
        sx={{
          width: '100%',

          display: 'flex',
          justifyContent: 'center',
          '& .react-datepicker': { boxShadow: 'none !important', border: 'none !important' }
        }}
      >
        <DatePicker
          inline
          selected={startDate}
          onChange={date => handleDateSend(date)}
          dateFormat={showMonthPicker ? 'MMM' : 'yyyy'}
          showMonthYearPicker={showMonthPicker === 'month'}
          showYearPicker={showMonthPicker === 'year'}
          fixedHeight
          calendarClassName='rasta-stripes'
          showFourColumnMonthYearPicker
          yearItemNumber={9}
          style={{
            fontSize: '16px', // Set the font size
            fontWeight: 'bold' // Set the font weight
            // Add any other inline styles as needed
          }}
          maxDate={new Date()}
        />
      </DatePickerWrapper>
    </Card>
  )
}
