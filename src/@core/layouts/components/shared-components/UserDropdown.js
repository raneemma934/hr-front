'use client'

// ** React Imports
import { useState, useEffect, Fragment, useCallback } from 'react'

// ** Next Import
import { useRouter } from 'next/router'

// ** MUI Imports
import Box from '@mui/material/Box'
import Menu from '@mui/material/Menu'
import Badge from '@mui/material/Badge'
import Avatar from '@mui/material/Avatar'
import Divider from '@mui/material/Divider'
import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'
import MenuItem from '@mui/material/MenuItem'
import Edit from '../../../../../public/images/IconInput/edit'
import Button from '@mui/material/Button'
import IconButton from '@mui/material/IconButton'
import Link from 'next/link'
import Stack from '@mui/material/Stack'

// ** Context
import { useAuth } from 'src/hooks/useAuth'
import { useTranslation } from 'react-i18next'
import useShowAllBranches from 'src/features/settings/hooks/useShowAllBranches'
import ChangePassword from '../../../../features/change/ChangePassword'

// ** Styled Components
const BadgeContentSpan = styled('span')(({ theme }) => ({
  width: 8,
  height: 8,
  borderRadius: '50%',
  backgroundColor: theme.palette.success.main,
  boxShadow: `0 0 0 2px ${theme.palette.background.paper}`
}))

const MenuItemStyled = styled(MenuItem)(({ theme }) => ({
  // Custom styles can be added here
}))

const UserDropdown = ({ settings }) => {
  const { data, isloading } = useShowAllBranches()

  const branches = data?.data?.data || []
  console.log("🚀 ~ UserDropdown ~ branches:", branches)

  const [isDropdownOpen, setIsDropdownOpen] = useState(false)
  const [open, setOpen] = useState(false)
  const [selectedBranch, setSelectedBranch] = useState(null)
  const [anchorEl, setAnchorEl] = useState(null)

  const { t } = useTranslation()
  const router = useRouter()
  const { logout } = useAuth()

  useEffect(() => {
    const storedBranch = JSON.parse(localStorage.getItem('selectedBranch'))
    if (storedBranch) {
      setSelectedBranch(storedBranch)
    }
  }, [])

  const handleDropdownOpen = event => {
    setAnchorEl(event.currentTarget)
  }

  const handleDropdownClose = useCallback(
    url => {
      if (url) {
        router.push(url)
      }
      setAnchorEl(null)
    },
    [router]
  )

  const handleMenuItemClick = branch => {
    localStorage.setItem('selectedBranch', JSON.stringify(branch))
    localStorage.setItem('branch', branch?.id)
    localStorage.setItem('branchName', branch?.name)
    window.location.reload() // Replaces window.location.reload(false) for consistency

    setSelectedBranch(branch)
  }

  const handleLogout = () => {
    logout()
    handleDropdownClose()
  }

  const userData = JSON.parse(localStorage.getItem('userData')) || {}
  const userInfo = JSON.parse(localStorage.getItem('userInfo')) || {}

  const direction = settings?.direction || 'ltr'

  return (
    <Fragment>
      <Badge
        overlap='circular'
        onClick={handleDropdownOpen}
        sx={{ ml: 2, cursor: 'pointer' }}
        badgeContent={<BadgeContentSpan />}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      >
        <Avatar alt='' src={`${process.env.NEXT_PUBLIC_IMAGES}/${userInfo?.image}`} sx={{ width: 38, height: 38 }} />
      </Badge>
      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={() => handleDropdownClose()}
        sx={{ '& .MuiMenu-paper': { width: 360, mt: 4.75 } }}
        anchorOrigin={{ vertical: 'bottom', horizontal: direction === 'ltr' ? 'right' : 'left' }}
        transformOrigin={{ vertical: 'top', horizontal: direction === 'ltr' ? 'right' : 'left' }}
      >
        <Box sx={{ py: 1.75, px: 6 }}>
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Badge
              overlap='circular'
              badgeContent={<BadgeContentSpan />}
              anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            >
              <Avatar
                alt=''
                src={`${process.env.NEXT_PUBLIC_IMAGES}/${userInfo?.image}`}
                sx={{ width: '2.5rem', height: '2.5rem' }}
              />
            </Badge>
            <Box display='flex' justifyContent='space-between' width='100%' alignItems='center'>
              <Box sx={{ display: 'flex', ml: 2.5, flexDirection: 'column' }}>
                <Typography sx={{ fontWeight: 500 }}>
                  {userData?.first_name} {userData?.last_name}
                </Typography>
                <Typography variant='body2'>{t(userData?.role)}</Typography>
              </Box>
              <Stack direction='row' alignItems='center' spacing={1}>
                <IconButton onClick={() => setOpen(true)}>
                  <Edit />
                </IconButton>
                <Link href='/Settings'>
                  <Box>
                    <img src='/images/setting.svg' alt='setting' />
                  </Box>
                </Link>
              </Stack>
            </Box>
          </Box>
        </Box>
        <Divider sx={{ my: 2 }} />

        {branches.map(branch => (
          <MenuItem
            key={branch.id}
            sx={{
              p: 0,
              backgroundColor: selectedBranch?.id === branch.id ? '#6AB2DF' : 'inherit',
              '&:hover': {
                backgroundColor: selectedBranch?.id === branch.id ? '#6AB2DF !important' : 'inherit'
              }
            }}
            onClick={() => handleMenuItemClick(branch)}
          >
            <Box>
              <Typography>{branch?.name}</Typography>
            </Box>
          </MenuItem>
        ))}

        <Divider sx={{ my: 2 }} />
        <MenuItemStyled sx={{ p: 0 }} onClick={handleLogout}>
          <Button sx={{ backgroundColor: '#DF2E38', color: '#fff', width: '100%' }}>{t('Log Out')}</Button>
        </MenuItemStyled>
      </Menu>
      {open && (
        <ChangePassword
          handleOpen={() => setOpen(true)}
          handleClose={() => setOpen(false)}
          open={open}
          userInfo={userInfo}
          userData={userData}
        />
      )}
    </Fragment>
  )
}

export default UserDropdown
