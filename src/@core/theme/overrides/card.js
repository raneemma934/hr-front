
const Card = skin => {
  return {
    MuiCard: {
      styleOverrides: {
        root: ({ theme }) => ({
          ...(skin === 'bordered' && { border: `1px solid ${theme.palette.divider}`  ,boxShadow:"0px 2px 9px 0px rgba(0, 0, 0, 0.05)" }),

          '& .MuiTableContainer-root, & .MuiDataGrid-root, & .MuiDataGrid-columnHeaders': {
            borderRadius: ' 0.75rem',
            boxShadow:"0px 2px 9px 0px rgba(0, 0, 0, 0.05)"
          }
        })
      },

    },
    MuiCardHeader: {
      styleOverrides: {
        root: ({ theme }) => ({
borderRadius:'12px',
          '& + .MuiCardContent-root, & + .MuiCardActions-root, & + .MuiCollapse-root .MuiCardContent-root': {
            paddingTop: 0
          },

        }),
        title: ({ theme }) => ({
          fontWeight: 600,
          lineHeight: 'normal',
          letterSpacing: '0.15px',
          fontSize: '1.25rem',
          color:'#8090A7'
        }),
        action: {
          marginTop: 0,
          marginRight: 0
        }
      }
    },
    MuiCardContent: {
      styleOverrides: {
        root: ({ theme }) => ({

          '& + .MuiCardHeader-root, & + .MuiCardContent-root, & + .MuiCardActions-root': {
            paddingTop: 0
          },
          '&:last-of-type': {
            paddingBottom: 0
          }
        })
      }
    },
    MuiCardActions: {
      styleOverrides: {
        root: ({ theme }) => ({
          padding: 0,
          '& .MuiButton-text': {
            paddingLeft: theme.spacing(3),
            paddingRight: theme.spacing(3)
          },
          '&.card-action-dense': {
            padding: theme.spacing(0, 3, 3),
            '.MuiCard-root .MuiCardMedia-root + &': {
              paddingTop: theme.spacing(3)
            }
          },
          '.MuiCard-root &:first-of-type': {
            paddingTop: theme.spacing(3),
            '& + .MuiCardHeader-root, & + .MuiCardContent-root, & + .MuiCardActions-root': {
              paddingTop: 0
            }
          }
        })
      }
    }
  }
}

export default Card
