import { color } from "@mui/system"

const Dialog = skin => {
  return {
    MuiDialog: {
      styleOverrides: {
        paper: ({ theme }) => ({
          boxShadow: theme.shadows[skin === 'bordered' ? 0 : 18],
          ...(skin === 'bordered' && { border: `1px solid ${theme.palette.divider}` }),
          padding:'1rem 1.5rem',
          borderRadius: '0.75rem',
          [theme.breakpoints.down('xs')]: {

            color:'#0000',
            width: `calc(100% - ${theme.spacing(8)})`,
            maxWidth: `calc(100% - ${theme.spacing(8)}) !important`
          },
          '& > .MuiList-root': {
              // Setting padding to 1rem

          }
        })
      }
    },
    MuiDialogTitle: {
      styleOverrides: {
        root: ({ theme }) => ({
          color:'#8090A7',
          fontSize:'16px',
          fontWeight:600,
          lineHeightL:'normal',
          fontStyle:'normal',

        })
      }
    },
    MuiDialogContent: {
      styleOverrides: {
        root: ({ theme }) => ({
          color:'#131627',
          fontSize:'0.875rem',
          fontWeight:400,
          lineHeightL:'2rem',
          fontStyle:'normal',
        })
      }
    },
    MuiDialogActions: {
      styleOverrides: {
        root: ({ theme }) => ({
          gap: '0.75rem',
          display: 'flex',
          padding: theme.spacing(2), // Add padding

        })
    }

    }
  }
}

export default Dialog
