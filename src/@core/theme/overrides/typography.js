

const typography = {
  MuiTypography: {
    styleOverrides: {
      gutterBottom: ({ theme }) => ({
        marginBottom: theme.spacing(2)
      })
    },
    variants: [
      {
        // this for breadcrumbs
        props: { variant: 'h1' },
        style: ({ theme }) => ({ color: '#8090A7',fontSize:'24px',fontWeight:600,fontStyle:'normal',lineHeight:'normal',letterSpacing:'1.2px' })
      },
      {
        // this for Section title
        props: { variant: 'h2' },
        style: ({ theme }) => ({ color: '#8090A7',fontSize:'20px',fontWeight:600,fontStyle:'normal',lineHeight:'normal',letterSpacing:'1.2px' })
      },
      {
        props: { variant: 'h3' },
        style: ({ theme }) => ({ color: '#3F4458',fontSize:'0.875rem',fontWeight:600,fontStyle:'normal',lineHeight:'normal' })
      },
      {
        props: { variant: 'h4' },
        style: ({ theme }) => ({ color: theme.palette.text.primary })
      },
      {
        props: { variant: 'h5' },
        style: ({ theme }) => ({ color: theme.palette.text.primary })
      },
      {
        props: { variant: 'h6' },
        style: ({ theme }) => ({ color: theme.palette.text.primary })
      },
      {
        props: { variant: 'subtitle1' },
        style: ({ theme }) => ({ color: theme.palette.text.primary })
      },
      {
        props: { variant: 'subtitle2' },
        style: ({ theme }) => ({ color: theme.palette.text.secondary })
      },
      {
        props: { variant: 'body1' },
        style: ({ theme }) => ({ color: theme.palette.text.primary })
      },
      {
        props: { variant: 'body2' },
        style: ({ theme }) => ({ color: '#8090A7',fontSize:'0.875rem',fontWeight:500,fontStyle:'normal',lineHeight:'normal' })
      },
      {
        props: { variant: 'button' },
        style: ({ theme }) => ({ textTransform: 'none', color: theme.palette.text.primary })
      },
      {
        props: { variant: 'caption' },
        style: ({ theme }) => ({ color: theme.palette.text.secondary })
      },
      {
        props: { variant: 'overline' },
        style: ({ theme }) => ({ color: theme.palette.text.secondary })
      }
    ]
  }
}

export default typography
