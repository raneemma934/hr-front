import { Typography } from '@mui/material'
import { Box, Stack, styled } from '@mui/system'
import React from 'react'
import { useTranslation } from 'react-i18next'
import CustomTextField from 'src/@core/components/mui/text-field'

const InputContainer = styled(Box)(({ theme }) => ({
  display: 'flex',
  width: '400px',
  gap: '4px',
  flexDirection:'column'
}));

export default function Header({ errors, defaultValues, control, Controller }) {
  const { t } = useTranslation();

  return (
    <Box sx={{ backgroundColor: '#FFFFFF', p: '30px', borderRadius: '12px' }} >
           <Typography variant='h2' mb='24px'>
      {t("Branch Information")}
      </Typography>
        <Stack direction={'row'} spacing={4} >

        <Box sx={{ width: '110px', height: '110px' }}>
          <img style={{ width: '100%', height: '100%' }} src='/images/policesIcon/pageLogo/logo.svg' />
        </Box>
        <InputContainer>
          <Controller
            name="ip"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <CustomTextField
                {...field}
                placeholder={t('Enter IP address')}
                fullWidth
                size="small"
                error={!!errors.ip}
                helperText={errors.ip ? errors.ip.message : ' '}
              />
            )}
          />
          <Controller
            name="name"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <CustomTextField
                {...field}
                placeholder={t('Enter branch name')}
                fullWidth
                size="small"
                error={!!errors.name}
                helperText={errors.name ? errors.name.message : ' '}
              />
            )}
          />
        </InputContainer>


    </Stack>
    </Box>

  );
}
