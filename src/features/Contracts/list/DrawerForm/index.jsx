import React, { useState } from 'react'
import { styled, useTheme } from '@mui/material/styles'
import Box from '@mui/material/Box'
import Drawer from '@mui/material/Drawer'
import List from '@mui/material/List'
import Typography from '@mui/material/Typography'
import { Button, TextField } from '@mui/material'
import { Stack } from '@mui/material'
import Avatar from '@mui/material/Avatar'
import { useForm, Controller } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'
import useEditContract from 'src/features/archive/contracts/list/Hooks/useEditContract'
import EditIcon from '../../../../../public/images/IconInput/edit'

const drawerWidth = 440

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  ...theme.mixins.toolbar,
  justifyContent: 'space-between',
  backgroundColor: '#DCE1E6',
  borderRadius: '12px 0px 0px 0px',
  height: '72px',
  padding: '24px'
}))

export default function DrawerForm({ open, setOpenParent, Data, pdf }) {
  const [filepdf, setFilepdf] = useState(null)
  const theme = useTheme()
  const { t } = useTranslation()
  const dispatch = useDispatch()

  const { mutate: EditContract } = useEditContract()

  const handleDrawerClose = () => {
    setOpenParent(false)
    reset()
  }

  const handleFileChange = event => {
    const file = event?.target?.files?.[0]
    setFilepdf(file)
  }

  const defaultValues = {
    startTime: Data?.startDate,
    endTime: Data?.endDate,
    path: Data?.path
  }

  const { control, handleSubmit, reset } = useForm({
    defaultValues,
    mode: 'onBlur'
  })

  const onSubmit = async data => {
    try {
      const formData = new FormData()
      if (filepdf) {
        formData.append('path', filepdf)
      }
      formData.append('startTime', data.startTime)
      formData.append('endTime', data.endTime)

      const dataFormate = {
        id: Data.id,
        formData
      }

      EditContract(dataFormate)
      reset()
    } catch (error) {
      console.log('Error:', error)
    }
  }

  return (
    <Box sx={{ display: 'flex' }}>
      <Drawer
        onClose={handleDrawerClose}
        sx={{
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: { xl: drawerWidth, md: drawerWidth, sm: drawerWidth, xs: '90%' },
            overflowX: 'hidden'
          }
        }}
        anchor='right'
        open={open}
        variant='temporary'
        ModalProps={{
          keepMounted: true
        }}
      >
        <DrawerHeader>
          <Typography sx={{ color: '#8090A7', fontSize: '20px', fontWeight: '600' }}>{t('Edit Contract')}</Typography>
        </DrawerHeader>
        <List>
          <Stack
            sx={{ padding: '12px', margin: '16px', flexDirection: theme.direction === 'rtl' ? 'row-reverse' : 'row' }}
            alignItems={'center'}
            spacing={2}
          >
            <Avatar
              alt={Data?.employee}
              src={`${process.env.NEXT_PUBLIC_IMAGES}/${Data?.user_info}`}
              sx={{ width: 40, height: 40 }}
            />
            <Box>
              <Typography
                sx={{ fontWeight: '500', fontSize: '16px', color: '#3f4458' }}
              >{`${Data?.employee} ${Data?.employeeLastName}`}</Typography>
              <Typography sx={{ fontWeight: '400', fontSize: '14px', color: '#8090a7' }}>
                {Data?.specialization}
              </Typography>
            </Box>
          </Stack>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Box marginTop={'24px'} sx={{ padding: '0px 24px', display: 'flex', flexDirection: 'column', gap: '12px' }}>
              <Typography sx={{ color: '#8090A7', fontSize: '14px', fontWeight: '500' }}> {t('Start date')}</Typography>
              <Controller
                name='startTime'
                control={control}
                render={({ field }) => <TextField {...field} type='date' fullWidth size='small' />}
              />
              <Typography sx={{ color: '#8090A7', fontSize: '14px', fontWeight: '500' }}> {t('End date')}</Typography>
              <Controller
                name='endTime'
                control={control}
                render={({ field }) => <TextField {...field} type='date' fullWidth size='small' />}
              />
              <Stack
                height={'64px'}
                padding={'16px'}
                justifyContent={'space-between'}
                flexDirection={'row'}
                alignItems={'center'}
              >
                <Box display={'flex'} alignItems={'center'} gap={'12px'}>
                  <img src='/images/pdf-icon.svg' alt='PDF Icon' />
                  <Typography sx={{ fontWeight: '500', fontSize: '14px', color: '#3f4458' }}>
                    {t('contract')}: {filepdf?.name || pdf}
                  </Typography>
                </Box>
                <label htmlFor='fileInput'>
                  <Button component='span'>
                    <EditIcon />
                  </Button>
                </label>
                <input
                  id='fileInput'
                  type='file'
                  accept='.pdf'
                  style={{ display: 'none' }}
                  onChange={handleFileChange}
                />
              </Stack>
            </Box>
            <Stack flexDirection={'row'} justifyContent='flex-end' gap={'16px'} sx={{ padding: '24px' }}>
              <Button
                variant='contained'
                type='submit'
                sx={{
                  width: '97px',
                  height: '39px',
                  borderRadius: '4px',
                  padding: '8px 24px',
                  backgroundColor: '#6ab2df',
                  fontWeight: '500',
                  fontSize: '14px',
                  color: '#fff'
                }}
              >
                {t('Submit')}
              </Button>
              <Button
                onClick={handleDrawerClose}
                sx={{
                  width: '97px',
                  height: '39px',
                  borderRadius: '4px',
                  padding: '8px 24px',
                  backgroundColor: '#DCE1E6',
                  fontWeight: '500',
                  fontSize: '14px',
                  color: '#8090A7'
                }}
              >
                {t('Cancel')}
              </Button>
            </Stack>
          </form>
        </List>
      </Drawer>
    </Box>
  )
}
