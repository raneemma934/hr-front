// ** React Imports
import { useState } from 'react'

// ** Next Import
import { useRouter } from 'next/router'

import Grid from '@mui/material/Grid'

import AboutOverivew from 'src/views/coach-profile/profile/AboutOverivew'
import ActivityTimeline from 'src/views/coach-profile/profile/ActivityTimeline'
import UserProfileHeader from 'src/views/coach-profile/UserProfileHeader'

const Profiles = ({ tab, data }) => {
  // ** State
  const [activeTab, setActiveTab] = useState(tab)
  const [isLoading, setIsLoading] = useState(true)

  // ** Hooks
  const router = useRouter()

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <UserProfileHeader Data={data} />
      </Grid>
      <Grid item lg={4} md={5} xs={12}>
        <AboutOverivew Data={data} />
      </Grid>
      <Grid item lg={8} md={7} xs={12}>
        <Grid container spacing={6}>
          <Grid item xs={12}>
            <ActivityTimeline />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default Profiles
