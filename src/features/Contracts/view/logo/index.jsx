import { Typography } from '@mui/material'
import { Box, Stack, styled } from '@mui/system'
import React from 'react'
import { useTranslation } from 'react-i18next'

const LogoContainer = styled(Box)(({ theme }) => ({
  position: 'relative', // Allow absolute positioning for the image
  width: '246px',
  display: 'flex',
  alignItems: 'center',
  width: 'fit-content',
  justifyContent: 'center', // Center the content horizontally
}));

const LogoImage = styled('img')({
  position: 'absolute', // Position the image absolutely
  top: '-95px',
  left: '0',
  width: '160px',
  backgroundColor:'#ffff',
  borderRadius:'8px',
  height: '160px',
  boxShadow:' 0px 2px 12px 0px rgba(0, 0, 0, 0.14);',
});

export default function Logo({data}) {
  console.log("🚀 ~ Logo ~ data:", data)
  const { t } = useTranslation();

  return (
    <Stack
      direction={'column'}
      spacing={2}
      sx={{ backgroundColor: '#FFFFFF', p: '30px', borderRadius: '12px',marginTop:'100px' }}
    >
      <LogoContainer>
        <LogoImage src='/images/policesIcon/pageLogo/logo.svg' />

      </LogoContainer>
      <Stack spacing={1} direction={'column'} sx={{marginLeft:'185px !important'}}  >
      <Typography  variant='h2' >
          {data?.data.data.name}
        </Typography>
        <Typography variant='body-2'>
          {data?.data.data.ip}
        </Typography>
      </Stack>
    </Stack>
  );
}
