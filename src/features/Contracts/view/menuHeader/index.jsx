import React from 'react'
import { Typography,Avatar } from '@mui/material'
import { Box, Stack, } from '@mui/system'
import { useTranslation } from 'react-i18next'
import useViewContract from '../../list/Hooks/useViewContracts'
import { CircularProgress } from '@mui/material'
import { format } from 'date-fns'

export default function HeaderComponent({id,componentRef}) {

  const {t} = useTranslation()


  const { data, isLoading, isError } = useViewContract(id)



  return (
    <Stack  direction={'column'} spacing={2} sx={{backgroundColor:"#FFFFFF",p:"30px",borderRadius:"12px"}}>
    {data?.data?.data?.length?
    <>

    {data?.data?.data.map((ele,index) => (
      <Stack direction={'row'} spacing={12} key={index}>
        <Box sx={{ display: 'flex', flexDirection: 'column', gap: '18px' }}>
          <Box
            sx={{

            }}
          >
            <Stack direction={'row'} alignItems={'center'}>


             <Avatar sx={{ width: '52px', height: '52px' }} src={process.env.NEXT_PUBLIC_IMAGES + '/' + ele?.user?.user_info?.image} alt='' />
            <Stack marginLeft={'12px'} direction={'column'}>
              <Typography className='custome-data-grid-font' >{ele?.user?.first_name} {ele?.user?.last_name}</Typography>
              <Typography className='custome-data-grid-font2'>{ele?.user?.specialization}</Typography>
            </Stack>
            </Stack>

            <Typography sx={{ marginTop: '20px', fontWeight: '600', fontSize: '14px', color: '#8090A7' }}>
            {t('Start Date')} : { ele?.startTime ? format(new Date(ele.startTime), 'yyyy-MM-dd') : ''}
            </Typography>

            <Typography sx={{ marginTop: '4px', fontWeight: '600', fontSize: '14px', color: '#8090A7' }}>
            {t('End Date')} : {ele.endTime}
            </Typography>
          </Box>

        </Box>
      </Stack>
    ))}
    </>
          :<Box height={'50vh'} display={'flex'} justifyContent={'center'} alignItems={'center'} ><CircularProgress/></Box>}

  </Stack>

  )
}
