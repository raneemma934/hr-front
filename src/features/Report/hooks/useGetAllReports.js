import { useQuery } from '@tanstack/react-query'
import Reports from '../api/GetAllReports'

export const useReports = date => {
  const query = useQuery({
    queryKey: ['Reports', date],
    queryFn: () => Reports(date)
  })

  return query
}
