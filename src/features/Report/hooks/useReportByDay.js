import { useMutation, useQueryClient } from '@tanstack/react-query'
import ReportByDay from '../api/reportByDay'

export const useReportByDay = () => {
  const queryClient = useQueryClient()

  return useMutation({
    mutationFn: ReportByDay,
    onSuccess: () => {
      queryClient.invalidateQueries('ReportDay')
    }
  })
}
