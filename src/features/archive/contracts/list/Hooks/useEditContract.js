
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { showSuccesToast } from 'src/utiltis/toastSecces';
import EditContract from '../api/EditContract';

const useEditContract = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:EditContract,
    onSuccess: (data) => {
      queryClient.invalidateQueries("contracts");
    showSuccesToast(data.data.message)
    },
    onError(error){
      showErrorToast("Add Contract ",data?.data?.success)
    }

  });
}

export default useEditContract

