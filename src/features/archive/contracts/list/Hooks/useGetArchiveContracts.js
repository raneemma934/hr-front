
import { useQuery } from '@tanstack/react-query'
import GetArchiveContracts from '../api/GetArchiveContracts'

const useGetArchiveContracts = () => {
  const query = useQuery({ queryKey: ['contractsArchive'], queryFn: GetArchiveContracts })

  return query
}

export default useGetArchiveContracts
