import { request } from "../../../../../utiltis/AxiosUtilitis";

const EditContract = async (payload) => {



  return request({
    url: `/api/contract/Update/${payload.id}`,
    method: 'post',
    data: payload.formData,
    headers: {
        "Content-Type": "multipart/form-data",
      },
  });
}

export default EditContract;
