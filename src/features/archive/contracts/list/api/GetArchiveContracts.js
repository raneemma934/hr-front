import { request } from "src/utiltis/AxiosUtilitis"

const GetArchiveContracts = async () => {
  return request({ url: '/api/contract/Archived' })
}

export default GetArchiveContracts
