
import { useMutation, useQueryClient } from '@tanstack/react-query';
import DeleteResigned from '../api/DeleteResigned';
import { showSuccesToast } from 'src/utiltis/toastSecces';
import { ShowErrorToast } from 'src/utiltis/showErrorToast';

const useDeleteUser = () => {

  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:DeleteResigned,
    onSuccess: (data) => {
      queryClient.invalidateQueries("resigned");
      showSuccesToast(data?.data?.message)

    },
    onError:(data) => {

      ShowErrorToast(data?.data?.message)
    }
  });
}

export default useDeleteUser


