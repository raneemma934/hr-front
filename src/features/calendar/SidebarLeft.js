import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Drawer from '@mui/material/Drawer'
import Divider from '@mui/material/Divider'
import Checkbox from '@mui/material/Checkbox'
import Typography from '@mui/material/Typography'
import FormControlLabel from '@mui/material/FormControlLabel'
import DatePicker from 'react-datepicker'
import Icon from 'src/@core/components/icon'
import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'
import { RequestPage } from '@mui/icons-material'
import { FormateDate } from 'src/utiltis/DateFormate'
import { useGetEventByDay } from './hooks/useGetEventByDay'
import Image from 'next/image'
import { useTranslation } from 'react-i18next'
import { useEffect, useState } from 'react'
import { Stack, padding } from '@mui/system'
import moment from 'moment'

const SidebarLeft = props => {
  const {
    store,
    mdAbove,
    dispatch,
    calendarApi,
    calendarsColor,
    leftSidebarOpen,
    leftSidebarWidth,
    handleSelectEvent,
    handleAllCalendars,
    handleCalendarsUpdate,
    handleLeftSidebarToggle,
    handleAddEventSidebarToggle
  } = props
  const { t } = useTranslation()

  const colorsArr = calendarsColor ? Object.entries(calendarsColor) : []
  const { mutate: getEvent, isLoading, data: DataEventByDay } = useGetEventByDay()
  const today = new Date()
  const d = today.getDate()
  const m = (Number(today.getMonth() + 1) < 10 ? '0' : '') + Number(today.getMonth() + 1)
  const y = today.getFullYear()
  const formattedDate = `${y}-${m}-${d}`
  useEffect(() => {
    getEvent(formattedDate)
  }, [])
  const [selectedDate, SetSelectedDate] = useState()

  const renderFilters = colorsArr.length
    ? colorsArr.map(([key, value]) => {
        return (
          <FormControlLabel
            key={key}
            label={key}
            sx={{ '& .MuiFormControlLabel-label': { color: 'text.secondary' } }}
            control={
              <Checkbox
                color={value}
                checked={store?.selectedCalendars.includes(key)}
                onChange={() => dispatch(handleCalendarsUpdate(key))}
              />
            }
          />
        )
      })
    : null

  const handleSidebarToggleSidebar = () => {
    handleAddEventSidebarToggle()
    dispatch(handleSelectEvent(null))
  }

  const handleDateChoose = date => {
    SetSelectedDate(date)

    const FinalformattedDate = moment(date).format('YYYY-MM-DD')
    getEvent(FinalformattedDate)
    calendarApi.gotoDate(date)
  }
  if (renderFilters) {
    return (
      <Drawer
        open={leftSidebarOpen}
        onClose={handleLeftSidebarToggle}
        variant={mdAbove ? 'permanent' : 'temporary'}
        ModalProps={{
          disablePortal: true,
          disableAutoFocus: true,
          disableScrollLock: true,

          keepMounted: true // Better open performance on mobile.
        }}
        sx={{
          zIndex: 3,
          display: 'block',
          position: mdAbove ? 'static' : 'absolute',
          '& .MuiDrawer-paper': {
            borderRadius: 1,
            boxShadow: 'none',
            width: leftSidebarWidth,
            borderTopRightRadius: 0,
            alignItems: 'flex-start',
            borderBottomRightRadius: 0,
            zIndex: mdAbove ? 2 : 'drawer',
            position: mdAbove ? 'static' : 'absolute'
          },
          '& .MuiBackdrop-root': {
            borderRadius: 1,
            position: 'absolute'
          }
        }}
      >
        <Box sx={{ p: 6, width: '100%' }}>
          <Typography sx={{ color: '#8090A7', fontSize: 20, marginLeft: '10px' }} variant='h3'>
            {t('Calendar')}
          </Typography>
        </Box>

        <DatePickerWrapper
          sx={{
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            '& .react-datepicker': { boxShadow: 'none !important', border: 'none !important' }
          }}
        >
          <DatePicker selected={selectedDate} inline onChange={date => handleDateChoose(date)} />
        </DatePickerWrapper>

        <Box sx={{ p: 7, width: '100%' }}>
          <Button fullWidth variant='contained' sx={{ '& svg': { mr: 2 } }} onClick={handleSidebarToggleSidebar}>
            <Icon icon='tabler:plus' fontSize='1.125rem' />
            {t('Add Event')}
          </Button>
        </Box>
        <Divider sx={{ width: '100%', m: '0 !important' }} />

        <Box sx={{ p: 4, width: '100%', overflow: 'auto', '&::-webkit-scrollbar': { width: '3px' } }} height={'350px'}>
          <Typography mb={2} sx={{ fontWeight: '600', fontSize: '20px', color: '#131627' }}>
            {t('Today Event')}
          </Typography>
          <Stack
            width={'280px'}
            height={'300px'}
            overflow={'scroll'}
            sx={{ '&::-webkit-scrollbar': { width: '3px' } }}
            spacing={2}
            direction={'column'}
            alignItems={'baseline'}
          >
            {Array.isArray(DataEventByDay?.data?.data) && DataEventByDay.data.data.length > 0 ? (
              DataEventByDay.data.data.map(event => (
                <>
                  <Stack
                    direction={'row'}
                    sx={{ padding: 0, margin: 0 }}
                    className='parent'
                    key={event.id}
                    height={'70px !important'}
                  >
                    <Stack
                      sx={{ fontWeight: '600', fontSize: '8px', color: '#3f4458' }}
                      alignItems={'center'}
                      direction={'column'}
                    >
                      <span style={{ padding: 0, marginRight: '2px' }} className='child'>
                        {event.day}
                      </span>

                      <Typography className='childp' width={'59px'} p={0} ml={0} variant='p' fontSize={10}>
                        {event.start}
                      </Typography>
                    </Stack>

                    <Box
                      sx={{
                        fontWeight: '500',
                        fontSize: '8px',
                        color: '#3f4458',
                        width: '160px',
                        height: '65px',
                        textAlign: 'start',
                        alignItems: 'center',
                        lineHeight: '15px',
                        overflowY: 'auto',
                        overflowWrap: 'break-word',
                        wordWrap: 'break-word',
                        '&::-webkit-scrollbar': {
                          width: '0px' /* This will hide the scrollbar on WebKit browsers (e.g., Chrome, Safari) */
                        },
                        scrollbarWidth: 'none' /* This will hide the scrollbar on Firefox */
                      }}
                    >
                      <Typography variant='p' fontSize={12} color={'#000'} style={{ padding: 0, margin: 0 }}>
                        {event.title}
                      </Typography>
                      <Divider sx={{ padding: '0', margin: '0' }} />
                      <Typography variant='p' fontSize={12} color={'#000'} style={{ padding: 0, margin: 0 }}>
                        {event.description}
                      </Typography>
                    </Box>
                  </Stack>
                </>
              ))
            ) : (
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                  marginLeft: '55px'
                }}
              >
                <Typography sx={{ fontWeight: '500', fontSize: '16px', color: '#8090a7' }}>
                  {t('No events to show')}
                </Typography>
              </Box>
            )}
          </Stack>
        </Box>
      </Drawer>
    )
  } else {
    return null
  }
}

export default SidebarLeft
