import React from 'react'
import { useState } from 'react'
import Box from '@mui/material/Box'
import Badge from '@mui/material/Badge'
import Avatar from '@mui/material/Avatar'
import Typography from '@mui/material/Typography'
import { useTranslation } from 'react-i18next'
import { Button,  IconButton, Input, InputAdornment, TextField } from '@mui/material'
import { Stack } from '@mui/system'
import { useForm, Controller, Form } from 'react-hook-form'
import { Visibility, VisibilityOff } from '@mui/icons-material'
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import useEditDetalisAdmin from './hook/useEditDetalisAdmin'



export default function ChangePassword({ handleClose, open, userInfo, userData }) {
  const { mutate: EditAdmin, isloading } = useEditDetalisAdmin()
  const [imagePreview, setImagePreview] = useState(null);
  const [selectedFile, setSelectedFile] = useState(null);
  const { t } = useTranslation()
  const [showPassword, setShowPassword] = useState(false);


  const defaultValues = {
    first_name: userData?.first_name || "",
    last_name: userData?.last_name || "",
    password: ""

  }

  const handleTogglePasswordVisibility = () => {
    setShowPassword((prevShowPassword) => !prevShowPassword);
  };

  const { control, handleSubmit, formState: { errors } } = useForm({
    defaultValues,
    mode: 'onBlur',
  });




  const handleImageUpload = () => {
    const fileInput = document.createElement('input');
    fileInput.type = 'file';
    fileInput.accept = 'image/*';
    fileInput.onchange = (event) => {
      const file = event.target.files[0];
      if (file) {
        setImagePreview(URL.createObjectURL(file));
        setSelectedFile(file);
      }
    };
    fileInput.click();
  };

  const onSubmit = async data => {
    try {
      const formData = new FormData();
      formData.append('image', selectedFile)
      formData.append('first_name', data.first_name);
      formData.append('last_name', data.last_name);
      formData.append('password', data.password);
      EditAdmin(formData);
    } catch (error) {
    }
  }

  return (

    <>


      <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle textAlign={'center'}>
          <Badge
            overlap="circular"
            sx={{ textAlign: 'center' }}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
          >
            <Avatar
              alt=""
              src={imagePreview || `${process.env.NEXT_PUBLIC_IMAGES}/${userInfo?.image}`}
              sx={{ width: '220px', height: '220px', textAlign: 'center' }}
            />
          </Badge>
          <Typography mt={'10px'}> </Typography>
          <Button onClick={handleImageUpload}>Change/Upload image</Button>



        </DialogTitle>
        <DialogContent>

          <form onSubmit={handleSubmit(onSubmit)}>
            <Box>
              <Typography className='NameInput '>{t("First Name")}</Typography>
              <Controller
                name={`first_name`}
                control={control}
                defaultValues=''
                render={({ field }) => (
                  <TextField
                    {...field}
                    fullWidth
                    type='name'
                    size='small'
                    placeholder={t("First name")}

                  />
                )}
              />
            </Box>
            <Box mt={'10px'}>
              <Typography className='NameInput '>{t("Last name")}</Typography>
              <Controller
                name={`last_name`}
                control={control}
                defaultValues=''

                render={({ field }) => (
                  <TextField
                    {...field}
                    fullWidth
                    type='name'
                    size='small'
                    placeholder={t("Last name")}

                  />
                )}
              />
            </Box>
            <Box mt={'10px'}>
              <Typography className='NameInput '>{t("Password")}</Typography>
              <Controller
                name={`password`}
                defaultValues=''
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    fullWidth
                    size='small'
                    placeholder={t("Password")}
                    type={showPassword ? 'text' : 'password'}


                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            onClick={handleTogglePasswordVisibility}
                            edge="end"
                          >
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                )}
              />
            </Box>
            <Stack direction={'row'} mt={'16px'} spacing={3}>

              <Box >
              <Button sx={{backgroundColor:"#6ab2df",padding:"12px 24px 12px 24px",color:"#fff","&:hover": {backgroundColor: "#6ab2df" },}} type="submit" >Save</Button>

              </Box>
              <Box >

              <Button sx={{backgroundColor:"#DCE1E6",padding:"12px 24px 12px 24px",color:"#8090A7","&:hover": {backgroundColor: "#DCE1E6" },}} onClick={handleClose}>Cancel</Button>
              </Box>

            </Stack>
          </form>


        </DialogContent>



      </Dialog>




    </>
  )
}
