import { request } from "src/utiltis/AxiosUtilitis";

const EditDetalisAdmin = async (payload) => {

  return request({
    url: `/api/Users/EditAdmin`,
    method: 'post',
    data: payload,
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

export default EditDetalisAdmin
