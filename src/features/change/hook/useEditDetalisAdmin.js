
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { showSuccesToast } from 'src/utiltis/toastSecces';
import EditDetalisAdmin from '../api/EditDetalisAdmin';

const useEditDetalisAdmin = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: EditDetalisAdmin,
    onSuccess: (data) => {
      queryClient.invalidateQueries("EditAdmin");
      showSuccesToast(data.data.message, "success")


    },
  });
}

export default useEditDetalisAdmin
