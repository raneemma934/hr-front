
import { useMutation, useQueryClient } from '@tanstack/react-query';
import DeleteComplaints from '../api/deletComplaints';
import { ShowErrorToast } from 'src/utiltis/showErrorToast';
import { showSuccesToast } from 'src/utiltis/toastSecces';

const useDeleteComplaints = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:DeleteComplaints,
    onSuccess: () => {
      queryClient.invalidateQueries("complaints");
      showSuccesToast()
    },
    onError: (data)=>{
      ShowErrorToast(data?.response?.status === '403' ? 'You cannot delete this request':data?.message,"")
    }
  });
}

export default useDeleteComplaints

