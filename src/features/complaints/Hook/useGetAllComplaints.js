
import { useQuery, useQueryClient } from '@tanstack/react-query'
import GetAllComplaints from '../api/getAllComplaints'

const useGetAllComplaints = (date) => {
  const queryClient = useQueryClient();

  const query = useQuery({
    queryKey: ['complaints', date],
    queryFn: () => GetAllComplaints(date),
  });

  return query
}

export default useGetAllComplaints


