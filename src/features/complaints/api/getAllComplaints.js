import { request } from "src/utiltis/AxiosUtilitis"

const GetAllComplaints = async (payload) => {
  return request({ url: `/api/Request/Complaints?date=${payload}` })
}

export default GetAllComplaints
