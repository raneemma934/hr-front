import { request } from '../../../../utiltis/AxiosUtilitis'

const GetAllData = async () => {
  return request({ url: '/api/attendances' })
}

export default GetAllData
