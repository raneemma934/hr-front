import { useMutation, useQueryClient } from '@tanstack/react-query'
import toast from 'react-hot-toast'
import UpdateAbsence from '../api/UpdateAbsence'
import { showSuccesToast } from 'src/utiltis/toastSecces'
import { ShowErrorToast } from 'src/utiltis/showErrorToast'

export const useUpdateAbsence = () => {
  const queryClient = useQueryClient()

  return useMutation({
    mutationFn: UpdateAbsence,
    onSuccess: data => {
      queryClient.invalidateQueries('UpdateAbsence')
      showSuccesToast('data?.data?.message', '')
    },
    onError(error) {
      ShowErrorToast('Add Contract ', data.message)
    }
  })
}
