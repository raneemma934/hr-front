
import { request } from "src/utiltis/AxiosUtilitis"

const GetAllAbsence = async (date) => {
  return request({ url: `/api/Absence/getUserAbsences?date=${date}` })
}

export default GetAllAbsence
