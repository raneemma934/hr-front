import { Button, Dialog, DialogActions, DialogTitle, Grid, TextField, Typography } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'

import Paper from '@mui/material/Paper'
import { useAddAbsence } from '../../hooks/useAddAbsence'

export default function PasswordModel({ open, setOpen, Data }) {
  const { mutate: AddAbsence } = useAddAbsence()

  const { t } = useTranslation()

  const handleDelete = () => {
    setConfirm(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleSubmit = event => {
    event.preventDefault()
    AddAbsence({ id: Data.id_abs, password: event.target[0].value, type: Data.justify, isPaid: Data.isPaid })
    handleClose()
  }

  return (
    <Dialog onClose={handleClose} open={open}>
      <Grid item xs={12}>
        <form onSubmit={handleSubmit}>
          <Paper>
            {!confirm ? (
              <Typography sx={{ fontWeight: '600', fontSize: '20px', color: '#131627' }}>{t('Delete')}</Typography>
            ) : null}
            {!confirm ? (
              <DialogTitle style={{ fontSize: '14px', color: '#3F4458' }}>
                {t('Are you sure you want to delete')} [{Branch && Branch && Branch.name}] ?{' '}
                {t(
                  'This action is irreversible and will permanently remove all associated data and configurations for this branch.'
                )}
              </DialogTitle>
            ) : (
              <DialogTitle style={{ fontSize: '14px', color: '#3F4458' }}>
                {t('Please, Enter your password to confirm .')}

                <TextField sx={{ marginTop: '3%' }} type='password' label='password' required fullWidth size='small' />
              </DialogTitle>
            )}
            <DialogActions style={{ display: 'flex', justifyContent: 'center', padding: '10px' }}>
              <Button onClick={handleClose} sx={{ backgroundColor: 'rgba(128, 144, 167,0.2)', color: '#8090A7' }}>
                {t('Cancel')}
              </Button>

              {!confirm ? (
                <Button
                  autoFocus
                  sx={{
                    color: '#fff',
                    backgroundColor: '#DF2E38',
                    border: '1px solid #DF2E38',
                    '&:hover': { color: '#fff', background: '#DF2E38' }
                  }}
                  onClick={handleDelete}
                >
                  {t('Delete')}
                </Button>
              ) : (
                <Button
                  autoFocus
                  type='submit'
                  sx={{
                    color: '#fff',
                    backgroundColor: '#DF2E38',
                    border: '1px solid #DF2E38',
                    '&:hover': { color: '#fff', background: '#DF2E38' }
                  }}
                >
                  Confirm
                </Button>
              )}
            </DialogActions>
          </Paper>
        </form>
      </Grid>
    </Dialog>
  )
}
