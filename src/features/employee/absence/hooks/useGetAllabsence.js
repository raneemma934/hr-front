import { useQuery, useQueryClient } from '@tanstack/react-query';
import GetAllAbsence from '../api/GetAllAbsence';

export const useGetAllAbsence = (date) => {
  const queryClient = useQueryClient();

  const query = useQuery({
    queryKey: ['Absence', date],
    queryFn: () => GetAllAbsence(date),
  });

  return query;
};
