export const AbsenceData = elements => {
  return elements?.data?.data?.map(element => {
    return {
      id: element?.id,
      name: element?.first_name,
      last_name: element?.last_name,
      paid_absences: element?.paid_absences,
      sick_absences: element?.sick_absences,
      un_paid_absences: element?.un_paid_absences,
      UnjustifiedUnPaid: element?.UnjustifiedUnPaid,
      UnjustifiedPaid: element?.UnjustifiedPaid,
      justifiedUnPaid: element?.justifiedUnPaid,
      justifiedPaid: element?.justifiedPaid,
      hours_num: element?.hours_num,
      unjustified: element?.userUnjustified,
      total: element?.all,
      user_info: element?.userinfo,
      specialization: element?.specialization,
      level: element?.userinfo?.level,
      Sick: element?.Sick,
      image: element?.user_info?.image,
      user_id: element?.user_info?.id
    }
  })
}
