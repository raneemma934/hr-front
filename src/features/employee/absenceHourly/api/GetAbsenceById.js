
import { request } from "src/utiltis/AxiosUtilitis"

const GetAllAbsenceById = async (id) => {
  return request({ url: `/api/Late/allUserLates?user_id=${id}` })
}

export default GetAllAbsenceById
