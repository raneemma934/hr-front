
import { request } from "src/utiltis/AxiosUtilitis"

const GetAllAbsence = async (date) => {
  return request({ url: `/api/Late/getUserLates?date=${date}` })
}

export default GetAllAbsence
