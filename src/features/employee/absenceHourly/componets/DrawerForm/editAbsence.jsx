import { Button, MenuItem, Stack, TextField, Typography } from '@mui/material'
import { Box } from '@mui/system'
import React, { useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import PasswordModel from './password'

export default function EditAbsence({ id }) {
  const { t } = useTranslation()
  const [paid, setPaid] = useState('1')
  const [justified, setJustified] = useState('justified')
  const [open, setOpen] = useState(false)
  const [Data, setData] = useState()

  const onSubmit = formData => {
    setData({
      ...formData,
      id_abs: id
    })
    setOpen(true)
  }
  const { control, handleSubmit, reset } = useForm()

  return (
    <Box>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Controller
          name={`isPaid`}
          defaultValue={'1'}
          control={control}
          sx={{ marginTop: '10px' }}
          render={({ field }) => (
            <TextField
              {...field}
              select
              fullWidth
              sx={{ marginTop: '10px' }}
              placeholder='paid'
              value={paid}
              SelectProps={{
                displayEmpty: true,
                onChange: e => {
                  field.onChange(e)
                  setPaid(e.target.value)
                }
              }}
              size='small'
            >
              <MenuItem value='1'>{t('Paid')}</MenuItem>
              <MenuItem value='0'>{t('Unpaid')}</MenuItem>
            </TextField>
          )}
        />

        <Controller
          name={`justify`}
          defaultValue={'justified'}
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              select
              fullWidth
              sx={{ marginTop: '10px' }}
              value={justified}
              SelectProps={{
                displayEmpty: true,
                onChange: e => {
                  field.onChange(e)
                  setJustified(e.target.value)
                }
              }}
              size='small'
            >
              <MenuItem value='justified'>{t('Justified')}</MenuItem>
              <MenuItem value='Unjustified'>{t('Unjustified')}</MenuItem>
              <MenuItem value='sick'>{t('Sick')}</MenuItem>
            </TextField>
          )}
        />
        <Stack sx={{ marginLeft: { sm: '50%' }, marginY: '15px' }} direction={'row'} spacing={2}>
          <Button type='submit' variant='contained' sx={{ mr: 4 }}>
            Add
          </Button>
          <Button variant='tonal' color='secondary'>
            Cancel
          </Button>
        </Stack>
      </form>
      <PasswordModel open={open} setOpen={setOpen} Data={Data} />
    </Box>
  )
}
