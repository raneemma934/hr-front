import React, { useEffect, useState } from 'react'
import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'
import Drawer from '@mui/material/Drawer'
import { Button, FormControl, FormControlLabel, Radio, RadioGroup, TextField, Typography } from '@mui/material'
import { useForm, useFieldArray } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import useViewGetAbsence from '../../hooks/useGetAbsenceById'
import { useDeleteAbsence } from '../../hooks/useDeletedAbsence'
import { Stack } from '@mui/system'
import { useDispatch } from 'react-redux'
import { useAddAbsence } from '../../hooks/useAddAbsence'
import EditAbsence from './editAbsence'
import moment from 'moment'

const drawerWidth = 440

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),

  ...theme.mixins.toolbar,
  justifyContent: 'flex-start'
}))

export default function DrawerForm({ open, setOpenParent, Data }) {
  const id = Data?.id

  const { t } = useTranslation()
  const dispatch = useDispatch()
  const { data, isLoading, isError } = useViewGetAbsence(id)

  const [value, setValue] = React.useState(null)

  const handleChange = event => {
    setValue(event)
  }
  const [fdata, setfdata] = useState('')

  const { mutate: deleteAbcence } = useDeleteAbsence()

  const [fndata, setfndata] = useState('')
  const [sickdata, setSickdata] = useState('')

  const [deleteDialog, setDeleteDialog] = useState(false)
  const [indexDelete, setIndexDelete] = useState()
  const [typeDelete, setTypeDelete] = useState()

  const handleDrawerClose = () => {
    setOpenParent(false)
  }

  const defaultValues = {
    absence: [{ type: '', date: '' }]
  }
  const [selectedTypes, setSelectedTypes] = useState(Array.from({ length: defaultValues.absence.length }, () => ''))
  const [selectedDates, setSelectedDates] = useState(Array.from({ length: defaultValues.absence.length }, () => ''))

  const handleTypeChange = (e, index) => {
    const newSelectedTypes = [...selectedTypes]
    newSelectedTypes[index] = e.target.value
    setSelectedTypes(newSelectedTypes)
  }

  // Handle change for date field
  const handleDateChange = (e, index) => {
    const newSelectedDates = [...selectedDates]
    newSelectedDates[index] = e.target.value
    setSelectedDates(newSelectedDates)
  }

  const handleDate = e => {
    const date = e.target.value

    let searchData
    if (!date) {
      setfdata(data?.data?.data?.un_paid_lates)
    } else {
      searchData = data?.data?.data?.un_paid_lates?.filter(element => element.lateDate == date)
      setfdata(searchData)
    }
  }

  const handleDateN = e => {
    const date = e.target.value

    let searchData
    if (!date) {
      setfndata(data?.data?.data?.paid_lates)
    } else {
      searchData = data?.data?.data?.paid_lates?.filter(element => element.lateDate === date)
      setfndata(searchData)
    }
  }

  const handleSickDate = e => {
    const date = e.target.value

    let searchData
    if (!date) {
      setSickdata(data?.data?.data?.Sick)
    } else {
      searchData = data?.data?.data?.Sick?.filter(element => element.startDate === date)
      setSickdata(searchData)
    }
  }

  useEffect(() => {
    setfdata(data?.data?.data?.un_paid_lates)
    setfndata(data?.data?.data.paid_lates)
    setSickdata(data?.data?.data?.Sick)

    reset(defaultValues)
  }, [data?.data?.data?.un_paid_lates, data?.data?.data.paid_lates])

  const { control, handleSubmit, reset } = useForm({
    defaultValues: {
      absence: Array.from({ length: 1 }, () => ({ type: '', date: '' }))
    },
    mode: 'onBlur'
  })

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'absence'
  })

  const { mutate: AddAbsence } = useAddAbsence()

  const onSubmit = formData => {
    const formDataCopy = { ...formData }

    formDataCopy.absence.forEach((element, index) => {
      element.type = selectedTypes[index]
      element.date = selectedDates[index]
      element.user_id = id
    })
    AddAbsence(formDataCopy)
  }

  const formatDecimalToTime = decimalValue => {
    const hours = Math.floor(decimalValue)
    const remainingMinutes = (decimalValue - hours) * 60
    const time = moment().startOf('day').add(hours, 'hours').add(remainingMinutes, 'minutes')

    return time.format('HH:mm')
  }

  const handleDelete = params => {
    deleteAbcence(params?.id)
    handleCloseDeleteDialog()
  }

  const handleDeleteAbsence = (index, type) => {
    setTypeDelete(type)
    setDeleteDialog(true)
    setIndexDelete(index)
  }

  const handleCloseDeleteDialog = _ => {
    setDeleteDialog(false)
  }

  return (
    <Drawer
      sx={{
        flexShrink: 0,
        '& .MuiDrawer-paper': {
          width: { xl: drawerWidth, md: drawerWidth, sm: drawerWidth, xs: '90%' },
          overflowX: 'hidden'
        },
        overflow: 'visible',
        borderRadius: '15px'
      }}
      anchor='right'
      open={open}
      variant='temporary'
      ModalProps={{
        keepMounted: true
      }}
      onClose={handleDrawerClose}
    >
      <Box
        sx={{
          width: '100%',
          backgroundColor: '#DCE1E6',
          fontSize: '20px',
          gap: '10px',
          padding: '15px',
          borderRadius: '10px'
        }}
      >
        {t('Edit Absence')}
      </Box>
      {/* Paid */}
      <Stack marginLeft={{ sm: '3%' }} marginTop={{ sm: '2%' }} direction={{ sm: 'column' }} spacing={3}>
        <Typography marginTop={4} sx={{ fontSize: '16px', fontWeight: 600 }}>
          {t('Paid')}
        </Typography>
        <Box width={{ sm: '94%' }}>
          <TextField onChange={handleDateN} fullWidth type='date' size='small' />
        </Box>
      </Stack>
      <Stack direction={'column'} marginTop={'2%'} style={{ padding: '15px' }}>
        {fndata &&
          fndata?.map((date, index) => (
            <>
              <Stack direction='row' justifyContent={'space-between'} alignItems='center'>
                <Typography>{date?.type_ar}</Typography>
                <Typography>{date?.lateDate}</Typography>
                <Typography>{date?.hours_num + ' m'}</Typography>
                <FormControl sx={{ marginLeft: '25px' }}>
                  <RadioGroup
                    aria-labelledby='demo-controlled-radio-buttons-group-Paid'
                    name='controlled-radio-buttons-group-Paid'
                    value={value === date.id ? 'selected' : ''}
                    onChange={() => handleChange(date.id)}
                  >
                    <FormControlLabel value='selected' control={<Radio />} />
                  </RadioGroup>
                </FormControl>
              </Stack>
              {value === date.id && (
                <Box>
                  <EditAbsence id={value} />
                </Box>
              )}
            </>
          ))}
      </Stack>

      {/* un_paid_lates */}
      <Stack marginLeft={{ sm: '3%' }} marginTop={{ sm: '2%' }} direction={{ sm: 'column' }} spacing={3}>
        <Typography marginTop={4} sx={{ fontSize: '16px', fontWeight: 600 }}>
          {t('un_paid_lates')}
        </Typography>
        <Box width={{ sm: '94%' }}>
          <TextField fullWidth onChange={handleDate} type='date' size='small' />
        </Box>
      </Stack>
      <Stack direction={'column'} marginTop={'2%'} style={{ padding: '15px' }}>
        {fdata &&
          fdata?.map((date, index) => (
            <>
              <Stack direction='row' justifyContent={'space-between'} alignItems='center'>
                <Box sx={{ display: 'flex' }}>
                  <Typography>{date?.type_ar}</Typography>
                  <Typography sx={{ marginX: '12px' }}>{date?.lateDate}</Typography>
                  <Typography>{formatDecimalToTime(date?.hours_num)}</Typography>
                </Box>
                <FormControl sx={{ marginLeft: '25px' }}>
                  <RadioGroup
                    aria-labelledby='demo-controlled-radio-buttons-group-Paid'
                    name='controlled-radio-buttons-group-Paid'
                    value={value === date.id ? 'selected' : ''}
                    onChange={() => handleChange(date.id)}
                  >
                    <FormControlLabel value='selected' control={<Radio />} />
                  </RadioGroup>
                </FormControl>
              </Stack>
              {value === date.id && (
                <Box>
                  <EditAbsence id={value} />
                </Box>
              )}
            </>
          ))}
      </Stack>

      {/* sick */}
      <Stack marginLeft={{ sm: '3%' }} marginTop={{ sm: '2%' }} direction={{ sm: 'column' }} spacing={3}>
        <Typography marginTop={4} sx={{ fontSize: '16px', fontWeight: 600 }}>
          {t('Sick')}
        </Typography>
        <Box width={{ sm: '94%' }}>
          <TextField fullWidth onChange={handleSickDate} type='date' size='small' />
        </Box>
      </Stack>
      <Stack direction={'column'} marginTop={'2%'} style={{ padding: '15px' }}>
        {sickdata &&
          sickdata?.map((date, index) => (
            <>
              <Stack direction='row' justifyContent={'space-between'} alignItems='center'>
                <Box sx={{ display: 'flex' }}>
                  <Typography>{date?.type_ar}</Typography>
                  <Typography sx={{ marginX: '12px' }}>{date?.lateDate}</Typography>
                  <Typography>{formatDecimalToTime(date?.hours_num)}</Typography>
                </Box>
                <FormControl sx={{ marginLeft: '25px' }}>
                  <RadioGroup
                    aria-labelledby='demo-controlled-radio-buttons-group-Paid'
                    name='controlled-radio-buttons-group-Paid'
                    value={value === date.id ? 'selected' : ''}
                    onChange={() => handleChange(date.id)}
                  >
                    <FormControlLabel value='selected' control={<Radio />} />
                  </RadioGroup>
                </FormControl>
              </Stack>
              {value === date.id && (
                <Box>
                  <EditAbsence id={value} />
                </Box>
              )}
            </>
          ))}
      </Stack>
      <Stack sx={{ marginLeft: { sm: '50%' }, marginY: '15px' }} direction={'row'} spacing={2}>
        <Button variant='tonal' color='secondary' onClick={handleDrawerClose}>
          {t('Cancel')}
        </Button>
      </Stack>
    </Drawer>
  )
}
