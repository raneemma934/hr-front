// Import necessary components and libraries
import React, { useEffect, useMemo, useState } from 'react'
import Stack from '@mui/material/Stack'
import Box from '@mui/material/Box'
import { Avatar, Typography } from '@mui/material'

import { useTranslation } from 'react-i18next'
import DrawerForm from '../componets/DrawerForm/index'
import Link from 'next/link'
import EditIcon from '../../../../../public/images/IconInput/edit'
import IconButton from '@mui/material/IconButton'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'

const useAbsenceColumns = () => {
  const [isDrawerOpenEdit, setIsDrawerOpenEdit] = useState(false)

  const [EditData, setEditData] = useState(null)
  const [openRows, setOpenRows] = useState({})

  const [open, setOpen] = useState(false)

  const handleToggle = id => {
    setOpenRows(prevOpenRows => ({
      ...prevOpenRows,
      [id]: !prevOpenRows[id]
    }))
  }

  const { t } = useTranslation()

  const handleOpenClick = row => {
    setIsDrawerOpenEdit(true)
    setEditData(row)
  }

  const columnStyles = {
    cell: {
      marginLeft: '10%'
    }
  }

  return useMemo(() => [
    {
      field: '',
      headerName: t('Employee'),
      disableClickEventBubbling: true,
      flex: 3,
      renderCell: params => {
        return (
          <Link style={{ textDecoration: 'none' }} href={`/profile/${params?.row?.user_id}`}>
            <Stack direction={'row'} alignItems={'center'}>
              <Avatar
                sx={{ width: '36px', height: '36px' }}
                src={process.env.NEXT_PUBLIC_IMAGES + '/' + params?.row?.image}
                alt=''
              />
              <Stack direction={'column'} marginLeft={'8px'}>
                <Typography className='custome-data-grid-font'>
                  {params?.row?.name} {params?.row?.last_name}
                </Typography>
                <Typography className='custome-data-grid-font2'>{params?.row?.specialization}</Typography>
              </Stack>
            </Stack>
          </Link>
        )
      }
    },

    // {
    //   field: 'level',
    //   headerName: t("Level"),
    //   disableClickEventBubbling: true,
    //   flex: 1.1,

    // },
    {
      field: 'justified',
      headerName: t('paid'),
      disableClickEventBubbling: true,
      flex: 2,
      renderCell: params => (
        <>
          <Typography className='custome-data-grid-font'>{params?.row?.paidHours + ' ' + 'ساعة'}</Typography>

          <Box onClick={() => handleToggle(params.row.id)}>
            <IconButton
              onClick={() => setOpen(!openRows)}
              sx={{
                fontSize: '13px',
                color: '#8090A7',
                '&:hover': {
                  background: 'none !important'
                },
                padding: 0
              }}
              disableRipple
            >
              {openRows[params.row.id] ? (
                <Stack direction={'column'}>
                  <Box>
                    <KeyboardArrowUpIcon sx={{ marginTop: '53px' }} />
                  </Box>
                  <>
                    <Stack direction={'row'} spacing={2}>
                      <Typography sx={{ fontSize: '14px' }}>{t('justified')}</Typography>
                      <Typography sx={{ fontSize: '14px' }}>{params?.row?.Paidj}</Typography>
                    </Stack>
                    <Stack direction={'row'} spacing={2}>
                      <Typography sx={{ fontSize: '14px' }}>{t('Unjustified')}</Typography>
                      <Typography sx={{ fontSize: '14px' }}>{params?.row?.Paidunj}</Typography>
                    </Stack>
                  </>
                </Stack>
              ) : (
                <KeyboardArrowDownIcon sx={{ marginTop: '3px' }} />
              )}
            </IconButton>
          </Box>
        </>
      )
    },
    {
      field: 'unjustified',
      headerName: t('Unpaid'),
      disableClickEventBubbling: true,
      flex: 2,
      renderCell: params => (
        <>
          <Typography className='custome-data-grid-font'>{params?.row?.unPaidHours + ' ' + 'ساعة'}</Typography>

          <Box onClick={() => handleToggle(params.row.id)}>
            <IconButton
              onClick={() => setOpen(!openRows)}
              sx={{
                fontSize: '13px',
                color: '#8090A7',
                '&:hover': {
                  background: 'none !important'
                },
                padding: 0
              }}
              disableRipple
            >
              {openRows[params.row.id] ? (
                <Stack direction={'column'}>
                  <Box>
                    <KeyboardArrowUpIcon sx={{ marginTop: '53px' }} />
                  </Box>

                  <>
                    <Stack direction={'row'} spacing={2}>
                      <Typography sx={{ fontSize: '14px' }}>{t('justified')}</Typography>
                      <Typography sx={{ fontSize: '14px' }}>{params?.row?.UnPaidUNj}</Typography>
                    </Stack>
                    <Stack direction={'row'} spacing={2}>
                      <Typography sx={{ fontSize: '14px' }}>{t('Unjustified')}</Typography>
                      <Typography sx={{ fontSize: '14px' }}>{params?.row?.UnPaidJ}</Typography>
                    </Stack>
                  </>
                </Stack>
              ) : (
                <KeyboardArrowDownIcon sx={{ marginTop: '3px' }} />
              )}
            </IconButton>
          </Box>
        </>
      )
    },
    {
      field: 'sick',
      headerName: t('SICK'),
      disableClickEventBubbling: true,
      flex: 2,
      renderCell: params => (
        <>
          <Typography className='custome-data-grid-font'>{params?.row?.sick_absences?.length}</Typography>
        </>
      )
    },
    {
      field: 'action',
      headerName: t('Action'),
      flex: 1,

      renderCell: params => {
        return (
          <>
            <Stack direction={{ sm: 'row' }} style={{ ...columnStyles.cell }}>
              <Box sx={{ cursor: 'pointer' }} onClick={() => handleOpenClick(params.row)}>
                <EditIcon
                  style={{
                    color: '#8090A7'
                  }}
                  variant='contained'
                  color='primary'
                  size='small'
                />
              </Box>
            </Stack>
            {isDrawerOpenEdit && (
              <DrawerForm open={isDrawerOpenEdit} setOpenParent={setIsDrawerOpenEdit} Data={EditData} />
            )}
          </>
        )
      }
    }
  ])
}

export default useAbsenceColumns
