export const AbsenceData = elements => {
  return elements?.data?.data?.map(element => {
    console.log('🚀 ~ AbsenceData ~ element:', element)

    return {
      id: element?.id,
      name: element?.first_name,
      last_name: element?.last_name,
      UnPaidUNj: Number(element?.UnjustifiedUnpaid).toFixed(0),
      Paidj: Number(element?.UnjustifiedPaid).toFixed(0),
      UnPaidJ: Number(element?.justifiedUnpaid).toFixed(0),
      Paidunj: Number(element?.justifiedPaid).toFixed(0),
      paidHours: (element?.UnjustifiedPaid + element?.justifiedPaid).toFixed(0),
      unPaidHours: (element?.UnjustifiedUnpaid + element?.justifiedUnpaid).toFixed(0),
      user_id: element?.user_info?.id,
      total: element?.all,
      image: element?.user_info?.image,
      specialization: element?.specialization,
      level: element?.userinfo?.level
    }
  })
}
