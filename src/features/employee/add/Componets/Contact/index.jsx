import { Button, Card, CardContent, List, ListItem, TextField, Typography, InputAdornment } from '@mui/material';
import { Box, Stack, styled } from '@mui/system';
import { useFieldArray, useForm } from 'react-hook-form';
import React, { useEffect, useState } from 'react';
import ListItemSelected from 'src/views/components/list/ListItemSelected';
import IconButton from 'src/@core/theme/overrides/icon-button';
import email from 'src/store/apps/email';
import { useTranslation } from 'react-i18next';
import { ContactSchema } from 'src/pages/employees/add/validation';
import CloseIcon from '@mui/icons-material/Close';
import CustomTextField from 'src/@core/components/mui/text-field';


export default function Contact({ onDataChange, Controller, control, defaultValues, errors }) {
  const { t } = useTranslation();


  const {
    fields: phoneNumbersFields,
    append: appendPhoneNumber,
    remove: removePhoneNumber,
  } = useFieldArray({
    control,
    name: 'contacts.phones',
  });

  const {
    fields: emailFields,
    append: appendEmail,
    remove: removeEmail,
  } = useFieldArray({
    control,
    name: 'contacts.emails',
  });


  return (
    <Card>
      <CardContent>
        <Typography className='title-section'>{t('Contact')}</Typography>
        <br />

        <Stack sx={{ padding: '0' }} direction={'column'} spacing={2} width={'100%'}>
          <Box>
            <Box sx={{ margin: '5px 0px' }}>
              <Controller
                name={`address`}
                control={control}
                render={({ field }) => (
                  <CustomTextField
                    {...field}
                    title={t(`address`)}
                    variant='outlined'
                    fullWidth
                    size='small'
                    error={Boolean(errors.address)}
                    helperText={errors.address?.message}
                  />
                )}
              />
                {phoneNumbersFields.map((field, indexPhone) => (
         <Controller
         key={indexPhone}
         name={`contacts.phones.${indexPhone}`}
         control={control}
         render={({ field }) => (
           <CustomTextField
             {...field}
             title={`phones ${indexPhone + 1}`}
             variant='outlined'
             fullWidth
             size='small'
             error={Boolean(errors?.contacts?.[labelKey]?.[index]?.[inputKey])}
             helperText={errors?.contacts?.[labelKey]?.[index]?.[inputKey]?.message}
             InputProps={{
              endAdornment: (
                <InputAdornment position='end'>
                  {indexPhone != 0 && (
                    <CloseIcon
                      style={{ cursor: 'pointer', color: 'red', padding: '1', margin: '0px 0px' }}
                      onClick={() => removePhoneNumber(indexPhone)}
                    />
                  )}
                </InputAdornment>
              )
            }}

           />
         )}
       />



            ))}

<Typography sx={{ marginRight: "30px", fontSize: "14px", color: "#6ab2df", cursor: "pointer" }} onClick={() => appendPhoneNumber()}>
              + {t("add")}
            </Typography>
            {emailFields.map((field, indexEmail) => (
              <>
         <Controller
         key={indexEmail}
         name={`contacts.emails.${indexEmail}`}
         control={control}
         render={({ field }) => (
           <CustomTextField
             {...field}
             title={`email ${indexEmail + 1}`}
             variant='outlined'
             fullWidth
             size='small'
             error={Boolean(errors?.contacts?.[labelKey]?.[index]?.[inputKey])}
             helperText={errors?.contacts?.[labelKey]?.[index]?.[inputKey]?.message}
             InputProps={{
              endAdornment: (
                <InputAdornment position='end'>
                  {indexEmail != 0 && (
                    <CloseIcon
                      style={{ cursor: 'pointer', color: 'red', padding: '1', margin: '0px 0px' }}
                      onClick={() => removeEmail(indexEmail)}
                    />
                  )}
                </InputAdornment>
              )
            }}

           />
         )}
       />


     </>
            ))}

<Typography sx={{ marginRight: "30px", fontSize: "14px", color: "#6ab2df", cursor: "pointer" }} onClick={() => appendEmail()}>
              + {t("add")}
            </Typography>
            </Box>
          </Box>

        </Stack>
      </CardContent>
    </Card>
  );
}
