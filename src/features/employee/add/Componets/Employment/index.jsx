import { Button, Card, CardContent, InputAdornment, MenuItem, Rating, TextField, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import { useEffect, useState } from 'react'
import Avatar from 'src/@core/components/mui/avatar'
import { useFieldArray, useForm, Controller } from 'react-hook-form'
import CloseIcon from '@mui/icons-material/Close'
import { useTranslation } from 'react-i18next'
import AttachmentIcon from '@mui/icons-material/Attachment'

// import ContractIcon from '../../../public/images/IconInput/contract'
import ContractIcon from '../../../../../../public/images/IconInput/contract'
import PDFViewer from 'src/features/Contracts/list/Profile/PdfViwer'

export default function Employment({ onDataChange, ShowUser, Controller, control, errors, SetcontractFile }) {
  const { t } = useTranslation()
  const date = new Date()
  const [fileName, setFileName] = useState('')

  const day = date.getDate().toString().padStart(2, '0')
  const month = (date.getMonth() + 1).toString().padStart(2, '0')
  const year = date.getFullYear()

  const formattedDate = `${day}-${month}-${year}`

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'secretaraits'
  })

  const handleAddClick = () => {
    append('secretaraits', { object: '', delivery_date: '', title: '' })
  }

  const handleRemoveClick = index => {
    remove(index)
  }

  useEffect(() => {
    append('secretaraits', { startTime: '', endTime: '', path: '' })
  }, [])

  const handleFileChange = event => {
    const file = event?.target?.files?.[0]
    SetcontractFile(file)
    setFileName(file.name)
  }

  const SvgDate = `
  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
  <path d="M14.1667 2.5L14.1667 5.83333" stroke="#8090A7" stroke-width="1.2" stroke-linecap="round"/>
  <path d="M5.83325 2.5L5.83325 5.83333" stroke="#8090A7" stroke-width="1.2" stroke-linecap="round"/>
  <path d="M2.5 9C2.5 7.11438 2.5 6.17157 3.08579 5.58579C3.67157 5 4.61438 5 6.5 5H13.5C15.3856 5 16.3284 5 16.9142 5.58579C17.5 6.17157 17.5 7.11438 17.5 9V9.16667H2.5V9Z" stroke="#8090A7" stroke-width="1.2"/>
  <rect x="2.5" y="5" width="15" height="12.5" rx="2" stroke="#8090A7" stroke-width="1.2"/>
  <path d="M5 12.5H8.33333" stroke="#8090A7" stroke-width="1.2" stroke-linecap="round"/>
  <path d="M11.6667 12.5H15.0001" stroke="#8090A7" stroke-width="1.2" stroke-linecap="round"/>
  <path d="M5 15H8.33333" stroke="#8090A7" stroke-width="1.2" stroke-linecap="round"/>
  <path d="M11.6667 15H15.0001" stroke="#8090A7" stroke-width="1.2" stroke-linecap="round"/>
</svg>
  `

  const SvgSalary = `
  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
  <path d="M10.7174 9.32258C9.0164 8.76422 8.53878 7.77972 8.6468 7.10564C8.74426 6.49514 9.3541 5.87826 10.4952 5.87826C11.9644 5.87826 12.2748 7.27086 12.3056 7.43498C12.3751 7.82218 12.7623 8.0721 13.13 8.01072C13.5185 7.94252 13.7402 7.5727 13.6723 7.18506C13.5251 6.34554 12.8609 4.84228 11.1009 4.51602V3.28138C11.1009 2.88736 10.8349 2.56836 10.4409 2.56836C10.0467 2.56836 9.7809 2.88736 9.7809 3.28138V4.52042C8.4609 4.76748 7.44824 5.66926 7.25486 6.8808C7.05862 8.1084 7.7272 9.83914 10.281 10.6773C11.6238 11.1182 12.3512 11.8242 12.2328 12.5678C12.1312 13.2069 11.359 13.8698 10.2464 13.8698C8.34012 13.8698 8.20328 12.8322 8.1958 12.6373C8.1958 12.2433 7.87658 11.9245 7.48234 11.9245C7.08832 11.9245 6.78516 12.2433 6.78516 12.6373C6.78516 13.4986 7.3609 15.08 9.7809 15.2756V16.2772C9.7809 16.6715 10.0467 16.9902 10.4409 16.9902C10.8349 16.9902 11.1009 16.6715 11.1009 16.2772V15.186C12.4209 14.878 13.4155 13.9507 13.5997 12.7918C13.6903 12.2321 13.7801 10.3264 10.7174 9.32258Z" fill="#8090A7"/>
</svg>`

  const SvgContracts = `
<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M9.78397 4.51026C9.78397 4.33175 9.64265 4.19043 9.46414 4.19043H5.7005C4.5848 4.19043 3.66992 5.09787 3.66992 6.22101V13.8301C3.66992 14.9458 4.57736 15.8532 5.7005 15.8532H13.9418C15.0575 15.8532 15.965 14.9458 15.965 13.8301V10.8103C15.965 10.6318 15.8236 10.4904 15.6451 10.4904C15.4666 10.4904 15.3253 10.6318 15.3253 10.8103V13.8301C15.3253 14.5962 14.7005 15.221 13.9344 15.221H5.70794C4.94182 15.221 4.31703 14.5962 4.31703 13.8301V6.22101C4.31703 5.45489 4.94182 4.8301 5.70794 4.8301H9.47157C9.65009 4.8301 9.79141 4.68878 9.79141 4.5177L9.78397 4.51026Z" fill="#8090A7" stroke="#8090A7" stroke-width="0.5" stroke-miterlimit="10"/>
<path d="M12.1718 12.4319C12.276 12.4319 12.3652 12.3798 12.4247 12.298L13.3396 11.0261C13.3768 10.974 13.3991 10.9071 13.3991 10.8401V4.2426C13.3991 3.84094 13.0718 3.51367 12.6702 3.51367H11.6586C11.257 3.51367 10.9297 3.84094 10.9297 4.2426V10.8476C10.9297 10.9145 10.952 10.9814 10.9892 11.0335L11.9041 12.3054C11.9636 12.3872 12.0603 12.4393 12.1644 12.4393L12.1718 12.4319ZM11.5694 4.2426C11.5694 4.19053 11.614 4.1459 11.6661 4.1459H12.6776C12.7297 4.1459 12.7743 4.19053 12.7743 4.2426V10.7434L12.1718 11.5765L11.5694 10.7434V4.2426Z" fill="#8090A7" stroke="#8090A7" stroke-width="0.5" stroke-miterlimit="10"/>
<path d="M9.07795 7.39697H6.05812C5.8796 7.39697 5.73828 7.53829 5.73828 7.71681C5.73828 7.89532 5.8796 8.03664 6.05812 8.03664H9.07795C9.25646 8.03664 9.39779 7.89532 9.39779 7.71681C9.39779 7.53829 9.25646 7.39697 9.07795 7.39697Z" fill="#8090A7" stroke="#8090A7" stroke-width="0.5" stroke-miterlimit="10"/>
<path d="M9.07795 9.5166H6.05812C5.8796 9.5166 5.73828 9.65792 5.73828 9.83644C5.73828 10.0149 5.8796 10.1563 6.05812 10.1563H9.07795C9.25646 10.1563 9.39779 10.0149 9.39779 9.83644C9.39779 9.65792 9.25646 9.5166 9.07795 9.5166Z" fill="#8090A7" stroke="#8090A7" stroke-width="0.5" stroke-miterlimit="10"/>
</svg>`

  return (
    <Card>
      <CardContent>
        <Typography className='title-section'>{t('Employment')}</Typography>
        <br />

        <Stack direction={'column'} spacing={3} width={'100%'}>
          <Typography>{t('Salary')}</Typography>
          <Controller
            name={`salary`}
            control={control}
            render={({ field }) => (
              <TextField
                {...field}
                fullWidth
                error={Boolean(errors.salary)}
                {...(errors.salary && { helperText: errors.salary.message })}
                InputProps={{
                  endAdornment: <InputAdornment position='end'>{t('L.S')}</InputAdornment>
                }}
                size='small'
                label={
                  <Stack direction={'row'} spacing={2}>
                    <Box>{t('Salary')}</Box>
                  </Stack>
                }
              />
            )}
          />

          <Typography>{t('Start date')}</Typography>
          <Controller
            name={`start_date`}
            control={control}
            render={({ field }) => (
              <TextField
                {...field}
                fullWidth
                size='small'
                type='date'
                error={!!errors.start_date}
                helperText={
                  errors.start_date ? errors.start_date.message : 'الرجاء إدخال التاريخ بالشكل الصحيح : 03-02-2018'
                }
                label={
                  <Stack direction={'row'} spacing={2}>
                    <Box>
                      <img src={`data:image/svg+xml;utf8,${encodeURIComponent(SvgDate)}`} />
                    </Box>
                    <Box>{t('Start date')}</Box>
                  </Stack>
                }
              />
            )}
          />

          <Typography> {t('Contract')}</Typography>

          {/* <Controller
                name={`contract.path`}
                control={control}

                // defaultValue={field.file}
                render={({ field }) => (

                  <TextField
                    {...field}
                    variant="outlined"
                    fullWidth
                    size="small"
                    type="file"
                    accept=".pdf"
   />
            )}
          /> */}
          <div>
            <Controller
              name='cv'
              control={control}
              render={({ field: { onChange, ...fieldProps } }) => (
                <>
                  <TextField
                    {...fieldProps}
                    variant='outlined'
                    fullWidth
                    size='small'
                    type='file'
                    accept='.pdf'
                    onChange={event => {
                      handleFileChange(event) // معالجة تحميل الملف
                      onChange(event.target.files) // تحديث `react-hook-form`
                    }}
                  />
                </>
              )}
            />

            <p>
              العقد القديم : <a href={process.env.NEXT_PUBLIC_IMAGES + '/' + ShowUser}> {ShowUser} </a>
            </p>
          </div>

          {/* <Controller
            name={`contract.startTime`}
            control={control}
            render={({ field }) => (
              <TextField
                {...field}
                fullWidth
                size='small'
                error={!!errors.contract?.startTime}
                helperText={
                  errors.contract?.startTime
                    ? errors.contract.startTime.message
                    : 'الرجاء إدخال التاريخ بالشكل الصحيح : 03-02-2018'
                }
                label={
                  <Stack direction={'row'} spacing={2}>
                    <Box>
                      <img src={`data:image/svg+xml;utf8,${encodeURIComponent(SvgDate)}`} />
                    </Box>
                    <Box>{t('تاريخ بداية العقد')}</Box>
                  </Stack>
                }
              />
            )}
          /> */}
{/*
          <Controller
            name={`contract.endTime`}
            control={control}
            render={({ field }) => (
              <TextField
                {...field}
                fullWidth
                size='small'
                label={
                  <Stack direction={'row'} spacing={2}>
                    <Box>
                      <img src={`data:image/svg+xml;utf8,${encodeURIComponent(SvgDate)}`} />
                    </Box>
                    <Box>{t('End date')}</Box>
                  </Stack>
                }
              />
            )}
          /> */}

        </Stack>
      </CardContent>
    </Card>
  )
}
