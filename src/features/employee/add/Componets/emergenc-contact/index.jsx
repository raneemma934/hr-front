import React from 'react';
import { Button, Card, CardContent, List, ListItem, TextField, Typography, Box, Stack, InputAdornment } from '@mui/material';
import { useFieldArray, Controller } from 'react-hook-form';
import CloseIcon from '@mui/icons-material/Close';
import { useTranslation } from 'react-i18next';
import CustomTextField from 'src/@core/components/mui/text-field';

export default function EmergencyContact({ onDataChange, control, errors }) {
  const { t } = useTranslation();

  const { fields, append, remove } = useFieldArray({
    control, // Pass the control from useForm
    name: 'emergency_contacts', // Name of the field array
  });

  const { fields:emailField, append:appendedEmail, remove:removeEmail} = useFieldArray({
    control, // Pass the control from useForm
    name: 'emails', // Name of the field array
  });

  const { fields:phonesField, append:appendedPhones, remove:removePhones} = useFieldArray({
    control, // Pass the control from useForm
    name: 'phones', // Name of the field array
  });

  return (
    <Card>
      <CardContent sx={{ padding: "0px !important" }}>
      <Stack direction={"row"} sx={{padding:"0px"}} alignItems={"center"} justifyContent={"space-between"}>
          <Box>
            <Typography sx={{marginLeft:"30px",marginTop:"9px",fontWeight:"600",fontSize:"20px",color:"#8090a7"}} >{t("Emergency Contacts")}</Typography>
          </Box>
          <Box>
          <Typography sx={{marginRight:"30px",marginTop:"9px",fontWeight:"600",fontSize:"16px",color:"#6ab2df",cursor:"pointer"}} onClick={() => append({ name: '', address: '' })}>+ {t("add")}</Typography>

          </Box>
        </Stack>
        <List sx={{ lineHeight: "50px", margin: "0px 20px" }}>

        {fields.map((field, index) => (
        <div key={index}>

          <Typography sx={{textAlign:"end",marginRight:"10px",cursor:"pointer"}}>
<CloseIcon sx={{ color:"#8090A7",'&:hover': { color: 'red' }}} onClick={() => remove(index)} />
</Typography>
         <Controller
          name={`emergency_contacts.${index}.name`}
          control={control}
          render={({ field }) => (
            <CustomTextField
              {...field}
              title={`name ${index + 1}`}
              variant='outlined'
              fullWidth
              size='small'
              error={Boolean(errors?.contacts?.[labelKey]?.[index]?.[inputKey])}
              helperText={errors?.contacts?.[labelKey]?.[index]?.[inputKey]?.message}

            />
          )}
        />
           <Controller
          name={`emergency_contacts.${index}.address`}
          control={control}
          render={({ field }) => (
            <CustomTextField
              {...field}
              title={`address ${index + 1}`}
              variant='outlined'
              fullWidth
              size='small'
              error={Boolean(errors?.contacts?.[labelKey]?.[index]?.[inputKey])}
              helperText={errors?.contacts?.[labelKey]?.[index]?.[inputKey]?.message}

            />
          )}
        />

            {emailField.map((fieldEmail, indexEmail) => (
              <>
         <Controller
         key={indexEmail}
         name={`emergency_contacts.${index}.emails.${indexEmail}`}
         control={control}
         render={({ field }) => (
           <CustomTextField
             {...field}
             title={`email ${indexEmail + 1}`}
             variant='outlined'
             fullWidth
             size='small'
             error={Boolean(errors?.contacts?.[labelKey]?.[index]?.[inputKey])}
             helperText={errors?.contacts?.[labelKey]?.[index]?.[inputKey]?.message}
             InputProps={{
              endAdornment: (
                <InputAdornment position='end'>
                  {indexEmail != 0 && (
                    <CloseIcon
                      style={{ cursor: 'pointer', color: 'red', padding: '1', margin: '0px 0px' }}
                      onClick={() => removeEmail(indexEmail)}
                    />
                  )}
                </InputAdornment>
              )
            }}
           />
         )}
       />


     </>
            ))}
              <Typography sx={{ marginRight: "30px", fontSize: "14px", color: "#6ab2df", cursor: "pointer" }} onClick={() => appendedEmail({ email: '' })}>
              + {t("add")}
            </Typography>

            {phonesField.map((phone, indexPhone) => (
              <>
         <Controller
         key={indexPhone}
         name={`emergency_contacts.${index}.phones.${indexPhone}`}
         control={control}
         render={({ field }) => (
           <CustomTextField
             {...field}
             title={`phones ${indexPhone + 1}`}
             variant='outlined'
             fullWidth
             size='small'
             error={Boolean(errors?.contacts?.[labelKey]?.[index]?.[inputKey])}
             helperText={errors?.contacts?.[labelKey]?.[index]?.[inputKey]?.message}
             InputProps={{
              endAdornment: (
                <InputAdornment position='end'>
                  {indexPhone != 0 && (
                    <CloseIcon
                      style={{ cursor: 'pointer', color: 'red', padding: '1', margin: '0px 0px' }}
                      onClick={() => removePhones(indexPhone)}
                    />
                  )}
                </InputAdornment>
              )
            }}

           />
         )}
       />


     </>
            ))}

<Typography sx={{ marginRight: "30px", fontSize: "14px", color: "#6ab2df", cursor: "pointer" }} onClick={() => appendedPhones({ phone: '' })}>
              + {t("add")}
            </Typography>


        </div>
      ))}

        </List>
      </CardContent>
    </Card>
  );
}
