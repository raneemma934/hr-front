import { request } from '../../../utiltis/AxiosUtilitis'

const GetEmployeeById = async (id) => {


  return request({ url: `/api/employees/${id}` })
}

export default GetEmployeeById
