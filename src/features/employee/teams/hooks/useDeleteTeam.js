import { useMutation, useQueryClient } from "@tanstack/react-query";
import DeleteTeam from "../api/DeleteTeam";
import { showSuccesToast } from "src/utiltis/toastSecces";

export const useDeleteTeam = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:DeleteTeam,
    onSuccess: () => {
      queryClient.invalidateQueries("Teams");
      showSuccesToast('',data?.data?.data)
    },
  });
};
