import { request } from "src/utiltis/AxiosUtilitis";

const DeleteMvpUser = async (id) => {

  return request({
    url: `/api/EmployeeOfMonth/Delete?user_id=${id}`,

    method: 'delete',
  });
}

export default DeleteMvpUser
