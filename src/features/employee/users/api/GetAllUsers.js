import { request } from '../../../../utiltis/AxiosUtilitis'

const GetAllUsers = async () => {
  return request({ url: '/api/employees' })
}

export default GetAllUsers
