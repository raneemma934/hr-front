import React, { useState } from 'react'
import {
  Grid,
  Card,
  CardHeader,
  CardContent,
  MenuItem,
  Divider,
  Typography,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  IconButton,
  Avatar
} from '@mui/material'
import TextField from '@mui/material/TextField'
import Stack from '@mui/material/Stack'
import { styled } from '@mui/material/styles'
import Paper from '@mui/material/Paper'
import { useTranslation } from 'react-i18next'
import CustomDataGrid from 'src/@core/components/custom-datagrid'
import useUserColumns from '../../hooks/useUserColumns'
import { UsersData } from '../../infrastructure'
import { Box } from '@mui/system'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import FallbackSpinner from '../spinner'
import { useEffect } from 'react'
import MoreHorizIcon from '@mui/icons-material/MoreHoriz'
import DrawerForm from '../spinner/DrawerForm'
import { useDispatch, useSelector } from 'react-redux'
import { getAttendancePercentage } from 'src/pages/dashboard/store'
import useGetMvp from '../../hooks/useGetMvp'
import Show10 from 'src/@core/components/show10'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import PersonIcon from '@mui/icons-material/Person'
import Menu from '@mui/material/Menu'
import { Dialog, DialogTitle, DialogContent, DialogActions } from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete'
import Drawer from '@mui/material/Drawer'
import { Button } from '@mui/material'
import Radio from '@mui/material/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormControl from '@mui/material/FormControl'
import FormLabel from '@mui/material/FormLabel'
import useUpdateMvp from '../../hooks/useUpdateMvp'
import useGetAllUsers from '../../hooks/useGetAllUsers'
import useDeleteMvpUser from '../../hooks/useDeleteMvpUser'

const Users = ({ rows }) => {
  console.log("🚀 ~ Users ~ rows:", rows)
  const columns = useUserColumns()
  const [openParent, setOpenParent] = React.useState(false)
  const [show, setShow] = React.useState(10)
  const [selectedId, setSelectedId] = useState(null)

  const { t } = useTranslation()

  const { mutate: UpdateMvp, isLoading } = useUpdateMvp()
  const { mutate: DeleteUserMvp } = useDeleteMvpUser()

  const handleDeleteAPI = id => {
    DeleteUserMvp(id)
    setOpenParent(false)
    setAnchorEl(false)
  }

  const handleDrawerDone = _ => {
    UpdateMvp(selectedId)
    setOpenParent(false)
    setOpenModal(false)
  }

  const handleRadioChange = event => {
    setSelectedId(event.target.value)
  }

  let roleData = new Set([])
  rows?.data?.data?.forEach(element => {
    if (element?.role) roleData.add(element?.role)
  })

  let departmentFilter = new Set([])
  rows?.data?.data?.forEach(element => {
    if (element?.department?.name) departmentFilter.add(element?.department?.name)
  })

  const [fdata, setfdata] = useState(rows)

  const [role, setRole] = useState('')

  const [specialization, setSpecialization] = useState('')
  const [department, setDepartment] = useState('')
  const [search, setSearch] = useState('')
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)
  const [openModal, setOpenModal] = useState(false)
  const [rowData, setrowData] = React.useState(null)
  const { data } = useGetAllUsers()

  const ITEM_HEIGHT = 162

  const handleCloseModal = () => {
    setOpenModal(false)
  }

  const handleCloseAnchor = () => {
    setAnchorEl(null)
  }

  const handleDeleteClick = () => {
    deleteContractMutation.mutate(rowData.id)
    handleCloseAnchor()
  }

  const handleOpenModal = () => {
    setOpenModal(true)
    console.log('hon')
    setOpenParent(false)
    setAnchorEl(null)
  }

  const handleClick = (event, params) => {
    setrowData(params)
    setAnchorEl(event.currentTarget)
  }

  useEffect(() => {
    let filterData = UsersData(rows)
    if (role) filterData = filterData?.filter((value, index) => value?.role == role)
    if (specialization) filterData = filterData?.filter((value, index) => value?.specialization == specialization)
    if (department) filterData = filterData?.filter((value, index) => value?.department == department)
    if (search)
      filterData = filterData?.filter((value, index) => {
        return (
          value?.first_name?.toLowerCase().includes(search.toLowerCase()) ||
          value?.last_name?.toLowerCase().includes(search.toLowerCase())
        )
      })
    setfdata(filterData)
  }, [rows, role, specialization, search, department])

  const handelSearch = event => {
    const searchText = event.target.value
    if (searchText) setSearch(searchText)
    else setSearch(null)
  }

  const gridStyles = {
    root: {
      '& .MuiDataGrid-columnHeader, & .MuiDataGrid-cell': {
        borderRight: '0px solid #000'
      },
      '& .MuiDataGrid-columnsContainer': {
        backgroundColor: '#f0f0f0'
      },
      '& .MuiDataGrid-root': {
        scrollbarWidth: 'thin',
        scrollbarColor: '#000 #000',
        '&::-webkit-scrollbar': {
          width: '1px'
        },
        '&::-webkit-scrollbar-thumb': {
          backgroundColor: '#000'
        }
      }
    }
  }

  const [percentageData, setpercentageData] = useState([])

  const dispatch = useDispatch()
  const store = useSelector(state => state.Dashboard)

  useEffect(() => {
    dispatch(getAttendancePercentage())
    setpercentageData(store?.AttendancePercentage)
  }, [dispatch, store?.AttendancePercentage?.length])

  const { data: Data, loading } = useGetMvp()

  const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start'
  }))

  return (
    <>
      <p className='Pagetitle' paddingBottom={'10px'}>
        {t('Employees')}
      </p>
      <Box sx={{ marginTop: '24px', margin: 0, padding: 0 }}>
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls='panel2-content' id='panel2-header'>
            <Stack width={'50%'} direction={'row'}>
              <Stack direction={'row'}>
                <Typography>{percentageData?.present_employees}</Typography>/
                <Typography>{percentageData?.total_employees}</Typography>
              </Stack>

              <Box marginTop={'5px'} marginLeft={'5px'} width={'50%'}>
                <FallbackSpinner total={percentageData?.total_employees} active={percentageData?.present_employees} />
              </Box>

              <Typography marginLeft={'5px'}>
                {isNaN((percentageData?.present_employees / percentageData?.total_employees) * 100)
                  ? 0
                  : ((percentageData?.present_employees / percentageData?.total_employees) * 100).toFixed(1)}
                %
              </Typography>
            </Stack>
          </AccordionSummary>

          <AccordionDetails>
            <Stack direction={'row'} justifyContent={'start'} alignItems={'center'}>
              <Typography>{t('Employee of the month')}:</Typography>
              <IconButton
                aria-label='more'
                id='long-button'
                aria-controls={open ? 'long-menu' : undefined}
                aria-expanded={open ? 'true' : undefined}
                aria-haspopup='true'
                sx={{ padding: 0, marginX: 3, marginY: 2 }}
                onClick={event => handleClick(event)}
              >
                <MoreHorizIcon />
              </IconButton>
            </Stack>
            {Data?.data?.data?.user?.first_name ? (
              <Stack direction={'row'} justifyContent={'start'} alignItems={'center'} spacing={2}>
                <Avatar src={process.env.NEXT_PUBLIC_IMAGES + '/' + Data?.data?.data?.user?.user_info?.image} />

                <Typography>{Data?.data?.data?.user?.first_name}</Typography>
                <Typography>{Data?.data?.data?.user?.last_name}</Typography>
              </Stack>
            ) : (
              t('Employee of the month No')
            )}
          </AccordionDetails>
        </Accordion>
        {/* ******* */}
        <Box>
          <Menu
            id='long-menu'
            MenuListProps={{
              'aria-labelledby': 'long-button'
            }}
            anchorEl={anchorEl}
            open={open}
            onClose={handleCloseAnchor}
            PaperProps={{
              style: {
                maxHeight: ITEM_HEIGHT * 4.5,
                width: '208px',
                boxShadow: 'none'
              }
            }}
          >
            <MenuItem sx={{ padding: '0', color: '#3F4458' }} onClick={handleOpenModal}>
              <Box style={{ textDecoration: 'none' }}>
                <IconButton>
                  <PersonIcon variant='contained' sx={{ color: '#3F4458' }} size='small' />
                </IconButton>
                <span style={{ color: '#3F4458' }}>{t('Change')}</span>
              </Box>
            </MenuItem>
            <MenuItem
              sx={{ padding: '0', color: '#3F4458' }}
              onClick={() => handleDeleteAPI(Data?.data?.data?.user?.id)}
            >
              <Box style={{ textDecoration: 'none' }}>
                <IconButton>
                  <DeleteIcon variant='contained' sx={{ color: '#3F4458' }} size='small' />
                </IconButton>
                <span style={{ color: '#3F4458' }}>{t('Delete')}</span>
              </Box>
            </MenuItem>
          </Menu>

          {/*  */}
          <>
            <Drawer
              anchor='top'
              open={openModal}
              variant='temporary'
              ModalProps={{
                keepMounted: true
              }}
              onEscapeKeyDown={handleCloseModal}
              sx={{
                width: { sm: '440px', xs: '300px', md: '440px' },
                flexShrink: 0,
                '& .MuiDrawer-paper': {
                  width: { sm: '440px', xs: '300px', md: '440px' },
                  margin: 'auto',
                  top: '20%',
                  borderRadius: '12px '
                }
              }}
            >
              <DrawerHeader sx={{ color: '#8090A7', padding: '0px 0px 0px 10px' }}>
                {t('Select the new employee of the month')}
              </DrawerHeader>

              <Stack height={'270px'} direction={'column'} padding={'2px 0px 0px 10px'}>
                <FormControl>
                  <RadioGroup
                    aria-labelledby='demo-radio-buttons-group-label'
                    defaultValue='female'
                    name='radio-buttons-group'
                    onChange={handleRadioChange}
                  >
                    <Stack spacing={3}>
                      {data?.data?.data?.map((element, index) => (
                        <Box key={index}>
                          <FormControlLabel
                            value={element.id}
                            control={<Radio />}
                            label={
                              <Stack direction={'row'} spacing={2} alignItems={'center'}>
                                <Avatar src={process.env.NEXT_PUBLIC_IMAGES + '/' + element?.user_info?.image} />
                                <Typography>{element.first_name}</Typography>
                                <Typography>{element.last_name}</Typography>
                              </Stack>
                            }
                          />
                        </Box>
                      ))}
                    </Stack>
                  </RadioGroup>
                </FormControl>

                <Box sx={{ display: 'flex', justifyContent: 'flex-end', padding: '7px', gap: '4px', marginTop: '8px' }}>
                  <Button onClick={handleCloseModal}>{t('Cancel')}</Button>
                  <Button
                    sx={{
                      backgroundColor: '#6AB2DF',
                      color: '#fff',
                      ':hover': { color: '#fff', backgroundColor: '#2A4759' }
                    }}
                    onClick={handleDrawerDone}
                  >
                    {t('Done')}
                  </Button>
                </Box>
              </Stack>
            </Drawer>
          </>
          {/*  */}
        </Box>

        {/* ******* */}
        {/* <DrawerForm  open={openParent} setOpenParent={setOpenParent} /> */}
      </Box>

      <Card sx={{ borderRadius: '12px', marginTop: '24px' }}>
        <CardContent>
          <Stack
            direction={{ xs: 'column', sm: 'column' }}
            spacing={2}
            alignContent={'center'}
            justifyContent={'center'}
          >
            <Stack direction={'row'} width={{ sm: '50%', xs: '100%' }} spacing={3} alignItems={'center'}>
              <Box mb={2} width={{ sm: '120px', xs: '100px' }}>
                <Show10 setShow={setShow} />
              </Box>
              <TextField
                placeholder={t('Search')}
                fullWidth
                InputProps={{
                  startAdornment: (
                    <Box paddingRight={1}>
                      <svg width='14' height='15' viewBox='0 0 14 15' fill='none' xmlns='http://www.w3.org/2000/svg'>
                        <g id='zoom-split'>
                          <path
                            id='Combined Shape'
                            fillRule='evenodd'
                            clipRule='evenodd'
                            d='M5.75002 11.875C2.64341 11.875 0.125015 9.3566 0.125015 6.25C0.125015 3.1434 2.64341 0.625 5.75002 0.625C8.85662 0.625 11.375 3.1434 11.375 6.25C11.375 9.3566 8.85662 11.875 5.75002 11.875ZM5.75 10.6251C8.16625 10.6251 10.125 8.6663 10.125 6.25005C10.125 3.8338 8.16625 1.87505 5.75 1.87505C3.33376 1.87505 1.375 3.8338 1.375 6.25005C1.375 8.6663 3.33376 10.6251 5.75 10.6251ZM13.692 14.1919C13.936 13.9478 13.936 13.5521 13.692 13.308L11.192 10.808C10.9479 10.5639 10.5522 10.5639 10.3081 10.808C10.064 11.0521 10.064 11.4478 10.3081 11.6919L12.8081 14.1919C13.0522 14.436 13.4479 14.436 13.692 14.1919Z'
                            fill='#8090A7'
                          />
                        </g>
                      </svg>
                    </Box>
                  )
                }}
                onChange={handelSearch}
                sx={{
                  backgroundColor: '#F5F7FA',
                  border: 'none',
                  boxShadow: 'none',
                  width: { sm: '320px', xs: '100%' }
                }}
                size='small'
              />
            </Stack>
            <Stack direction={{ xs: 'column', sm: 'row' }} alignItems={'center'} spacing={2}>
              <Typography className='filterTitle'>{t('Filters')}</Typography>
              {/* <TextField
                select
                sx={{ width:{sm:'320px',xs:'100%'} }}
                defaultValue=''
                SelectProps={{
                  value: role,
                  displayEmpty: true,
                  onChange: handleRoleChange
                }}
                size='small'
              >
                <MenuItem value=''>{`${t('Role')}`}</MenuItem>

                {Array.from(roleData).map(element => (
                  <MenuItem key={element} value={element}>
                    {element}
                  </MenuItem>
                ))}
              </TextField> */}

              {/* <TextField
                select
                fullWidth
                defaultValue=''
                SelectProps={{
                  value: specialization,
                  displayEmpty: true,
                  onChange: handleSpecializationChange
                }}
                size='small'
              >
                <MenuItem value=''>{`${t('Specialization')}`}</MenuItem>
                {Array.from(specializationFilter).map(element => (
                  <MenuItem key={element} value={element}>
                    {element}
                  </MenuItem>
                ))}
              </TextField> */}

              <TextField
                select
                sx={{ width: { sm: '320px', xs: '100%' } }}
                defaultValue=''
                SelectProps={{
                  value: department,
                  displayEmpty: true,
                  onChange: e => setDepartment(e.target.value)
                }}
                size='small'
              >
                <MenuItem value=''>{`${t('Department')}`}</MenuItem>
                {Array.from(departmentFilter).map(element => (
                  <MenuItem key={element} value={element}>
                    {element}
                  </MenuItem>
                ))}
              </TextField>
            </Stack>

            {rows ? <CustomDataGrid columns={columns} sx={gridStyles.root} show={show} rows={fdata || []} /> : null}
          </Stack>
        </CardContent>
      </Card>
    </>
  )
}

export default Users
