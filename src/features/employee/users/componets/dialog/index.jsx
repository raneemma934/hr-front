import React from "react";
import { Button, Modal, Typography } from "@mui/material";
import { Box, Grid} from '@mui/material';
import useDeleteUser from "../../hooks/useDeleteUser";
import { useTranslation } from "react-i18next";


const AlertDialogDeleteUser = ({ id, open, handleClose }) => {
  const { t } = useTranslation();
  const { mutate: DeleteUser, isLoading } = useDeleteUser();

  const handleDeleteAPI = () => {
    DeleteUser(id);
    handleClose();
  };


  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: '#fff',
    border: '2px solid #000',
    boxShadow: 0,
    p: 4,
    textAlign:"center",
    overflow:"hidden",
    borderRadius:"12px"
  };

  return (
    <Box >
      <Grid columnSpacing={{ xs: 1, sm: 2, md: 3 }} >
        <Modal
          open={open}
          onClose={handleClose}
          className="dialog-arabic"
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
          keepMounted={true}
        >
          <Grid item xs={4}>
            <Box sx={style}>
              <Typography  sx={{ fontWeight: "600", fontSize: "16px", color: "#131627" }} id="modal-modal-title" variant="h6" component="h2">{t('Delete')}</Typography>
              <Typography style={{ fontSize: "19px", color: '#B4B4B3',marginTop:"14px" }} id="modal-modal-description" sx={{ mt: 2 }}>
                {t("Are you sure you want to delete ")} {t("user")}?
              </Typography>
              <Box marginTop={"14px"} style={{ display: 'flex', justifyContent: 'center', padding: '10px' ,gap:"14px" }}>
                <Button sx={{ color: "#fff",backgroundColor:"#DF2E38",padding:"8px 24px 8px 24px",borderRadius:"4px", "&:hover": { backgroundColor: "#DF2E38" } }} onClick={handleDeleteAPI} autoFocus>
                  {t('Delete')}
                </Button>
                <Button onClick={handleClose} style={{ color: '#fff',backgroundColor:"#B4B4B3",borderRadius:"4px",padding:"8px 24px 8px 24px" }}>{t('Cancel')}</Button>
              </Box>
            </Box>
          </Grid>
        </Modal>
      </Grid>
    </Box>
  );
}

export default AlertDialogDeleteUser;
