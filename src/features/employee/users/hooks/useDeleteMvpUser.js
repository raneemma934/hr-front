
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { showErrorToast } from 'src/utiltis/showErrorToast';
import { showSuccesToast } from 'src/utiltis/toastSecces';
import DeleteMvpUser from '../api/DeleteMvpUser';

const useDeleteMvpUser = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:DeleteMvpUser,
    onSuccess: (data) => {
      queryClient.invalidateQueries("Users");
      showSuccesToast("","success")
    },
    onError:(data) =>{
      queryClient.invalidateQueries("Users");
      showErrorToast("","Delete failed")
    }
  });
}

export default useDeleteMvpUser

