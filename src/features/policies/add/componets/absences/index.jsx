import React from 'react'
import { Button, Card, CardContent, TextField, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import { useState } from 'react'
import styled from 'styled-components'
import KeyboardArrowUpRoundedIcon from '@mui/icons-material/KeyboardArrowUpRounded'
import ExpandMoreRoundedIcon from '@mui/icons-material/ExpandMoreRounded'
import Checkbox from '@mui/material/Checkbox'
import Divider from '@mui/material/Divider'
import { List } from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'
import { useFieldArray } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

export default function AbsencesManagement({ Controller, control, Paid, setPaid, Unpaid, setUnpaid, setSick, Sick }) {
  console.log("🚀 ~ AbsencesManagement ~ Sick:", Sick)
  console.log("🚀 ~ AbsencesManagement ~ Unpaid:", Unpaid)
  console.log("🚀 ~ AbsencesManagement ~ Paid:", Paid)
  const [noteAdded, setNoteAdded] = useState(false)
  const { t } = useTranslation()

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'absence_management.notes'
  })

  const handleAddClick = () => {
    append({})
    setNoteAdded(true)
  }

  const handleRemoveClick = index => {
    remove(index)
    setNoteAdded(false)
  }

  const Typo = styled(Typography)(({ theme }) => ({
    fontSize: '14px',
    fontWeight: 500,
    letterSpacing: 0.7,
    color: '#8090A7'
  }))

  const TittleSection = styled(Typography)(({ theme }) => ({
    fontSize: '20px',
    fontWeight: 600,

    color: '#8090A7'
  }))
  const label = { inputProps: { 'aria-label': 'Checkbox demo' } }

  const handleUpPaid = () => {
    setPaid(Paid + 1)
  }

  const handleDownPaid = () => {
    if (Paid > 0) {
      setPaid(Paid - 1)
    }
  }

  const handleUpUnpaid = () => {
    setUnpaid(Unpaid + 1)
  }

  const handleDownUnpaid = () => {
    if (Unpaid > 0) {
      setUnpaid(Unpaid - 1)
    }
  }

  const handleUpSick = () => {
    setSick(Sick + 1)
  }

  const handleDownSick = () => {
    if (Sick > 0) {
      setSick(Sick - 1)
    }
  }

  return (
    <Card sx={{ borderRadius: '12px' }}>
      <CardContent>
        <Stack direction={'column'} spacing={2}>
          <TittleSection fontSize={'20px'}>{t('Absences Management')}</TittleSection>


            <Typo>{t('Paid absence days')}</Typo>

            <Controller
              defaultValue=''
              name={`policy[number_of_paid_days_in_absence]`}
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  value={Paid !== 0 ? Paid : t('none')}
                  disabled
                  fullWidth
                  size='small'
                  InputProps={{
                    endAdornment: (
                      <Box marginLeft={'70%'} display={'flex'}>
                        <Button onClick={handleUpPaid} style={{ padding: 0, minWidth: 'unset' }}>
                          <KeyboardArrowUpRoundedIcon />
                        </Button>
                        <Button onClick={handleDownPaid} style={{ padding: 0, minWidth: 'unset' }}>
                          <ExpandMoreRoundedIcon />
                        </Button>
                      </Box>
                    )
                  }}
                />
              )}
            />

<Box sx={{ width: '100%', marginTop: '15px !important' }}>
            <Typo>{t('Sick absence days')}</Typo>
            <Controller
              name={`policy[number_of_unpaid_days_in_absence]`}
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  value={Unpaid !== 0 ? Unpaid : t('none')}
                  disabled
                  fullWidth
                  size='small'
                  InputProps={{
                    endAdornment: (
                      <Box marginLeft={'70%'} display={'flex'}>
                        <Button onClick={handleUpUnpaid} style={{ padding: 0, minWidth: 'unset' }}>
                          <KeyboardArrowUpRoundedIcon />
                        </Button>
                        <Button onClick={handleDownUnpaid} style={{ padding: 0, minWidth: 'unset' }}>
                          <ExpandMoreRoundedIcon />
                        </Button>
                      </Box>
                    )
                  }}
                />
              )}
            />

          </Box>

          <Box sx={{ width: '100%', marginTop: '15px !important' }}>
            <Typo>{t('Sick absence days')}</Typo>
            <Controller
              name={`policy[sick_absence_days]`}
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  value={Sick !== 0 ? Sick : t('none')}
                  disabled
                  fullWidth
                  size='small'
                  InputProps={{
                    endAdornment: (
                      <Box marginLeft={'70%'} display={'flex'}>
                        <Button onClick={handleUpSick} style={{ padding: 0, minWidth: 'unset' }}>
                          <KeyboardArrowUpRoundedIcon />
                        </Button>
                        <Button onClick={handleDownSick} style={{ padding: 0, minWidth: 'unset' }}>
                          <ExpandMoreRoundedIcon />
                        </Button>
                      </Box>
                    )
                  }}
                />
              )}
            />

          </Box>

          {/* **** */}



        </Stack>
      </CardContent>
    </Card>
  )
}
