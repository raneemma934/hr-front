import React, { useState } from 'react';
import { Box, Stack, styled } from '@mui/system';
import Drawer from '@mui/material/Drawer';
import { Button } from '@mui/material';
import { useTranslation } from 'react-i18next';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';





export default function DrawerForm({ open, setOpenParent ,setDays }) {
  const { t } = useTranslation()

  const [selectedDays, setSelectedDays] = useState([]);

  const handleDrawerClose = () => {
    setOpenParent(false);
  };

  const handleDrawerDone = () => {
    setDays(selectedDays)
    handleDrawerClose();
  };

  const handleButtonToggle = (day) => {
    setSelectedDays((prevSelectedDays) =>
      prevSelectedDays.includes(day)
        ? prevSelectedDays.filter((selectedDay) => selectedDay !== day)
        : [...prevSelectedDays, day]
    );
  };

  return (
    <>
      <Dialog
        anchor="top"
        open={open}
        variant="temporary"
        ModalProps={{
          keepMounted: true,
        }}
        onEscapeKeyDown={handleDrawerClose}
      >
        <DialogTitle mb='12px'>
        {t("Set Work Days")}
        </DialogTitle>
<DialogContent>
        <Stack spacing={4} direction={'row'} justifyContent={'space-between'}>

          {['saturday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday'].map((day) => (
            <Button
              key={day}
              sx={{
                backgroundColor: selectedDays.includes(day) ? '#6AB2DF' : '#E9ECF3',
                color: selectedDays.includes(day) ? '#fff' : '#8090A7',

                ':hover': {
                  backgroundColor: selectedDays.includes(day) ? '#4F8CC9' : '#D0D7E3',
                  color: selectedDays.includes(day) ? '#fff' : '#576B7E',
                },
              }}
              onClick={() => handleButtonToggle(day)}
            >
              {' ' + day.substring(0, 3)+ ''}
            </Button>
          ))}

        </Stack>
        </DialogContent>
<DialogActions >
            <Button onClick={handleDrawerClose} variant='tonal' color='secondary' >   {t("Cancel")}</Button>
            <Button
             variant='tonal'
             color='primary'
              onClick={handleDrawerDone}
            >
                            {t("Set")}

            </Button>

          </DialogActions>
      </Dialog>
    </>
  );
}
