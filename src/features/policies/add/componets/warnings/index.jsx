import { Button, Card, CardContent, TextField, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import KeyboardArrowUpRoundedIcon from '@mui/icons-material/KeyboardArrowUpRounded'
import ExpandMoreRoundedIcon from '@mui/icons-material/ExpandMoreRounded'
import { List } from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'
import Divider from '@mui/material/Divider'
import { useFieldArray } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import CustomTextField from 'src/@core/components/mui/text-field'

export default function Warnings({
  Controller,
  control,
  defaultValues,
  setAlert,
  alert,
  warningsto,
  setWarningsto,
  errors
}) {
  const [noteAdded, setNoteAdded] = useState(false)
  const { t } = useTranslation()

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'warnings.notes'
  })

  const handleAddClick = () => {
    append({})
    setNoteAdded(true)
  }

  const handleRemoveClick = index => {
    remove(index)
    setNoteAdded(false)
  }

  const Typo = styled(Typography)(({ theme }) => ({
    fontSize: '14px',
    fontWeight: 500,
    letterSpacing: 0.7,
    color: '#8090A7'
  }))

  const TittleSection = styled(Typography)(({ theme }) => ({
    fontSize: '20px',
    fontWeight: 600,

    color: '#8090A7'
  }))

  const handleUpAlert = _ => {
    setAlert(alert + 1)
  }

  const handleDownAlert = _ => {
    if (alert != 0) setAlert(alert - 1)
  }

  const handleUpWaring = _ => {
    setWarningsto(warningsto + 1)
  }

  const handleDownWarning = _ => {
    if (warningsto != 0) setWarningsto(warningsto - 1)
  }

  return (
    <Card sx={{ borderRadius: '12px' }}>
      <CardContent>
        <Stack direction={'column'} spacing={2}>
          <TittleSection fontSize={'20px'}>{t('Warnings Management')}</TittleSection>

          <Typo style={{ marginTop: '24px' }}>{t('Alerts to warning')}</Typo>

          <Controller
            name={`policy.alerts_to_warnings`}
            control={control}
            render={({ field }) => (
              <CustomTextField
                {...field}
                value={alert != 0 ? alert : t('none')}
                fullWidth
                InputProps={{
                  endAdornment: (
                    <Box marginLeft={'70%'} display={'flex'}>
                      <Button onClick={handleUpAlert} style={{ padding: 0, minWidth: 'unset' }}>
                        <KeyboardArrowUpRoundedIcon />
                      </Button>
                      <Button onClick={handleDownAlert} style={{ padding: 0, minWidth: 'unset' }}>
                        <ExpandMoreRoundedIcon />
                      </Button>
                    </Box>
                  )
                }}
              />
            )}
          />
          <Typo>{t('Warnings to dismissal')}</Typo>

          <Controller
            name={`policy.warnings_to_dismissal`}
            control={control}
            render={({ field }) => (
              <CustomTextField
                {...field}
                value={warningsto != 0 ? warningsto : t('none')}
                fullWidth
                InputProps={{
                  endAdornment: (
                    <Box marginLeft={'70%'} display={'flex'}>
                      <Button onClick={handleUpWaring} style={{ padding: 0, minWidth: 'unset' }}>
                        <KeyboardArrowUpRoundedIcon />
                      </Button>
                      <Button onClick={handleDownWarning} style={{ padding: 0, minWidth: 'unset' }}>
                        <ExpandMoreRoundedIcon />
                      </Button>
                    </Box>
                  )
                }}
              />
            )}
          />
        </Stack>
      </CardContent>
    </Card>
  )
}
