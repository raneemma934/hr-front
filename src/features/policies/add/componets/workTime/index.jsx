import React, { useEffect, useState } from 'react'
import { Box, Button, Card, CardContent, Chip, Stack, TextField, Typography } from '@mui/material'
import DrawerForm from '../drawerDay'
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth'
import styled from 'styled-components'
import { useFieldArray } from 'react-hook-form'
import { List } from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'
import Divider from '@mui/material/Divider'
import { useTranslation } from 'react-i18next'
import useSelectBranch from 'src/pages/employees/add/hook/useSelectBranch'
import { MenuItem } from '@mui/material'
import CustomTextField from 'src/@core/components/mui/text-field'

export default function WorkTime({ Controller, control, days, setDays, errors }) {
  const [openParent, setOpenParent] = React.useState(false)
  const [noteAdded, setNoteAdded] = useState(false)
  const { t } = useTranslation()
  const { data } = useSelectBranch()

  const handleDayPicker = date => {
    setOpenParent(true)
  }

  const [selectedTime, setSelectedTime] = useState(null) // State variable to store selected time

  const handleTimeChange = newTime => {
    setSelectedTime(newTime)
  }


  const Typo = styled(Typography)(({ theme }) => ({
    fontSize: '14px',
    fontWeight: 500,
    letterSpacing: 0.7,
    color: '#8090A7'
  }))

  const TittleSection = styled(Typography)(({ theme }) => ({
    fontSize: '20px',
    fontWeight: 600,

    color: '#8090A7'
  }))



  const handleAddClick = () => {
    append({})
    setNoteAdded(true)
  }

  const handleRemoveClick = index => {
    remove(index)
    setNoteAdded(false)
  }

  const [startTime, setStartTime] = useState('')

  const handleChange = event => {
    setStartTime(event.target.value)
  }

  return (
    <>
    <Card sx={{ borderRadius: '12px' }}>
      <CardContent>
        <Stack spacing={2} direction={'column'}>
          <Typography variant='h2' >{t('Work Time')}</Typography>
          <Typo style={{ marginTop: '24px' }}>{t('Work days')}</Typo>
          <Controller
        name={`policy.work_days`}
        control={control}
        render={({ field }) => (
          <>
            <Button
              onClick={() => {
                handleDayPicker()
                field.onChange(JSON.stringify(days))
              }}
              sx={{
                justifyContent: 'start',
                border: '0.1px solid #d7d7da',
                height: '42px',

                '&:hover': {
                  backgroundColor: '#fff',
                  color: 'none'
                }
              }}
              fullWidth
            >
              {days?.length ? (
                days?.map(day => (
                  <Chip
                    key={day}
                    label={day.substring(0, 3)}
                    sx={{
                      marginX: '4px',
                      backgroundColor: '#6AB2DF',
                      color: '#fff',
                      fontSize: '13px',
                      width: 'auto'
                    }}
                  />
                ))
              ) : (
                <Stack direction={'row'} width={'100%'} justifyContent={'space-between'}>
                  <Box>
                    <Typo>{t('Work days')}</Typo>
                  </Box>
                  <Box>
                    <CalendarMonthIcon sx={{ color: '#8090A7' }} />
                  </Box>
                </Stack>
              )}
            </Button>
          </>
        )}
      />



          <Controller
            name={`policy.start_time`}
            control={control}
            render={({ field }) => (
              <CustomTextField
                {...field}
                type='time'
                title={t('Start time')}
                placeholder='Start time'
                fullWidth
                size='small'
                error={!!errors.policy?.start_time}
                helperText={
                  errors.policy?.start_time
                    ? errors.policy?.start_time.message
                    : ''
                }
              />
            )}
          />



          <Controller
            name={`policy.minutes_grace_period`}
            control={control}
            render={({ field }) => (
              <CustomTextField
                {...field}
                placeholder='minutes grace period'
                fullWidth
                title={t('Cut-off time')}
                type='number'
                size='small'
                error={!!errors.policy?.minutes_grace_period}
                helperText={
                  errors.policy?.minutes_grace_period
                    ? errors.policy?.minutes_grace_period.message
                    : ''
                }
              />
            )}
          />
          <Controller
            name={`policy.end_time`}
            control={control}
            render={({ field }) => (
              <CustomTextField
                {...field}
title={t('End time')}
                type='time'
                placeholder='End time'
                fullWidth

                error={!!errors.policy?.end_time}
                helperText={
                  errors.policy?.end_time ? errors.policy?.end_time.message : ''
                }
              />
            )}
          />



        </Stack>

      </CardContent>
    </Card>

    <DrawerForm open={openParent} setOpenParent={setOpenParent} setDays={setDays} />
    </>
  )
}
