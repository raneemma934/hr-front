import { request } from '../../../utiltis/AxiosUtilitis'

const GetAllPolicy = async () => {
  return request({ url: `/api/branches/${localStorage.getItem('branch')}` })
}

export default GetAllPolicy
