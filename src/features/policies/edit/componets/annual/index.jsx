import React, { useState } from 'react'
import { Card, CardContent, Checkbox, FormControlLabel, FormGroup, TextField, Typography, Divider, Box, Stack } from '@mui/material'
import { useFieldArray, Controller } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import CustomTextField from 'src/@core/components/mui/text-field'
import PropTypes from 'prop-types'

export default function Annual({ control, errors, setDays, days }) {
  const [noteAdded, setNoteAdded] = useState(false)
  const { t } = useTranslation()

  // Typography styling can use sx prop for better integration with MUI system
  const titleStyles = {
    fontSize: '20px',
    fontWeight: 600,
    color: '#8090A7'
  }

  const typoStyles = {
    fontSize: '14px',
    fontWeight: 500,
    letterSpacing: 0.7,
    color: '#8090A7'
  }

  // Handle adding/removing note logic (noteAdded state)
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'annual_salary_increase.notes'
  })

  const handleAddClick = () => {
    append({})
    setNoteAdded(true)
  }

  const handleRemoveClick = index => {
    remove(index)
    setNoteAdded(false)
  }

  return (
    <Card>
      <CardContent>
        <Stack spacing={2}>
          <Typography sx={titleStyles}>{t('Branches')}</Typography>

          {/* Branch Name Field */}
          <Typography sx={{ ...typoStyles, marginTop: '24px' }}>{t('Branch name')}</Typography>
          <Controller
            name="name"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <CustomTextField
                {...field}
                placeholder={t('Enter branch name')}
                fullWidth
                size="small"
                error={!!errors.name}
                helperText={errors.name ? errors.name.message : ' '}
              />
            )}
          />

          {/* IP Field */}
          <Typography sx={typoStyles}>{t('IP')}</Typography>
          <Controller
            name="ip"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <CustomTextField
                {...field}
                placeholder={t('Enter IP address')}
                fullWidth
                size="small"
                error={!!errors.ip}
                helperText={errors.ip ? errors.ip.message : ' '}
              />
            )}
          />


        </Stack>
      </CardContent>
    </Card>
  )
}

// Prop types validation (better type checking in non-TS environments)
Annual.propTypes = {
  control: PropTypes.object.isRequired,
  errors: PropTypes.object,
  setDays: PropTypes.func,
  days: PropTypes.any
}
