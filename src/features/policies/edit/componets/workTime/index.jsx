import React, { useEffect, useState } from 'react'
import { Box, Button, Card, CardContent, Chip, Stack, TextField, Typography } from '@mui/material'
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth'
import styled from 'styled-components'
import { useFieldArray } from 'react-hook-form'
import { List } from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'
import Divider from '@mui/material/Divider'
import { useTranslation } from 'react-i18next'
import useSelectBranch from 'src/pages/employees/add/hook/useSelectBranch'
import { MenuItem } from '@mui/material'
import DrawerForm from 'src/features/policies/add/componets/drawerDay'
import CustomTextField from 'src/@core/components/mui/text-field'

export default function WorkTime({ EditData, Controller, control,defaultValues, days, setDays, errors,setValue,reset }) {
console.log("🚀 ~ WorkTime ~ days:", days)

  const [openParent, setOpenParent] = React.useState(false)
  const [noteAdded, setNoteAdded] = useState(false)
  const { t } = useTranslation()
  const { data } = useSelectBranch()




  const handleDayPicker = date => {
    setOpenParent(true)
  }

  const Typo = styled(Typography)(({ theme }) => ({
    fontSize: '14px'
  }))

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'policy.notes'
  })

  const handleAddClick = () => {
    append({})
    setNoteAdded(true)
  }

  const handleRemoveClick = index => {
    remove(index)
    setNoteAdded(false)
  }

  // useEffect(() => {
  //   setValue('policy.start_time', defaultValues.policy.cut_off_time);
  // }, [defaultValues, setValue]);

  return (
    <Card>
      <CardContent>
        <Stack spacing={2} direction={'column'}>
          <Typography fontSize={'20px'}>{t('Work Time')}</Typography>
          <Typo style={{ marginTop: '24px' }}>{t('Work days')}</Typo>
          <Controller
           name={`policy.work_days`}
           control={control}
           render={({ field }) => (
           <>
      <Button
    onClick={() => {
      handleDayPicker()
      field.onChange(JSON.stringify(days))
    }}
    sx={{
      justifyContent: 'start',
      border: '0.1px solid #d7d7da',
      height: '42px',

      '&:hover': {
        backgroundColor: '#fff',
        color: 'none'
      }
    }}
    fullWidth
      >
        <Stack direction={'row'} spacing={1}>
          {days.length ? (
            days?.[0]?.map((day,index) => (
              <Box key={index}>

              <Chip
                key={index}
                label={day.substring(0, 3)}
                sx={{
                  backgroundColor: '#6AB2DF'    ,
                  color: '#fff',
                  fontSize: '13px',
                  width: 'auto'
                }}
              />
             </Box>
            ))
          ) : (
            <Stack direction={'row'} width={'100%'} justifyContent={'space-between'}>
              <Box>
                <Typography>{t('Work days')}</Typography>
              </Box>
              <Box>
                <CalendarMonthIcon sx={{ color: '#8090A7' }} />
              </Box>
            </Stack>
          )}
        </Stack>
      </Button>
    </>
  )}
/>




          <Controller
            name={`policy.start_time`}
            control={control}
            render={({ field }) => (
              <CustomTextField
                {...field}
                type='time'
                title={t('Start time')}
                placeholder='Start time'
                fullWidth
                size='small'
                error={!!errors.policy?.start_time}
                helperText={errors.policy?.start_time ? errors.policy?.start_time.message : ''}


              />
            )}
          />



          <Controller
            name={`policy.minutes_grace_period`}
            control={control}
            render={({ field }) => (
              <CustomTextField
                {...field}
                type='number'
                title={t('Cut-off time')}
                placeholder='Cut-off time'
                fullWidth
                size='small'
                error={!!errors.policy?.cut_off_time}
                helperText={errors.policy?.cut_off_time ? errors.policy?.cut_off_time.message : ' '}


              />
            )}
          />
          <Controller
            name={`policy.end_time`}
            control={control}
            render={({ field }) => (
              <CustomTextField
                {...field}
                type='time'
                title={t('End time')}
                placeholder='End time'
                fullWidth
                size='small'
                error={!!errors.policy?.end_time}
                helperText={errors.policy?.end_time ? errors.policy?.end_time.message : ''}
              />
            )}
          />
        </Stack>

        <DrawerForm open={openParent} setOpenParent={setOpenParent} setDays={setDays} />
      </CardContent>
    </Card>
  )
}
