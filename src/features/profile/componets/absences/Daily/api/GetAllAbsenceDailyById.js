
import { request } from "src/utiltis/AxiosUtilitis"

const GetAllAbsenceDailyById = async (obj) => {

  return request({ url: `/api/Absence/allUserAbsences?date=${obj.date}&user_id=${obj.id}` })
}

export default GetAllAbsenceDailyById
