import { Button, Dialog, DialogActions, DialogTitle, Grid, TextField,Typography } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import Paper from '@mui/material/Paper';
import { useAddAbsence } from 'src/features/employee/absence/hooks/useAddAbsence';

export default function PasswordModel ({open,setOpen,Data,setOpenParent}){
  const { id, Value,Type } = Data;

  const { mutate: AddAbsence } = useAddAbsence()
    const {t} = useTranslation()

    const handleDelete = () => {

      setConfirm(true)

    }

    const handleClose=()=>{
      setOpen(false)
    }

    const handleSubmit=event=>{
      event.preventDefault()


        try {
          const formData = new FormData()

          formData.append('type', Type)
          formData.append('id',id)
          const isPaidValue = Value ? '1' : '0';
          formData.append('isPaid', isPaidValue);
          formData.append('password',event.target[0].value);
          AddAbsence(formData)

          handleClose()

          } catch (error) {


        }


    }

    const Item = styled(Paper)(({ theme }) => ({
      // backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
      // ...theme.typography.body2,
      // padding: theme.spacing(1),
      // textAlign: 'center',
      // color: theme.palette.text.secondary
    }))

    return(

        <Dialog onClose={handleClose} open={open}>
        <Grid item xs={12}>
          <form onSubmit={handleSubmit} >
          <Item>
            {!confirm? <Typography sx={{ fontWeight: '600', fontSize: '20px', color: '#131627' }}>{t('Delete')}</Typography>:null}
            {!confirm ?
            <DialogTitle style={{ fontSize: '14px', color: '#3F4458' }}>
              {t('Are you sure you want to delete')} [{Branch && Branch && Branch.name}] ? {t('This action is irreversible and will permanently remove all associated data and configurations for this branch.')}
            </DialogTitle>
            :
            <DialogTitle style={{ fontSize: '14px', color: '#3F4458' }}>

{t('Please, Enter your password to confirm .')}

            <TextField
              sx={{ marginTop:'3%' }}
              type='password'
              label='password'
              required
              fullWidth
              size='small'
            />
            </DialogTitle>
            }
            <DialogActions style={{ display: 'flex', justifyContent: 'center', padding: '10px' }}>
              <Button onClick={handleClose} sx={{ backgroundColor: 'rgba(128, 144, 167,0.2)', color: '#8090A7' }}>
              {t('Cancel')}
              </Button>

              {!confirm?
              <Button
                autoFocus
                sx={{
                  color: '#fff',
                  backgroundColor: '#DF2E38',
                  border: '1px solid #DF2E38',
                  '&:hover': { color: '#fff', background: '#DF2E38' }
                }}
                onClick={handleDelete}
              >
                {t('Delete')}
              </Button>
              :
              <Button
              autoFocus
                type='submit'
              sx={{
                color: '#fff',
                backgroundColor: '#DF2E38',
                border: '1px solid #DF2E38',
                '&:hover': { color: '#fff', background: '#DF2E38' }
              }}
            >
              {t("Confirm")}
            </Button>
            }
            </DialogActions>
          </Item>
          </form>
        </Grid>
      </Dialog>

    )

}
