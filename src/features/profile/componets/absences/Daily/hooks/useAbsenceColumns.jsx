// Import necessary components and libraries
import React, { useEffect, useMemo, useState } from 'react';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import BorderColorOutlinedIcon from '@mui/icons-material/BorderColorOutlined';
import { Avatar, ListItem, Typography } from '@mui/material';
import Collapse from '@mui/material/Collapse'
import ListItemIcon from '@mui/material/ListItemIcon'
import TableCell from '@mui/material/TableCell'

import { useTranslation } from 'react-i18next';
import DrawerForm from '../componets/DrawerForm';
import Link from 'next/link';
import EditIcon from '../../../../../../../public/images/IconInput/edit'
import List from '@mui/material/List'
import ListItemText from '@mui/material/ListItemText'
import IconButton from '@mui/material/IconButton'

import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'

import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'


import MuiListItem from '@mui/material/ListItem'

const useAbsenceDailyColumns = () => {
  const [isDrawerOpenEdit, setIsDrawerOpenEdit] = useState(false);

  const [EditData, setEditData] = useState(null);
  const [openRows, setOpenRows] = useState({});

  const [open, setOpen] = useState(false)

  const handleToggle = (id) => {
    setOpenRows((prevOpenRows) => ({
      ...prevOpenRows,
      [id]: !prevOpenRows[id],
    }));
  };


  const { t } = useTranslation();



  const handleOpenClick = row =>{
    setIsDrawerOpenEdit(true)

     setEditData(row);

  }


  const handleEditClick = (row) => {
    setIsDrawerOpenEdit(true);

  };

  const handleClickOpen = (params) => {
    const { id } = params;
    setDeleteId(id);
    setIsDeletePopupOpen(true);


  };

  const handleClose = () => {
    setIsDeletePopupOpen(false)
  };

  const columnStyles = {
    cell: {
      marginLeft: '10%',
    },
  };

  return useMemo(() => [
    {
      field: 'startDate',
      headerName: t("startDate"),
      disableClickEventBubbling: true,
      flex: 4,
      renderCell: params => (
        <Typography className='custome-data-grid-font' >{params?.row?.startDate}</Typography>

      )
    },

     {
      field: 'type',
       headerName: t("TYPE"),
       disableClickEventBubbling: true,
      flex: 2,


     },
    {
      field: 'status',
      headerName: t("STATUS"),
      disableClickEventBubbling: true,
      flex: 4,
      renderCell: params => (
        <Typography className='custome-data-grid-font' >{params?.row?.status}</Typography>

      )
    },


    {
      field: 'action',
      headerName: t("Action"),
      flex: 1,

      renderCell: (params) => {
        return (
          <>
            <Stack direction={{ sm: 'row' }} style={{ ...columnStyles.cell }}>
              <Box sx={{cursor:"pointer"}} onClick={()=> handleOpenClick(params.row)}>
                  <EditIcon
                    style={{
                      color: '#8090A7',
                    }}
                    variant="contained"
                    color="primary"
                    size="small"

                  />

              </Box>



            </Stack>
            {isDrawerOpenEdit && (
              <DrawerForm
                open={isDrawerOpenEdit}
                setOpenParent={setIsDrawerOpenEdit}
                Data={EditData}
              />
            )}

          </>
        );
      },
    },
  ]);
};

export default useAbsenceDailyColumns;
