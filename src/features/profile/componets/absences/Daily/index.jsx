import React, { useEffect } from 'react'
import AbsenceDailyTable from './componets/DataGrid'
import useGetAllAbsenceDailyById from './hooks/useGetAllAbsenceDailyById'

export default function Daily({date,id}) {
  const { data: DataByDaily, mutate: getDataDaily } = useGetAllAbsenceDailyById()


  useEffect(() => {
    getDataDaily({id,date})


  }, [date])

  return (
    <AbsenceDailyTable rows={DataByDaily}/>
  )
}
