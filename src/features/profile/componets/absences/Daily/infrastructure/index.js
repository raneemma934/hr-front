export const AbsenceData = elements => {
  return elements?.data?.data?.all_absences?.map(element => {
    return {
      id: element?.id,
      startDate: element?.startDate,
      type: element?.type_ar,
      status: element?.status,
      unjustified: element?.userUnjustified,
      total: element?.all,
      user_info: element?.userinfo,
      DateAbcense: element?.DateAbcense,
      type: element?.type_ar,
      statusAbsence: element?.statusAbsence,
      specialization: element?.specialization
    }
  })
}
