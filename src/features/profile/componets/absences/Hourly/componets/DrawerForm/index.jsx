import React, { useEffect, useState } from 'react'
import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'
import Drawer from '@mui/material/Drawer'
import List from '@mui/material/List'
import IconButton from '@mui/material/IconButton'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import { Button, Card, CardContent, Divider, MenuItem, TextField, Typography } from '@mui/material'
import { useForm, Controller, useFieldArray } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useTranslation } from 'react-i18next'
import CloseIcon from '@mui/icons-material/Close'
import AddIcon from '@mui/icons-material/Add'
import { Stack } from '@mui/system'
import { useDispatch } from 'react-redux'
import { useAddAbsence } from 'src/features/employee/absenceHourly/hooks/useAddAbsence'
import PasswordModel from '../../hooks/password'

const drawerWidth = 440

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),

  ...theme.mixins.toolbar,
  justifyContent: 'flex-start'
}))

export default function DrawerForm({ open, setOpenParent, Data }) {
  const id = Data?.id
  const { t } = useTranslation()
  const [Type, setType] = useState()
  const [Value, seValue] = useState()
  const [openPasswordModal, setOpenPasswordModal] = useState(false)
  const { mutate: AddAbsence } = useAddAbsence()

  const handleDrawerClose = () => {
    setOpenParent(false)
  }

  const handleTypeChange = e => {
    setType(e.target.value)
  }

  const handleValueChange = e => {
    seValue(e.target.value)
  }

  const { control, handleSubmit, reset } = useForm({
    defaultValues: {
      absence: Array.from({ length: 1 }, () => ({ type: '', date: '' }))
    },
    mode: 'onBlur'
  })

  const handleSubmi = event => {
    event.preventDefault()

    try {
      const formData = new FormData()

      formData.append('type', Type)
      formData.append('id', id)
      const isPaidValue = Value ? '1' : '0'
      formData.append('isPaid', isPaidValue)

      AddAbsence(formData)

      handleClose()
    } catch (error) {}
  }

  return (
    <Drawer
      onClose={handleDrawerClose}
      backgroundColor='#fff'
      sx={{
        flexShrink: 0,
        '& .MuiDrawer-paper': {
          width: { xl: drawerWidth, md: drawerWidth, sm: drawerWidth, xs: '90%' },
          overflowY: 'auto',
          overflowX: 'hidden'
        },
        overflow: 'visible',
        borderRadius: '15px'
      }}
      anchor='right'
      open={open}
      variant='temporary'
      ModalProps={{
        keepMounted: true
      }}
    >
      <Stack spacing={3}>
        <Box
          sx={{
            width: '100%',
            backgroundColor: '#DCE1E6',
            fontSize: '20px',
            gap: '10px',
            padding: '24px',
            color: '#8090A7',

            fontWeight: 600
          }}
        >
          {t('Edit Absence')}
        </Box>

        <Stack spacing={4} sx={{ padding: '0px 24px 0px 24px' }}>
          <Box>
            <Controller
              name={`isPaid`}
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  select
                  fullWidth
                  value={Value}
                  label={t('Paid')}
                  placeholder='Paid'
                  SelectProps={{
                    displayEmpty: true,
                    onChange: e => {
                      handleValueChange(e)
                    }
                  }}
                  size='small'
                >
                  <MenuItem value='0'>{`${t('Paid')}`}</MenuItem>
                  <MenuItem value='1'>{`${t('Unpaid')}`}</MenuItem>
                  <MenuItem value='sick'>{`${t('Sick')}`}</MenuItem>
                </TextField>
              )}
            />
          </Box>
          <Box>
            <Controller
              name={`type`}
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  select
                  fullWidth
                  placeholder='Type'
                  label={t('Type')}
                  value={Type}
                  SelectProps={{
                    displayEmpty: true,
                    onChange: e => {
                      handleTypeChange(e)
                    }
                  }}
                  size='small'
                >
                  <MenuItem value='justified'>{`${t('مبرر ')}`}</MenuItem>
                  <MenuItem value='Unjustified '>{`${t('غير مبرر ')}`}</MenuItem>
                </TextField>
              )}
            />
          </Box>
        </Stack>

        <Box
          sx={{
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'end',
            width: '100%',
            height: '73vh',
            padding: '10px'
          }}
        >
          <Stack sx={{ marginLeft: { sm: '55%' } }} direction={'row'} spacing={2}>
            <Button
              onClick={handleDrawerClose}
              sx={{
                backgroundColor: '#DCE1E6',
                color: '#8090A7',
                borderRadius: '4px',
                padding: '8px 24px',
                '&:hover': { backgroundColor: '#DCE1E6' }
              }}
            >
              {t('Cancel')}
            </Button>
            <Button
              loadingPosition='start'
              variant='contained'
              onClick={handleSubmi}
              sx={{
                borderRadius: '4px',
                padding: '8px 24px'
              }}
            >
              {t('Add')}
            </Button>
          </Stack>
        </Box>
      </Stack>
    </Drawer>
  )
}
