
 import GetAllAbsenceDailyById from '../api/GetAllAbsenceDailyById'

import { useMutation, useQueryClient } from "@tanstack/react-query";

const useGetAllAbsenceDailyById = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:GetAllAbsenceDailyById,
    onSuccess: (data) => {
      queryClient.invalidateQueries("AbsenceDaily");
    },
  });
};



export default useGetAllAbsenceDailyById

