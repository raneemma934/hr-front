import React, { useEffect } from 'react'
import AbsenceHourlyTable from './componets/DataGrid'
import useGetAllAbsenceDailyById from './hooks/useGetAllAbsenceDailyById'

export default function Hourly({ date, id }) {
  const { data: DataByHour, mutate: getDataHour } = useGetAllAbsenceDailyById()

  useEffect(() => {
    getDataHour({ id, date })
  }, [date])
  
  return (
    <>
      <AbsenceHourlyTable rows={DataByHour?.data?.data.all_lates} />
    </>
  )
}
