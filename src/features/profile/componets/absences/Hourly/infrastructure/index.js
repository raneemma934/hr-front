export const AbsenceData = elements => {
  return elements?.map(element => {
    return {
      id: element?.id,
      lateDate: element?.lateDate,
      status: element?.status,
      type: element?.type_ar,
      check_in: element?.check_in,
      hours_num: element?.hours_num,
      name: element?.username,
      last_name: element?.lastname,
      end: element?.end,
      justified: element?.userJustified,
      unjustified: element?.userUnjustified,
      total: element?.all,
      user_info: element?.userinfo,
      specialization: element?.specialization,
      level: element?.userinfo?.level
    }
  })
}
