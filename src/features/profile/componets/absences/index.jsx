import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Daily from './Daily';
import Hourly from './Hourly';
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next';

// import useGetAllAbsence from './Hourly/hooks/useGetAllabsence';
// import useGetAllAbsenceDaily from './Daily/hooks/useGetAllabsence';

function CustomTabPanel(props) {

  const { children, value, index, ...other } = props;

  return (
    <div
    role="tabpanel"
    hidden={value !== index}
    id={`simple-tabpanel-${index}`}
    aria-labelledby={`simple-tab-${index}`}
    {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

CustomTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function Absences({id}) {
  const [value, setValue] = React.useState(0);
  const store = useSelector(state => state.user)

  const {t} = useTranslation()



  const handleChange = (event, newValue) => {
    setValue(newValue);
  };


  return (
    <Box sx={{ width: '100%' }}>
      <Box sx={{  }}>
        <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
          <Tab label={t("Daily")} {...a11yProps(0)} />
          <Tab label={t("Hourly")} {...a11yProps(1)} />
        </Tabs>
      </Box>
      <CustomTabPanel value={value} index={0}>
        <Daily id={id} date={store?.date}/>
      </CustomTabPanel>
      <CustomTabPanel value={value} index={1}>
        <Hourly id={id} date={store?.date}/>
      </CustomTabPanel>

    </Box>
  );
}
