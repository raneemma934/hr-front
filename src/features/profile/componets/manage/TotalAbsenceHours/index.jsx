import React, { useEffect, useMemo } from 'react'
import { Button, Card, CardContent, Grid, Typography, TextField } from '@mui/material'
import { Modal } from '@mui/material'
import { Box, Stack } from '@mui/system'
import { useState } from 'react'
import CustomTextField from 'src/@core/components/mui/text-field'
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined'
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import { useTheme } from '@mui/material/styles'
import { useForm, Controller } from 'react-hook-form'
import { useAddDecision } from '../hook/useAddDecision'
import useMediaQuery from '@mui/material/useMediaQuery'
import { useEditDecision } from '../hook/useEditDecision'
import Paper from '@mui/material/Paper'
import { styled } from '@mui/material/styles'
import { CustomPickerManage } from 'src/@core/components/customPickerManage'
import InsertInvitationIcon from '@mui/icons-material/InsertInvitation'
import CancelRoundedIcon from '@mui/icons-material/CancelRounded'

// import { useAddAbsence } from './hook/useAddAbsence';
// import useGetAbsence from './hook/useGetAbsence';
// import { useDeleteAbsence } from './hook/useDeleteAbsence';
// import { useEditAbsence } from './hook/useEditAbsence';
import { MenuItem } from '@mui/material'
import { useTranslation } from 'react-i18next'
import KeyboardArrowUpRoundedIcon from '@mui/icons-material/KeyboardArrowUpRounded'
import ExpandMoreRoundedIcon from '@mui/icons-material/ExpandMoreRounded'
import useGetCompensationHours, { useAddCompensationHours } from '../hook/useAddCompensationHours'
import { useEditCompensationHours } from '../hook/useEditCompensationHours'

export default function TotalAbsenceHours({ id }) {
  const idUser = id
  const { t } = useTranslation()

  const { data, isLoading } = useGetCompensationHours(idUser)
  const { mutate: editcompensation } = useEditCompensationHours(idUser)

  const [openAdd, setOpenAdd] = React.useState(false)
  const [Edit, setEdit] = useState({})
  const theme = useTheme()
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'))

  const TypoHeader = styled(Typography)(() => ({
    fontSize: '16px',
    marginLeft: '5px',
    fontWeight: '600',
    textTransform: 'capitalize',
    color: '#131627'
  }))

  const StackRow = styled(Stack)(({ direction }) => ({
    flexDirection: direction === 'column' ? 'column' : 'row',
    justifyContent: 'space-between',
    width: '100% ',
    alignItems: 'center'
  }))

  const handleClickOpenAdd = dataRow => {
    setOpenAdd(true)
    setEdit(dataRow)
  }

  const handleCloseAdd = () => {
    setOpenAdd(false)
  }

  const defaultValues = {
    compensation_hours: Edit?.compensation_hours,

    user_id: idUser
  }

  const onSubmit = async data => {
    const formData = new FormData()
    formData.append('compensation_hours', data?.compensation_hours)
    editcompensation({ formData, id: idUser })
    handleCloseAdd()
  }

  const {
    control,
    handleSubmit,

    formState: { errors }
  } = useForm({
    defaultValues: defaultValues,
    mode: 'onBlur'
  })

  return <Card></Card>
}
