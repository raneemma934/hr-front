import { request } from "src/utiltis/AxiosUtilitis";

const DeleteAbsence = (id) => {
  return request({ url: `/api/Decision/remove/${id}`, method: "delete" });
};

export default DeleteAbsence;
