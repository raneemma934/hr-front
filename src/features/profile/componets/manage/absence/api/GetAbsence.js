import { request } from "src/utiltis/AxiosUtilitis"

const GetAbsence = async (payload) => {

  // return request({ url: `/api/Absence/getUserAbsence?user_id=${payload.idUser}&type=advanced&date=${payload.formattedDate}` })
  return request({ url: `/api/Decision/getUserDecisions?user_id=${payload.idUser}&type=advanced&date=${payload.formattedDate}` })


}

export default GetAbsence

