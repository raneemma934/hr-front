import { useMutation, useQueryClient } from "@tanstack/react-query";
import { showSuccesToast } from "src/utiltis/toastSecces";
import EditAbsence from "../api/EditAbsence";
import EditAdvances from "../api/EditAdvances";

export const useEditAdvances = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:EditAdvances,
    onSuccess: (data) => {
      queryClient.invalidateQueries("Advances");
      showSuccesToast(data.data.message,"")
    },
  });
};




