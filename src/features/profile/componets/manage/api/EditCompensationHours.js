import { request } from "src/utiltis/AxiosUtilitis";

const EditCompensationHours = (obj) => {

  return request({
    url: `/api/UserInfo/demandCompensationHours/${obj.id}`,
    method: "post",
    data: obj.formData,
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};


export default EditCompensationHours;


