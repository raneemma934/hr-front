
import { useQuery } from '@tanstack/react-query'
import GetCompensationHours from '../api/GetCompensationHours';

const useGetCompensationHours = (id) => {
  const query = useQuery({
    queryKey: ['compensationHours', id],
    queryFn: () => GetCompensationHours(id),
  });

  return query;
}

export default useGetCompensationHours
