import { useMutation, useQueryClient } from "@tanstack/react-query";
import { showSuccesToast } from "src/utiltis/toastSecces";
import EditCompensationHours from "../api/EditCompensationHours";

export const useEditCompensationHours = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:EditCompensationHours,
    onSuccess: (data) => {
      queryClient.invalidateQueries("EditCompensationHours");
      showSuccesToast(data.data.message,"")
    },
  });
};




