import React, { useEffect, useState } from "react";
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,Typography ,Modal} from "@mui/material";
import { Box, Stack } from '@mui/system'
import CancelRoundedIcon from '@mui/icons-material/CancelRounded';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import { useDeleteNote } from "../../hooks/useDeleteNote";
import { useTranslation } from "react-i18next";

export default function AlertDialogDeleteNote({ id, open, handleClose }) {

  const { mutate: deleteNote, isLoading } = useDeleteNote();

  const { t } = useTranslation();


  const handleDeleteAPI = () => {
      deleteNote(id)
      handleClose()
  };



  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: '#fff',
    border: '2px solid #000',
    boxShadow: 0,
    p: 4,
    textAlign:"center",
    overflow:"hidden",
    borderRadius:"12px"
  };



  return (
    <>
    {/* <Box >
    <Grid columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
    <Dialog
      onClose={handleClose}
      open={open}
      >
      <Grid item xs={12}>
        <Item>
        <DialogContent sx={{height:"99px",width:"100%",borderRadius:"10px",position:"relative",overflow:"visible"}}>
        <DialogContentText sx={{}}>
          <CancelRoundedIcon  style={{ backgroundColor:"#FFFFFF",color: '#A20D29' ,fontSize: 160,position:"fixed",top: "50%", left: "50%", transform: "translate(-50%, -90%)",borderRadius:"50%" }} />

        </DialogContentText>
      </DialogContent>
      <Typography  sx={{fontWeight:"600",fontSize:"16px",color:"#131627"}}>Delete</Typography>


        <DialogTitle style={{ fontSize: "19px", color: '#B4B4B3' }}>
        {"Are you sure you want to delete note?"}
      </DialogTitle>


        <DialogActions style={{ display: 'flex', justifyContent: 'center', padding: '10px' }}>
        <Button onClick={handleClose} style={{ color: '#B4B4B3' }}>Cancel</Button>
        <Button  sx={{color:"#DF2E38"}}  onClick={handleDeleteAPI} autoFocus>
          Delete
        </Button>
      </DialogActions>
      </Item>
      </Grid>
    </Dialog>
    </Grid>
  </Box> */}

  <Box >
      <Grid columnSpacing={{ xs: 1, sm: 2, md: 3 }} >
        <Modal
          open={open}
          onClose={handleClose}
          className="dialog-arabic"
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
          keepMounted={true}
        >
          <Grid item xs={4}>
            <Box sx={style}>
              <Typography  sx={{ fontWeight: "600", fontSize: "16px", color: "#131627" }} id="modal-modal-title" variant="h6" component="h2">{t('Delete')}</Typography>
              <Typography style={{ fontSize: "19px", color: '#B4B4B3',marginTop:"14px" }} id="modal-modal-description" sx={{ mt: 2 }}>
               {t("Are you sure you want to delete note")} ؟
              </Typography>
              <Box marginTop={"14px"} style={{ display: 'flex', justifyContent: 'center', padding: '10px' ,gap:"14px" }}>
                <Button sx={{ color: "#fff",backgroundColor:"#DF2E38",padding:"8px 24px 8px 24px",borderRadius:"4px", "&:hover": { backgroundColor: "#DF2E38" } }} onClick={handleDeleteAPI} autoFocus>
                  {t('Delete')}
                </Button>
                <Button onClick={handleClose} style={{ color: '#fff',backgroundColor:"#B4B4B3",borderRadius:"4px",padding:"8px 24px 8px 24px" }}>{t('Cancel')}</Button>
              </Box>
            </Box>
          </Grid>
        </Modal>
      </Grid>
    </Box>
  </>

  );
}
