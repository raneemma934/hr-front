import { useMutation, useQueryClient } from "@tanstack/react-query";
import AddNote from "../api/Add";
import { showSuccesToast } from "src/utiltis/toastSecces";

export const useAddNote = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:AddNote,
    onSuccess: () => {
      queryClient.invalidateQueries("notes");
      showSuccesToast("success","")
    },
  });
};
