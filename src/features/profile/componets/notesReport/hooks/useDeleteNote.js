import { useMutation, useQueryClient } from "@tanstack/react-query";
import deleteNote from "../api/Delete";
import { showSuccesToast } from "src/utiltis/toastSecces";

export const useDeleteNote = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:deleteNote,
    onSuccess: () => {
      queryClient.invalidateQueries("notes");
      showSuccesToast("success","")
    },
  });
};
