import { useMutation, useQueryClient } from "@tanstack/react-query";
import EditNote from "../api/Edit";
import { showSuccesToast } from "src/utiltis/toastSecces";

export const useEditNote = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:EditNote,
    onSuccess: () => {
      queryClient.invalidateQueries("notes");
      showSuccesToast("success","")

    },
  });
};
