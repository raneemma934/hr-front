import { useQuery, useQueryClient } from '@tanstack/react-query'
import GetAllRequest from '../../api/GetAllrequest'

const useGetAllInquiries = (date) => {
  const queryClient = useQueryClient();

  const query = useQuery({
    queryKey: ['Reports', date],
    queryFn: () => GetAllRequest(date),
  });

  return query
}

export default useGetAllInquiries
