import { request } from "src/utiltis/AxiosUtilitis"

const GetAllRequest = async (payload) => {
  return request({ url: `/api/Request/All?date=${payload}`})
}

export default GetAllRequest
