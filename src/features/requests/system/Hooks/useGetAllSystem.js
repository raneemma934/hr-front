
import { useQuery, useQueryClient } from '@tanstack/react-query'
import GetAllInquiries from '../../api/GetAllSystem'

const useGetAllSystem = (date) => {
  const queryClient = useQueryClient();

  const query = useQuery({
    queryKey: ['system', date],
    queryFn: () => GetAllInquiries(date),
  });

  return query
}

export default useGetAllSystem

