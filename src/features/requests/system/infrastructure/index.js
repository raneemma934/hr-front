  export const SystemData = elements => {


    return elements?.data?.data?.map(element => {



      let countAlert = element?.user?.alert?.length
      let title;

      if(countAlert<3 && countAlert > 0)
        title = "Warning"
      else if (countAlert >=3)
        title = "Dismissed"
      else
        title = "None"

      return {
        id:element?.id,
        user_id:element?.user?.id,
        content: element?.content,
        type: element?.type,
        date:element?.dateTime,
        first_name:element?.user_decision?.first_name,
        last_name:element?.user_decision?.last_name,
        user_info:element?.user?.user_info?.image,
        specialization:element?.user?.specialization,
        department:element?.department?.name?element?.department?.name:'',

      }
    })
  }
