import { request } from '../../../utiltis/AxiosUtilitis'

const GetAllBranches = async () => {
  return request({ url: '/api/branches/' })
}

export default GetAllBranches
