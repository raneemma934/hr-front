import { request } from "src/utiltis/AxiosUtilitis";

const deleteBranch = (data) => {

  return request({ url: `/api/branches/${data?.id}`, method: "delete",data:{
    password:data?.password
  } });
};

export default deleteBranch;
