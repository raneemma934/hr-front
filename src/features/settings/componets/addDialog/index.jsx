import React, { useEffect, useState } from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Typography
} from '@mui/material'
import { Box, Stack } from '@mui/system'
import CancelRoundedIcon from '@mui/icons-material/CancelRounded'
import Grid from '@mui/material/Grid'
import { styled } from '@mui/material/styles'
import Paper from '@mui/material/Paper'
import useAddBranch from '../../hooks/useAdd'
import { useTranslation } from 'react-i18next'

export default function AlertDialogAdd({  open, handleClose }) {

  const { mutate: AddBranch, isLoading } = useAddBranch();

  const {t} = useTranslation()

  const handleSubmit = event =>{

    event.preventDefault();
    AddBranch(event.target[0].value)
    handleClose()

  }

  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary
  }))

  return (


          <Dialog onClose={handleClose} open={open}>
             <Dialog open={open} onClose={handleClose} maxWidth={'xs'} >
      <DialogTitle>Dialog Title</DialogTitle>
      <DialogContent>
        This is the content of the dialog. Padding is set to 1rem top-bottom and 1.5rem left-right.
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary" variant='contained'>
          Close
        </Button>
        <Button onClick={handleClose} color="secondary" variant='contained'>
          Save
        </Button>
      </DialogActions>
    </Dialog>
          </Dialog>

  )
}
