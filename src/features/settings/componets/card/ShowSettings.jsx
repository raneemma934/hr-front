import { Avatar, Button, Card, CardActions, CardContent, CardHeader, Grid, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React from 'react'
import useShowAllBranches from '../../hooks/useShowAllBranches'
import EditIcon from '@mui/icons-material/Edit'
import { useState } from 'react'
import AlertDialog from '../deleteDialog'
import AlertDialogEdit from '../editDialog'
import AddIcon from '@mui/icons-material/Add'
import AlertDialogAdd from '../addDialog'
import { useTranslation } from 'react-i18next'
import { useAuth } from 'src/hooks/useAuth'
import { useRouter } from 'next/router';

export default function ShowSetting() {
  const { data, isloading } = useShowAllBranches()
  const { t } = useTranslation()
  const router = useRouter();
  const [isDeletePopupOpen, setIsDeletePopupOpen] = useState(false)
  const [isEditPopupOpen, setisEditPopupOpen] = useState(false)
  const [isAdd, setisAdd] = useState(false)
  const { logout } = useAuth()
  const [branchRow, setBranch] = useState()

  const handleNavigate = () => {
    router.push('/policies/add/'); // Navigate to /admin using Next.js router
  };

  const handleDelete = branch => {
    setBranch(branch)
    setIsDeletePopupOpen(true)
  }

  const handleClose = () => {
    setIsDeletePopupOpen(false)
  }

  const handleEdit = branch => {

    setBranch(branch)
    router.push('/policies/edit/');
  }

  const handleCloseEdit = () => {
    setisEditPopupOpen(false)
  }


  const handleCloseAdd = _ => {
    setisAdd(false)
  }

  const handleLogout = _ => {
    logout()
  }

  return (
    <>
      <Stack direction={'row'} width={'100%'} justifyContent={'space-between'}>
        <Typography variant='h1' paddingBottom={'24px'}>
          {t('Settings')}
        </Typography>
        <Stack direction={'row'} spacing={6} paddingBottom={'24px'}>

          <Button
        variant="tonal"
        color="primary"
        size="large"
        onClick={handleNavigate}
        startIcon={<AddIcon />} // Setting the start icon
      >
       {t('Add new branch')}
      </Button>

          <Button
           variant="contained"
           color="error"
           size="large"
            onClick={handleLogout}

          >
            {t('Logout')}
          </Button>
        </Stack>
      </Stack>

      {isDeletePopupOpen && <AlertDialog Branch={branchRow} open={isDeletePopupOpen} handleClose={handleClose} />}

      {isEditPopupOpen && <AlertDialogEdit Branch={branchRow} open={isEditPopupOpen} handleClose={handleCloseEdit} />}
      {isAdd ? <AlertDialogAdd open={isAdd} handleClose={handleCloseAdd} /> : null}
      <Grid  container spacing={8}>
        {data?.data?.data?.map((branch, index) => (
          <Grid item sm={6} xs={12} key={index}>
            <Card skin="bordered" >
            <CardHeader
            title={branch.name}
            avatar={<Avatar variant="rounded" alt={branch.name} src={branch.image} />}

          />
              <CardContent>

                <Typography variant="body2" component="div" style={{ display: 'flex', alignItems: 'center',marginBottom:'12px',marginTop:'16px' }}>
              <Typography variant="h3">Work days :</Typography>
              {branch.policy.work_days.length > 0 ?  branch.policy.work_days.map((day,index)=>(<Typography key={index} variant="body2" style={{ marginLeft: '10px',color:'#8090A7' }}>{index < branch.policy.work_days.length - 1 ? day + ' - ' : day}</Typography>))  : 'no works days'


              }
            </Typography>
            <Typography variant="body2" component="div" style={{ display: 'flex', alignItems: 'center' }}>
              <Typography variant="h3">Work Time :</Typography>
              <Typography variant="body2" style={{ marginLeft: '10px',color:'#8090A7' }}>{branch.policy.start_time} - {branch.policy.end_time}</Typography>
            </Typography>
            </CardContent>
            <CardActions sx={{ justifyContent: 'end', gap: 5 }}>


                  <Button
        variant="tonal"
        color="secondary"
        size="medium"
        onClick={() => {
          handleEdit(branch)
        }}
        startIcon={<EditIcon />} // Setting the start icon
      >
        {t('Edit')}
      </Button>

                  <Button variant="outlined" color="error" onClick={() => {
                      handleDelete(branch)
                    }} >
         {t('Delete')}
      </Button>

 </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>

    </>
  )
}
