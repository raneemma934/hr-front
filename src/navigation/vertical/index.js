import ComplaintsIcon from '../../../public/images/IconInput/complaints'
import HierarchyIcon from '../../../public/images/IconInput/hierarchy'
import HomeIcon from '../../../public/images/IconInput/home'
import EmployeeIcon from '../../../public/images/IconInput/employee'
import ContractIcon from '../../../public/images/IconInput/contract'
import OrderLigthIcon from '../../../public/images/IconInput/Order_light'
import ReviewsIcon from '../../../public/images/IconInput/reviews'
import SalaryIcon from '../../../public/images/IconInput/Salary'
import RequestsIcon from '../../../public/images/IconInput/Requests'
import Calender from '../../../public/images/IconInput/Calender'
import EmailIcon from '../../../public/images/IconInput/Emai'
import ArachiveIcon from '../../../public/images/IconInput/Arachive'
import System from '../../../public/images/IconInput/System'
import Branches from '../../../public/images/IconInput/Branches'
import AbcensesIcon from '../../../public/images/IconInput/Abcenses'

const navigation = () => {
  return [
    {
      title: 'Home',
      icon: <HomeIcon />,
      path: '/dashboard'
    },
    {
      title: 'Employees',
      icon: <EmployeeIcon />,
      children: [
        {
          title: 'List',
          path: '/employees/users'
        },
        {
          title: 'Departments',
          path: '/employees/team'
        },
        {
          title: 'Registeration2',
          path: '/employees/registeration'
        },

        {
          title: 'Secretariats',
          path: '/employees/secretariats'
        },
        {
          title: 'Add',
          path: '/employees/add'
        }
      ]
    },

    {
      title: 'Absence',
      icon: <AbcensesIcon />,
      children: [
        {
          title: 'Daily Absence',
          path: '/employees/absence'
        },

        {
          title: 'Hourly Absence',
          path: '/employees/absencehourly'
        }
      ]
    },

    {
      title: 'Contracts',
      icon: <ContractIcon />,
      children: [
        {
          title: 'Employees',
          path: '/contracts/list'
        },
        {
          title: 'Add',
          path: '/contracts/add'
        }
      ]
    },
    {
      title: 'Reports',
      icon: <OrderLigthIcon />,
      path: '/report'
    },

    {
      title: 'Salaries',
      icon: <SalaryIcon />,
      path: '/salary'
    },
    {
      title: 'Requests',
      icon: <RequestsIcon />,
      path: '/requests/system'
    },
    {
      title: 'Calendar',
      icon: <Calender />,
      path: '/calendar'
    },
    {
      title: 'Archive',
      icon: <ArachiveIcon />,
      path: '/Archive',
      children: [
        {
          title: 'Employees',
          path: '/archive/employees'
        },
        {
          title: 'Contracts',
          path: '/archive/contracts'
        }
      ]
    },
    {
      title: 'Policies',
      icon: <System />,
      path: '/policies/view'
    },
    {
      title: 'Branches',
      icon: <Branches />,
      path: '/Settings'
    },


  ]
}

export default navigation
