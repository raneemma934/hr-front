import React from 'react'
import Contract from 'src/features/archive/contracts/list/Componets/DataGrid'
import useGetArchiveContracts from 'src/features/archive/contracts/list/Hooks/useGetArchiveContracts'

export default function ContractArchive() {

  const {data:rows} = useGetArchiveContracts()


  return (
    <Contract  rows={rows?.data?.data}/>
  )
}
