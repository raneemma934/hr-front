import React from 'react'
import useGetAllComplaints from 'src/features/complaints/Hook/useGetAllComplaints'
import ComplaintsTable from 'src/features/complaints/components/DataGrid'
import { CircularProgress } from '@mui/material'
import { Box } from '@mui/system'
import { useSelector } from 'react-redux'

export default function Complaints() {
  const store = useSelector(state => state.user)
  const date = store?.date; // Example date, replace it with your actual date
  const {data} = useGetAllComplaints(date)

  return <>

{data?
<ComplaintsTable Data={data?.data?.data}/>:

<Box sx={{display:"flex",justifyContent:"center",alignItems:"center",height:"70vh"}}>

<CircularProgress className='loading-rtl'/>
</Box>
}


  </>

}
