import { Box } from '@mui/system'
import React, { useEffect, useState } from 'react'
import Attendance from 'src/features/dashboard/attendance'
import AppCalendar from 'src/features/dashboard/calendar'
import Registration from 'src/features/dashboard/registration/components/DataGrid'
import { useDispatch, useSelector } from 'react-redux'
import { getAttendancePercentage, getRegisteration } from './store'
import { Grid } from '@mui/material'
import { useTranslation } from 'react-i18next'
import moment from 'moment'
import useGetAllData from 'src/features/dashboard/Registarion/hooks/useGetData'

export default function Dashboard() {
  const store = useSelector(state => state.Dashboard)

  const [registration, setRegistration] = useState([])
  const [percentageData, setpercentageData] = useState([])
  const { t } = useTranslation()
  const dispatch = useDispatch()
  const formattedDate = moment().format('YYYY-MM-DD')
  const { data, isloading } = useGetAllData()
  useEffect(() => {
    dispatch(getAttendancePercentage())
    setpercentageData(store?.AttendancePercentage)
  }, [store?.AttendancePercentage?.length])

  useEffect(() => {
    dispatch(getRegisteration(formattedDate))
    setRegistration(store?.Registertion)
  }, [store?.Registertion?.length])

  return (
    <>
      {/* <Loading/> */}
      <Grid container spacing={6} sx={{ overflow: 'hidden' }}>
        <Grid item sm={8} xs={12}>
          <Box sx={{ backgroundColor: '#fff', borderRadius: '12px' }}>
            <AppCalendar />
          </Box>
        </Grid>

        <Grid sx={{ position: 'relative', borderRadius: '12px' }} item sm={4} xs={12}>
          <Attendance Data={percentageData} />
        </Grid>
        <p
          className='Attendance'
          style={{
            position: 'absolute',
            right: '18%',
            marginTop: '37px',
            fontSize: '20px',
            fontWeight: '600',
            color: '#8090a7'
          }}
        >
          {t('Attendance')}
        </p>

        <Grid item sm={12} xs={12}>
          <Registration Data={registration} />
        </Grid>
      </Grid>
    </>
  )
}
