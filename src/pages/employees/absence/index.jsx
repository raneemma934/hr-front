import { CircularProgress } from '@mui/material'
import { Box } from '@mui/system'
import { useEffect } from 'react'
import { useSelector } from 'react-redux'
import Absence from 'src/features/employee/absence/componets/DataGrid'
import { useGetAllAbsence } from 'src/features/employee/absence/hooks/useGetAllabsence'


export default function Employees() {
  const store = useSelector(state => state.user)
  const date = store?.date; // Example date, replace it with your actual date
const { data: absenceData, isLoading, isError } = useGetAllAbsence(date);


  return (
    <>

    {absenceData ? <Absence rows = {absenceData}/>:<Box height={'100%'} display={'center'} justifyContent={'center'}  alignItems={'center'} >
   <Box sx={{display:"flex",justifyContent:"center",alignItems:"center",height:"70vh"}}>

<CircularProgress className='loading-rtl'/>
</Box>    </Box>}
    </>
  )

}
