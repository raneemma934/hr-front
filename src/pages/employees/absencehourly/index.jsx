import { CircularProgress } from '@mui/material'
import { Box } from '@mui/system'
import { useSelector } from 'react-redux'

import AbsenceHourlyTable from 'src/features/employee/absenceHourly/componets/DataGrid'
import { useGetAllAbsence } from 'src/features/employee/absenceHourly/hooks/useGetAllabsence'


export default function AbsenceHourly() {
  const store = useSelector(state => state.user)
  const date = store?.date; // Example date, replace it with your actual date
const { data, isLoading, isError } = useGetAllAbsence(date);


  return (
    <>
    {data ? <AbsenceHourlyTable rows = {data}/>:<Box height={'100%'} display={'center'} justifyContent={'center'}  alignItems={'center'} >
   <Box sx={{display:"flex",justifyContent:"center",alignItems:"center",height:"70vh"}}>

<CircularProgress className='loading-rtl'/>
</Box>    </Box>}
    </>
  )

}
