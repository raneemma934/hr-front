import { request } from "src/utiltis/AxiosUtilitis"

const GetAllSelectBranch = async () => {
  return request({ url: '/api/branches/' })
}

export default GetAllSelectBranch
