import { request } from "src/utiltis/AxiosUtilitis"

const GetAllSelect = async () => {
  return request({ url: '/api/specializations' })
}

export default GetAllSelect
