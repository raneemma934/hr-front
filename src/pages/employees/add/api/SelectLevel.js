import { request } from "src/utiltis/AxiosUtilitis"

const GetAllLevel = async () => {
  return request({ url: '/api/levels' })
}

export default GetAllLevel
