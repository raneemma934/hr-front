import { request } from "src/utiltis/AxiosUtilitis"

const GetUser = async (id) => {
  return request({ url: `/api/employees/${id}` })
}

export default GetUser
