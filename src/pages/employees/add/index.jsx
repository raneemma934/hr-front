import React, { useEffect, useState } from 'react'
import { Button, CircularProgress, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import Account from 'src/features/employee/add/Componets/Account'
import Contact from 'src/features/employee/add/Componets/Contact'
import Employment from 'src/features/employee/add/Componets/Employment'
import Info from 'src/features/employee/add/Componets/Personal Info'
import Professional from 'src/features/employee/add/Componets/Professional'
import Skills from 'src/features/employee/add/Componets/Skills'
import Snapshot from 'src/features/employee/add/Componets/Snapshot'
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useFieldArray } from 'react-hook-form'
import EmergencyContact from 'src/features/employee/add/Componets/emergenc-contact'
import AdditionalFiles from 'src/features/employee/add/Componets/additional-fils'
import { Schema } from './validation'
import { useAddUsers } from './hook/useAddUsers'
import AddIcon from '@mui/icons-material/Add'
import { useTranslation } from 'react-i18next'
import { showErrorToast } from 'src/utiltis/showErrorToast'
import { useRouter } from 'next/router'
import useGetUser from './hook/useGetUser'
import { useEditUsers } from './hook/useEditUserTotal'
import { useAddContract } from 'src/features/Contracts/list/Hooks/useAddContract'
import { useDropzone } from 'react-dropzone'

export default function Add() {
  const [snapshotData, setSnapshotData] = useState({})
  const [accountData, setAccountData] = useState({})
  const [infoData, setInfoData] = useState({})
  const [contactData, setContactData] = useState({})
  const [professionalData, setProfessionalData] = useState({})
  const [skillsData, setSkillsData] = useState({})
  const [employmentData, setEmploymentData] = useState({})
  const [AdditionalFilesData, setAdditionalFilesData] = useState({})
  const [EmergencyContactData, setEmergencyContacttData] = useState({})
  const [ProfileImage, setProfileImage] = useState()
  const [files, setFiles] = useState([])

  const { t } = useTranslation()

  const { mutate: addUsers } = useAddUsers()

  const { mutate: EditUsers } = useEditUsers()

  const router = useRouter()
  let { user_id } = router.query

  const { data: ShowUser, isLoading } = useGetUser(user_id)
  console.log("🚀 ~ Add ~ ShowUser:", ShowUser?.data?.data)



  const handleFieldChange = (field, value) => {
    onDataChange(prevData => ({ ...prevData, [field]: value }))
  }

  ShowUser?.data?.data?.[0]?.deposits?.forEach((element, key) => {
    element.object = element?.description
    element.delivery_date = element?.received_date
  })

  //const phonenumbers = ShowUser?.data?.data?.[0]?.my_contacts.map(contact => ({ phone: contact.phone_num }))

  const defaultValues = {
    pin: '',
    first_name: ShowUser?.data?.data?.first_name || '',
    middle_name: ShowUser?.data?.data?.middle_name || '',
    last_name: ShowUser?.data?.data?.last_name || '',
    email: ShowUser?.data?.data?.user_email || '',
    birth_date: ShowUser?.data?.data?.birth_date || '',
    id_number: ShowUser?.data?.data?.id_number || '',
    pin: ShowUser?.data?.data?.pin || '',
    health_status: ShowUser?.data?.data?.health_status || '',
    gender: ShowUser?.data?.data?.gender || '',
    military_situation: ShowUser?.data?.data?.military_situation || 'Exempt',
    level: ShowUser?.data?.data?.level || '',
    military_status: ShowUser?.data?.data?.social_situation || '',

    salary: ShowUser?.data?.data?.salary || '',
    address: ShowUser?.data?.data?.address || '',

    branch_id: ShowUser?.data?.data?.branch_id || '',
    department_id: ShowUser?.data?.data?.department_id || '',
    contacts: {
      emails: ShowUser?.data?.data?.emails || [''],
      phones: ShowUser?.data?.data?.phones || ['']
    },
    skills_and_career:{
      skills: ShowUser?.data?.data?.skills || [{ skill: '', rate: '' }],
      educations: ShowUser?.data?.data?.study_situations || [{ study: '', degree: '' }],
      languages: ShowUser?.data?.data?.languages || [{ language: '', rate: '' }],
      experiences: ShowUser?.data?.data?.careers || [],
      certificates: ShowUser?.data?.data?.certificates || [],


    },

   // emergency_contact: ShowUser?.data?.data?.emergency,
    role: ShowUser?.data?.data?.role,
    password: ShowUser?.data?.data?.password,

  }

  const {
    control,
    setError,
    handleSubmit,
    getValues,
    setValue,
    register,
    watch,
    reset,
    formState: { errors }
  } = useForm({
    defaultValues,
    mode: 'onBlur',
    resolver: Boolean(ShowUser?.data?.data?.length) ? undefined : yupResolver(Schema)
  })

  const [contractFile, SetcontractFile] = useState(null)

  const handleDataSubmit = data => {
    const formData = new FormData()
    formData.append('image', ProfileImage)
    formData.append('contract', contractFile)
    data.image = ProfileImage

    formData.append('path', files[0])



    addUsers(getValues())
  }

  const handleDataEditSubmit = data => {
    const formData = new FormData()
    formData.append('image', ProfileImage)

    data.image = ProfileImage

    EditUsers({ id: user_id, data: data })
  }

  const handleRatingChange = (index, newValue) => {
    const updatedSkills = [...getValues('skills_and_career.skills')]
    updatedSkills[index].rate = newValue

  }

  const handleLanguageChange = (index, newValue) => {
    const updatedLanguages = [...getValues('skills_and_career.languages')]
    updatedLanguages[index].rate = newValue

  }

  useEffect(() => {
    Object.entries(defaultValues).forEach(([field, value]) => {
      setValue(field, value)
    })
  }, [ShowUser])

  if (isLoading) {
    return (
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%' }}>
        <CircularProgress />
      </div>
    )
  }

  return (
    <>
      <Box width={'100%'} display={'flex'} justifyContent={'space-between'} alignItems={'center'}>
        <Typography variant='h2'>{ShowUser?.data?.data.length === 0 ? t('Add Employee') : t('Edit Employee')}</Typography>
        <Button
          type='submit'
          sx={{
            backgroundColor: '#6AB2DF',
            color: '#fff',
            ':hover': { color: '#fff', backgroundColor: '#2A4759' }
          }}
          onClick={ handleDataSubmit }
        >
          <Stack direction={'row'}>
            <AddIcon />
            <Typography color={'inherit'}>
              {ShowUser?.data?.data.length === 0 ? t('Add Employee') : t('Edit Employee')}
            </Typography>
          </Stack>
        </Button>
      </Box>
      <Stack marginTop={'1%'} direction={{ sm: 'row', xs: 'column' }} spacing={8}>
        <Stack spacing={8} width={{ sm: '50%' }} direction={{ sm: 'column', xs: 'column' }}>
          <Box>
            <Snapshot
              onDataChange={setSnapshotData}
              errors={errors}
              defaultValues={defaultValues}
              setError={setError}
              control={control}
              Controller={Controller}
              setProfileImage={setProfileImage}
              ShowUser={ShowUser}
            />
          </Box>
          <Box>
            <Account
              errors={errors}
              onDataChange={setAccountData}
              setError={setError}
              defaultValues={defaultValues}
              control={control}
              Controller={Controller}
              watch={watch}
              register={register}
              ShowUser={ShowUser}
            />
          </Box>
          <Box>
            <Info
              errors={errors}
              onDataChange={setInfoData}
              defaultValues={defaultValues}
              setError={setError}
              control={control}
              Controller={Controller}
              passwordValue={getValues('password')}
              ShowUser={ShowUser}
            />
          </Box>
          <Box>
            <Contact
              errors={errors}
              defaultValues={defaultValues}
              handleFieldChange={handleFieldChange}
              setError={setError}
              control={control}
              Controller={Controller}
              onDataChange={setContactData}
            />
          </Box>
          <Box>
            <EmergencyContact
              errors={errors}
              handleFieldChange={handleFieldChange}
              setError={setError}
              control={control}
              Controller={Controller}
              onDataChange={setEmergencyContacttData}
            />
          </Box>
          <Box>
            {/* <AdditionalFiles
              errors={errors}
              handleFieldChange={handleFieldChange}
              setError={setError}
              control={control}
              Controller={Controller}
              onDataChange={setAdditionalFilesData}
            /> */}
          </Box>
        </Stack>

        <Stack flex={1} direction={'column'} spacing={8}>
          <Box>
            <Professional
              onDataChange={setProfessionalData}
              setError={setError}
              control={control}
              Controller={Controller}
              errors={errors}
              ShowUser={ShowUser}
            />
          </Box>
          <Box>
            <Skills
              errors={errors}
              handleLanguageChange={handleLanguageChange}
              handleRatingChange={handleRatingChange}
              handleFieldChange={handleFieldChange}
              setError={setError}
              control={control}
              Controller={Controller}
              onDataChange={setSkillsData}
              ShowUser={ShowUser}
            />
          </Box>
          <Box>
            <Employment
              errors={errors}
              onDataChange={setEmploymentData}
              handleFieldChange={handleFieldChange}
              setError={setError}
              control={control}
              Controller={Controller}
              ShowUser={ShowUser?.data?.data[0]?.contract[0]?.path}
              SetcontractFile={SetcontractFile}
            />
          </Box>
        </Stack>
      </Stack>
    </>
  )
}
