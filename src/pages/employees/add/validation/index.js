import * as yup from 'yup'

export const Schema = yup.object().shape({
  first_name: yup.string().required('الرجاء ادخال الاسم الاول').min(3, ' min 3 char').max(20, '20 max char'),
  middle_name: yup.string().required('الرجاء ادخال اسم الاوسط').min(3, ' min 3 char').max(20, '20 max char'),
  last_name: yup.string().required('الرجاء ادخال الكنية').min(3, ' min 3 char').max(20, '20 max char'),
  start_date: yup.string().required('الرجاء ادخال تاريخ البدء').min(3, ' min 3 char').max(20, '20 max char'),

  password: yup
    .string()
    .required('الرجاء ادخال كلمة المرور')
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@$!%*#?&]{8,}$/,
      'Password must contain at least 8 characters, one letter, and one number'
    ),
  confirm_password: yup
    .string()
    .required('الرجاء ادخال تأكيد كلمة المرور')
    .oneOf([yup.ref('password'), null], 'Passwords must match'),

  email: yup.string().email().required('الرجاء ادخال الايميل'),

  // address: yup.string().required("address is required"),
  birth_date: yup
    .string()
    .required('الرجاء ادخال تاريخ الميلاد')
    .matches(/^\d{4}-\d{2}-\d{2}$/, 'شكل التاريخ المطلوب : 2020-12-12'),
  nationalID: yup
    .string()
    .required('الرجاء ادخال المنطقة')
    .matches(/^\d{11}$/, 'الرقم الوطني من 11 خانة'),

  // health_status:yup.string().required("health status required").max(250,"max 250 charectar"),
  gender: yup.string().required('الرجاء اختيار الجنس'),

  //social_situation: yup.string().required("socialsituation is required"),
  //specialization: yup.string().required('Specialization is required'),
  contract: yup.object().shape({
    startTime: yup.date().required('تاريخ مطلوب')
  }),

  // level: yup.string().required('Level is required'),
  branch_id: yup.string().required(' الفرع مطلوب')

  //  contacts: yup.object().shape({

  //   phonenumbers: yup.array().of(
  //     yup.object().shape({
  //       phone_num:yup.string().matches(/^09\d{8}$/, 'Invalid Syrian phone number').required("phone is required")
  //   })),

  //   emails:yup.array().of(
  //     yup.object().shape({
  //       email:yup.string().email('Invalid email address')
  //     }))

  // }),

  // emergency_contact: yup.array().of(
  //   yup.object().shape({
  //     name: yup.string().required('Name is required'),
  //     phonenumber: yup.string().matches(/^09\d{8}$/, 'Invalid Syrian phone number').required('Phone Number is required'),
  //     email: yup.string().email('Invalid email address')
  //   })
  // ),

  // additionalfiles: yup.array().of(
  //   yup.object().shape({
  //     description: yup.string().required('Description is required'),
  //     file: yup.mixed().required('File is required'),
  //   })
  // ),

  // educations: yup.array().of(
  //   yup.object().shape({
  //     study: yup.string()

  //     // degree: yup.string().oneOf(['Degree', 'bachelor', 'master', 'phd'], 'Invalid degree').required('Degree is required'),
  //   })
  // ),

  // salary: yup.number().required('Salary is required'),

  // secretariats: yup.array().of(
  //   yup.object().shape({
  //     object: yup.string(),
  //     delivery_date: yup.string().matches(/^\d{4}-\d{2}-\d{2}$/, 'Valid date format required Examp : 2020-12-12'),

  //   })
  // ),
})

// finance: yup.string().required('wallet is required')
