import { Grid } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React, { useState } from 'react'
import Logo from 'src/features/Contracts/view/logo'

import {  Button, Card, CardContent, Chip,  TextField, Typography } from '@mui/material';
import { useForm, Controller } from 'react-hook-form';
import { useAddPolicies } from '../hook/useAddPolicies'
import WorkTime from 'src/features/policies/add/componets/workTime';
import Annual from 'src/features/policies/add/componets/annual';
import Reviews from 'src/features/policies/add/componets/reviews';
import Warnings from 'src/features/policies/add/componets/warnings';
import AbsencesManagement from 'src/features/policies/add/componets/absences';
import Deductions from 'src/features/policies/add/componets/deductions';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { Schema } from '../validation';
import PickersTime, { CustomTimePicker } from 'src/@core/components/customPickerTime';
import { convertTo12HourFormat } from 'src/utiltis/12hFormat';
import { useDispatch } from 'react-redux';
import { addUser } from 'src/store/apps/user';
import Header from 'src/features/Contracts/add/header';




export default function Policies() {
  const { mutate: AddPolicies, isLoading } = useAddPolicies();
  const [days, setDays] = React.useState();
  const [alert , setAlert] = useState(0);
  const [warningsto , setWarningsto] = useState(0);
  const [Paid  , setPaid ] = useState(0);
  const [Unpaid  , setUnpaid ] = useState(0);
  const dispatch = useDispatch();
  const [Sick , setSick] = useState(0);
  const {t} = useTranslation()
  console.log("🚀 ~ Policies ~ days:", days)



  const defaultValues = {
  name:'',
  ip:'',
  work_days:[],
    policy:{
      number_of_warnings:'',
      end_time:'',
      start_time:'',
      number_of_alerts:'',
      minutes_grace_period:'',
    },

 };

 const handleDataSubmit =  (data) => {
 console.log("🚀 ~ handleDataSubmit ~ data:", getValues())



     setValue('work_days',days)
     setValue('policy.number_of_alerts',alert)
     setValue('policy.number_of_warnings',warningsto)
     setValue('policy.number_of_paid_days_in_absence',Paid)
     setValue('policy.number_of_unpaid_days_in_absence',Unpaid)
     setValue('policy.sick_absence_days',Sick)
     try{
      AddPolicies(getValues())
     }
     catch(error){
      console.log(error)
     }


};


  const {
    control,
    setError,
    handleSubmit,
    getValues,
    setValue,
    register,
    reset,
    formState: { errors, },
  } = useForm({
    defaultValues,
    mode: 'onBlur',

      resolver: yupResolver(Schema),
  });





  return (
    <>
    <Stack direction={"row"} justifyContent={"space-between"} padding={"20px"}>
    <Box>
      <Typography variant='h1'>
      {t("Branches / Add Branches")}
      </Typography>
    </Box>
    <Stack direction={"row"} alignItems={"center"} gap={"5px"}>
      <Box>
        {/* <Button   sx={{fontWeight:"500",backgroundColor:"#8090a7",color:"#fff",':hover': { color: '#fff', backgroundColor: '#2A4759' }}}>
        {t("Cancel")}
        </Button> */}
      </Box>
      <Box >
        <Button
         onClick={handleDataSubmit}
        sx={{color:"#fff",borderRadius:"8px",padding:"10px",backgroundColor:"#6ab2df",fontWeight:"600" ,':hover': { color: 'inhert', backgroundColor: '#2A4759' }      }}>
        {t("Save changes")}
        </Button>
      </Box>
    </Stack>
    </Stack>
    <Header errors={errors} defaultValues={defaultValues} control={control} Controller={Controller} />
    <Grid container spacing={6}>
    <Grid item sm={6} xs={12} marginTop={'24px'} >

        <Stack  spacing={6} direction={'column'}>
          <Box  >

            <WorkTime  errors={errors} defaultValues={defaultValues} control={control} Controller={Controller} setDays={setDays} days={days}/>

          </Box>


          {/* <Box >
            <Reviews errors={errors}  defaultValues={defaultValues} control={control} Controller={Controller}/>
          </Box> */}
        </Stack>
    </Grid>

    <Grid item sm={6}  xs={12} marginTop={'24px'}>

      <Stack spacing={6} direction={'column'}>
        <Warnings errors={errors} defaultValues={defaultValues} control={control} Controller={Controller} setAlert={setAlert} alert={alert} setWarningsto={setWarningsto} warningsto={warningsto}/>
        {/* <Annual errors={errors} defaultValues={defaultValues} control={control} Controller={Controller} setAlert={setAlert} alert={alert} setWarningsto={setWarningsto} warningsto={warningsto}/> */}

        <AbsencesManagement control={control} Controller={Controller} setPaid={setPaid} Paid={Paid} Unpaid={Unpaid} setUnpaid={setUnpaid} setSick={setSick} Sick={Sick}/>
        {/* <Deductions control={control} Controller={Controller} /> */}
      </Stack>

    </Grid>

    </Grid>
    </>

  )
}
