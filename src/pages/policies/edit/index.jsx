import { Grid } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React, { useEffect, useState } from 'react'
import Logo from 'src/features/Contracts/view/logo'

import {  Button, Card, CardContent, Chip,  TextField, Typography } from '@mui/material';
import { useForm, Controller } from 'react-hook-form';
import { useAddPolicies } from '../hook/useAddPolicies'
import WorkTime from 'src/features/policies/edit/componets/workTime';
import Annual from 'src/features/policies/edit/componets/annual';
import Reviews from 'src/features/policies/edit/componets/reviews';
import Warnings from 'src/features/policies/edit/componets/warnings';
import AbsencesManagement from 'src/features/policies/edit/componets/absences';
import Deductions from 'src/features/policies/edit/componets/deductions';
import { useTranslation } from 'react-i18next';
import { yupResolver } from '@hookform/resolvers/yup';
import { Schema } from '../validation';
import useShowPolicies from 'src/features/policies/hook/useShowPolicies';
import { useEditPolicies } from '../hook/useEditPolicies';
import { convertTo12HourFormat } from 'src/utiltis/12hFormat';
import Header from 'src/features/Contracts/add/header';




export default function EditPolicies() {
  const {data}=useShowPolicies()
  const Data = data?.data.data
  console.log("🚀 ~ EditPolicies ~ Data:", Data)

  const {mutate: EditPolicie}=useEditPolicies()

  const defaultValues = {
    name:Data?.name,
    ip:Data?.ip,
    policy:{
      start_time:Data?.policy?.start_time,
      minutes_grace_period:Data?.policy?.minutes_grace_period,
      end_time:Data?.policy?.end_time
    },

  }


  const [days, setDays] = React.useState();

  const [alert , setAlert] = useState(0);
  const [warningsto , setWarningsto] = useState(0);
  const [Paid  , setPaid ] = useState();

  const [Unpaid  , setUnpaid ] = useState(0);
  const [Sick , setSick] = useState(0);
  const [deductions , seDeductions] = useState(0);
  console.log("🚀 ~ EditPolicies ~ deductions:",  Number(deductions))
  const [annualSalarypercentage,setAnnualSalarypercentage] = useState()


  const {t} = useTranslation()

  useEffect(()=>{
    setValue('policy.start_time',Data?.policy?.start_time)
    setValue('policy.end_time',Data?.policy?.end_time)
    setValue('policy.minutes_grace_period',Data?.policy?.minutes_grace_period)
    setValue('ip',Data?.ip)
    setValue('name',Data?.name)
    setPaid(Data?.policy?.number_of_paid_days_in_absence)
    setUnpaid(Data?.policy?.number_of_unpaid_days_in_absence)
    setSick(Data?.policy?.number_of_sick_days_in_absence)
    setAlert(Data?.policy?.number_of_alerts)
    setWarningsto(Data?.policy?.number_of_warnings )

    setDays(Data?.policy?.work_days)
    setAnnualSalarypercentage(Data?.policy.annual_salary_increase?.annual_salary_percentage)

  },[data])








  const {
    control,
    setError,
    handleSubmit,
    getValues,
    setValue,
    register,
    reset,
    formState: { errors },
  } = useForm({
    mode: 'onBlur',
    defaultValues,

     resolver: yupResolver(Schema),
  });


  const handleDataSubmit2 = (data) => {

    setValue('_method','PUT')
    setValue('work_days',days)
    setValue('policy.number_of_alerts',alert)
    setValue('policy.number_of_paid_days_in_absence',Paid)
    setValue('policy.number_of_unpaid_days_in_absence',Unpaid)
    setValue('policy.number_of_sick_days_in_absence',Sick)



      EditPolicie(getValues());


  };

  function name(params) {
    console.log("done");

  }


  return (
    <>
    <Stack direction={"row"} justifyContent={"space-between"} padding={"20px"}>
    <Box>
    <Box>
      <Typography variant='h1'>
      {t("Policies / Edit Policies")}
      </Typography>
    </Box>
    </Box>
    <Stack direction={"row"} alignItems={"center"} gap={"5px"}>
      <Box>

      </Box>
      <Box >
        <Button type='submit'onClick={handleDataSubmit2} sx={{color:"#fff",borderRadius:"8px",padding:"10px",backgroundColor:"#6ab2df",fontWeight:"600" ,':hover': { color: 'inhert', backgroundColor: '#2A4759' }      }}>
        {t("Save changes")}
        </Button>
      </Box>
    </Stack>
    </Stack>
    <Header errors={errors} defaultValues={defaultValues} control={control} Controller={Controller} />
    <Grid container spacing={6}>
    <Grid item sm={6} xs={12} marginTop={'24px'} >

        <Stack  spacing={6} direction={'column'}>
          <Box  >

            <WorkTime getValues={getValues} defaultValues={defaultValues} reset={reset} setValue={setValue} EditData={Data} errors={errors} control={control} Controller={Controller} setDays={setDays} days={[days]}/>

          </Box>

          {/* <Box >
            <Annual EditData={Data} errors={errors}  control={control} annualSalarypercentage={annualSalarypercentage} setAnnualSalarypercentage={setAnnualSalarypercentage} Controller={Controller}/>
          </Box> */}

        </Stack>
    </Grid>

    <Grid item sm={6}  xs={12} marginTop={'24px'}>

      <Stack spacing={6} direction={'column'}>
        <Warnings EditData={Data} errors={errors}  control={control} Controller={Controller} setAlert={setAlert} alert={alert} setWarningsto={setWarningsto} warningsto={warningsto}/>
        <AbsencesManagement control={control} Controller={Controller} setPaid={setPaid} Paid={Paid} Unpaid={Unpaid} setUnpaid={setUnpaid} setSick={setSick} Sick={Sick}/>

        {/* <Deductions deductions={deductions} seDeductions={seDeductions}  EditData={Data} control={control} Controller={Controller} />
       */}
      </Stack>

    </Grid>

    </Grid>
    </>

  )
}
