import { useMutation, useQueryClient } from "@tanstack/react-query";
import AddPolicies from "../api/AddPolicies";
import { ShowErrorToast } from "src/utiltis/showErrorToast";
import { showSuccesToast } from "src/utiltis/toastSecces";

export const useAddPolicies = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: AddPolicies,
    onSuccess: (data) => {
      // Invalidate the relevant query to refresh the data after successful mutation
      queryClient.invalidateQueries("AddPolicies");
      showSuccesToast("", "Policies added successfully");
    },
    onError: (error) => {

      ShowErrorToast(error);
    }
  });
};
