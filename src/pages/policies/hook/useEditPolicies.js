import { useMutation, useQueryClient } from "@tanstack/react-query";
import EditPolicies from "../api/EditPolicies";
import { showSuccesToast } from "src/utiltis/toastSecces";
import { ShowErrorToast } from "src/utiltis/showErrorToast";

export const useEditPolicies = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:EditPolicies,
    onSuccess: (data) => {
    console.log("🚀 ~ useEditPolicies ~ data:", data)

      queryClient.invalidateQueries("Update Policies");
      showSuccesToast("",'Policies Update successfully')

    },
    onError:(error) => {
    console.log("🚀 ~ useEditPolicies ~ error:", error)
    ShowErrorToast(error)



    }
  });
};
