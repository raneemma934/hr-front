import { Box } from '@mui/system'
import { CircularProgress } from '@mui/material'
import CollapsibleTable from 'src/features/Report/componets/table'

import { useReports } from 'src/features/Report/hooks/useGetAllReports'
import { useEffect } from 'react'
import { useSelector } from 'react-redux'
import moment from 'moment'

const ReportPage = () => {
  const store = useSelector(state => state.user)
  const date = store?.date // Example date, replace it with your actual date
  const { data, isLoading, isError } = useReports(date)

  return (
    <>
      {data ? (
        <CollapsibleTable Data={data?.data?.data} />
      ) : (
        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '50vh' }}>
          <CircularProgress />
        </Box>
      )}
    </>
  )
}

export default ReportPage
