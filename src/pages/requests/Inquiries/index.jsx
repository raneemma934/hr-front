import React from 'react'
import Inquiries from 'src/features/requests/Inquiries/Componets/DataGrid'
import { Box, CircularProgress } from '@mui/material'
import useGetAllInquiries from 'src/features/requests/Inquiries/Hooks/useGetAllInquiries';
import { useSelector } from 'react-redux';

export default function RequestInquiries() {

  const store = useSelector(state => state.user)
  const date = store?.date; // Example date, replace it with your actual date
 const {data}=useGetAllInquiries(date)

  return <>
    {data ? <Inquiries  rows={data}  /> :
    <Box sx={{display: 'flex',justifyContent:"center",alignItems:"center",height:'100%' }} ><CircularProgress /></Box>
  }

  </>
}
