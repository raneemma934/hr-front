import { CircularProgress } from '@mui/material'
import { Box } from '@mui/system'
import React from 'react'
import { useSelector } from 'react-redux'
import SystemTable from 'src/features/requests/system/Componets/DataGrid'
import useGetAllSystem from 'src/features/requests/system/Hooks/useGetAllSystem'

export default function System() {
  const store = useSelector(state => state.user)
  const date = store?.date; // Example date, replace it with your actual date
 const {data}=useGetAllSystem(date)


  return (<>
    {data ? <SystemTable  rows={data}  /> :
    <Box sx={{display: 'flex',justifyContent:"center",alignItems:"center",height:'100%' }} ><CircularProgress /></Box>
  }
  </>
  )
}
