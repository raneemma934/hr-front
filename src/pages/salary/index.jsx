import { CircularProgress } from '@mui/material'
import { Box } from '@mui/system'
import { useEffect, useState } from 'react'
import SalaryDataGrid from 'src/features/salary/users/componets/DataGrid'
import { useGetDataByMonth } from 'src/features/salary/users/hooks/useGetDataByMonth'
import { DateFormateOfMonth } from 'src/utiltis/DateFormateOfMonth'

export default function Employees() {


  const { data: GetDataByMonth, mutate: getData } = useGetDataByMonth()

  const date = DateFormateOfMonth(new Date())

  // console.log("🚀 ~ Employees ~ date:", date)
  const [Datee, setDatee] = useState(date)

  useEffect(() => {
    getData(Datee)
  }, [Datee])




  return (
    <>
      {GetDataByMonth ? <SalaryDataGrid rows={GetDataByMonth} getData={getData}  setDatee={setDatee} Datee={Datee}/> :
        <Box height={'100%'} display={'center'} justifyContent={'center'} alignItems={'center'} >
          <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center", height: "70vh" }}>

            <CircularProgress className='loading-rtl' />
          </Box>
        </Box>}
    </>
  )

}
