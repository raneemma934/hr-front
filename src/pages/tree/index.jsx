import { Button } from '@mui/material';
import { Box, Stack } from '@mui/system'
import dynamic from 'next/dynamic';
import React from 'react'
import Logo from 'src/features/Contracts/view/logo';


const TreeComponent = dynamic(
  () => import("../../features/tree/componets/tree"),
  {
    ssr: false,
  }
);

export default function Tree() {



  return (
      <Stack >

      <Box  >
        <Logo />
         <div>
      {/* Primary text button */}
      <Button variant="tonal" color="secondary" >
        cancel
      </Button>


      {/* Success tonal button */}
      <Button variant="tonal" color="primary" size="small">
        primary for add submit
      </Button>

      {/* Error contained button */}
      <Button variant="contained" color="error">
        Error Contained
      </Button>
        {/* Error outline button */}
        <Button variant="outlined" color="error">
        Error outline
      </Button>
    </div>
    <div>
      {/* Tonal Button with Secondary Color and Custom Padding */}
      <Button size="small" variant="tonal" color="secondary">
        Small Tonal (12px 4px padding)
      </Button>

      <Button size="medium" variant="tonal" color="secondary">
        Medium Tonal (12px 8px padding)
      </Button>

      <Button size="large" variant="tonal" color="secondary">
        Large Tonal (24px 12px padding)
      </Button>
    </div>
    <div>
      {/* Button with a start icon */}
      <Button
        variant="contained"
        color="primary"
        size="medium"
        startIcon={<SaveIcon />} // Setting the start icon
      >
        Save
      </Button>

      {/* You can try with other variants or colors */}
      <Button
        variant="outlined"
        color="secondary"
        size="large"
        startIcon={<SaveIcon />} // Setting the start icon
      >
        Save Changes
      </Button>
    </div>
      </Box>

      <Stack  direction={"row"} justifyContent={"center"}>
        <TreeComponent/>
      </Stack>
      </Stack>
  )
}
