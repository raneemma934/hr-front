export function convertTo12HourFormat(time24) {
  const [hours, minutes] = time24?.split(':');
  let hours12 = parseInt(hours, 10);
  let ampm = '';

  if (hours12 === 0) {
    hours12 = 12;
    ampm = 'AM';
  } else if (hours12 < 12) {
    ampm = 'AM';
  } else if (hours12 === 12) {
    ampm = 'PM';
  } else {
    hours12 -= 12;
    ampm = 'PM';
  }

  return `${hours12}:${minutes} ${ampm}`;
}
