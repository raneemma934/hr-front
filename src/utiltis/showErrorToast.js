import toast from 'react-hot-toast';

// Function to extract meaningful error messages from different error response formats
const getErrorMessages = (errors) => {
  console.log("🚀 ~ getErrorMessages ~ errors:", errors);

  // Check if errors are provided in a detailed format (array or object structure)
  if (errors?.response?.data?.message && Array.isArray(errors.response.data.message)) {
    return errors.response.data.message;
  }

  // // Check if a single message is provided in the response
  // if (errors?.response?.data?.message) {
  //   return [errors.response.data.message];
  // }

  // Fallback: if no structured errors exist, return a generic message
  return ['حدث خطأ! يرجى المحاولة مرة أخرى.']; // Arabic message for "An error occurred! Please try again."
}

// Function to show each error as a separate toast
export const ShowErrorToast = (errors) => {
  const errorMessages = getErrorMessages(errors);

  // Show each error in a separate toast
  errorMessages.forEach((error, index) => {
    toast.error(`${error}`, {   // No need to include index in the message unless necessary
      id: `error-toast-${index}`, // Assign unique ID for each toast to ensure separation
      position: "bottom-right",
      style: {
        backgroundColor: "#DF2E38",   // Red color for error messages
        color: '#fff',                // White text for better contrast
        fontWeight: "600",            // Bold font for emphasis
        fontSize: "16px"              // Slightly larger text
      }
    });
  });
};
