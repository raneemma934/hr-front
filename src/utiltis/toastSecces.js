// toastUtils.js
import toast from 'react-hot-toast';

export const showSuccesToast = (message,SuccessMessageApi) => {
  
    toast.success(`${message?message:""}    ${SuccessMessageApi?SuccessMessageApi:""} `, {
        position: "bottom-left",
        style: {
          backgroundColor: "#91C483",
          color:'#ffff',
      fontWeight:"600",
      fontSize:"16px"

        }})
};

